<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
// define('DB_NAME', 'pdebacker');

// /** MySQL database username */
// define('DB_USER', 'root');

// /** MySQL database password */
// define('DB_PASSWORD', '');

// /** MySQL hostname */
// define('DB_HOST', 'localhost:8889');

// /** Database Charset to use in creating database tables. */
// define('DB_CHARSET', 'utf8mb4');

// /** The Database Collate type. Don't change this if in doubt. */
// define('DB_COLLATE', '');

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'pdebacker_dev');

/** MySQL database username */
define('DB_USER', 'pdebacker_dev_sa');

/** MySQL database password */
define('DB_PASSWORD', '12Adversary75');

/** MySQL hostname */
define('DB_HOST', 'gd174970-001.privatesql:35184');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8mb4');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'ThE;,-O%5iHT7g_^Z]OqPp$&=Vf$?kDdiqp<}`jwGGp+gPib-H)#X!>.Mf5O`m(6');
define('SECURE_AUTH_KEY',  '_YPdR&EH0AtJ$p|Uid|5-FvEO|m(CWMb!{A0uY^)i{dMn=q|nn}Fiz`)0aA$j}#n');
define('LOGGED_IN_KEY',    '<>.=yoT&V,NQ$1BNPi4|gd[l2zJg8Y~,z}?sj0%66XSH>WbPs<cx6lnqNRDTRF*Y');
define('NONCE_KEY',        '{VJKUo9+<bL/px~R&=yts>vnhoXziH& K!.nNSCk2uI^p@KzGUb1n6p{7BaPyjR8');
define('AUTH_SALT',        'in6.w9olNXRa8t]5{.[0Av0`R:].@u)**@GRgqMqQ?b90m 0#B0UDMccVUYWWw*f');
define('SECURE_AUTH_SALT', 'dG&hP5O11%mV4(,)q_&,/9WaJI>}zbOD#bCx,H>+/mu`LU:O%}TYHf-J1s,eRIyT');
define('LOGGED_IN_SALT',   '8NQwX!94+!x98nBFW-QozqL2x)n6O7vIg!X1fN+ryh<!+8L,Saw|hw9[dbKZeYxw');
define('NONCE_SALT',       '-E>Yx,$ah1T#*BS[ybH!Vv1iBGiPm-XV|RTx 3/fd5<.YHihH!Jbc$HR#k5dVS*t');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define( 'WP_DEBUG', true );
define( 'SAVEQUERIES', true );

define('WP_EXPORTER_ADMIN_BAR', true);

// Enable Debug logging to the /wp-content/debug.log file
define( 'WP_DEBUG_LOG', true );

// Disable display of errors and warnings 
define( 'WP_DEBUG_DISPLAY', false );
@ini_set( 'display_errors', 0 );

// Use dev versions of core JS and CSS files (only needed if you are modifying these core files)
define( 'SCRIPT_DEBUG', true );

define ("WPML_LOAD_API_SUPPORT",1);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
