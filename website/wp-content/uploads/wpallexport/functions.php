<?php
	function get_authority_id($value) {

		return get_page_by_title($value, OBJECT, 'authority')->ID;

	}

	function get_main_processing_objective_id($value) {

		return get_page_by_title($value, OBJECT, 'processing_objective')->ID;

	}

	function get_other_processing_objective_ids($value) {

		if (strpos($value, ', ') > 0) {
			$value = str_replace(', ', ';; ', $value);
		}

		if (strpos($value, ',') > 0) {

			$processing_objectives = explode(',', $value);

			$processing_objectives_ids = '';

			foreach ($processing_objectives as $processing_objective) {

				$processing_objective = str_replace(';; ', ', ', $processing_objective);

				$processing_objectives_ids .= get_page_by_title($processing_objective, OBJECT, 'processing_objective')->ID . '//';

			}

			$processing_objectives_ids = substr($processing_objectives_ids, 0, -2);

			return $processing_objectives_ids;

		}
		else {

			$value = str_replace(';;', ', ', $value);

			return get_page_by_title($value, OBJECT, 'processing_objective')->ID;

		}

	}

	function get_processing_base_ids($value) {

		if (strpos($value, ', ') > 0) {
			$value = str_replace(', ', ';; ', $value);
		}

		if (strpos($value, ',') > 0) {

			$processing_bases = explode(',', $value);

			$processing_bases_ids = '';

			foreach ($processing_bases as $processing_base) {

				$processing_base = str_replace(';; ', ', ', $processing_base);

				$processing_bases_ids .= get_page_by_title($processing_base, OBJECT, 'processing_base')->ID . '//';
			}

			$processing_bases_ids = substr($processing_bases_ids, 0, -2);

			return $processing_bases_ids;

		}
		else {

			$value = str_replace(';;', ', ', $value);

			return get_page_by_title($value, OBJECT, 'processing_base')->ID;

		}

	}

	function get_involved_parties_ids($value) {

		if (strpos($value, ', ') > 0) {
			$value = str_replace(', ', ';; ', $value);
		}

		if (strpos($value, ',') > 0) {

			$involved_parties = explode(',', $value);

			$involved_parties_ids = '';

			foreach ($involved_parties as $involved_party) {

				$involved_party = str_replace(';; ', ', ', $involved_party);

				$involved_parties_ids .= get_page_by_title($involved_party, OBJECT, 'involved_party')->ID . '//';

			}

			$involved_parties_ids = substr($involved_parties_ids, 0, -2);

			return $involved_parties_ids;

		}
		else {

			$value = str_replace(';;', ', ', $value);

			return get_page_by_title($value, OBJECT, 'involved_party')->ID;

		}

	}

	function get_data_category_theme_id($value) {

		return get_page_by_title($value, OBJECT, 'data_category_theme')->ID;

	}

	function get_data_exchange_repeater_ids($value) {

		// if data category
		if (strpos($value, ' - ') > 0) {

			if (strpos($value, ', ') > 0) {
				$value = str_replace(', ', ';; ', $value);
			}

			if (strpos($value, ',') > 0) {

				$data_categories = explode(',', $value);

				$data_categories_ids = '';

				foreach ($data_categories as $data_category) {

					$data_category = str_replace(';; ', ', ', $data_category);

					$data_categories_ids .= get_page_by_title($data_category, OBJECT, 'data_category')->ID . '//';

				}

				$data_categories_ids = substr($data_categories_ids, 0, -2);

				return $data_categories_ids;

			}
			else {

				$value = str_replace(';;', ', ', $value);

				return get_page_by_title($value, OBJECT, 'data_category')->ID;

			}

		}
		// if country or beneficiary
		elseif (strpos($value, ' <br> ') > 0) {

			if (strpos($value, ', ') > 0) {
				$value = str_replace(', ', ';; ', $value);
			}

			if (strpos($value, ',') > 0) {

				$values = explode(',', $value);

				$values_ids = '';

				foreach ($values as $value_data) {

					$value_data = str_replace(';; ', ', ', $value_data);

					// checking whether the value is a country or a beneficiary
					if (get_page_by_title($value_data, OBJECT, 'country')->ID != '') {

						$values_ids .= get_page_by_title($value_data, OBJECT, 'country')->ID . '//';

					}
					elseif (get_page_by_title($value_data, OBJECT, 'beneficiary')->ID != '') {

						$values_ids .= get_page_by_title($value_data, OBJECT, 'beneficiary')->ID . '//';
						
					}

				}

				$values_ids = substr($values_ids, 0, -2);

				return $values_ids;

			}
			else {

				$value = str_replace(';;', ', ', $value);

				// checking whether the value is a country or a beneficiary
				if (get_page_by_title($value, OBJECT, 'country')->ID != '') {

					$values_ids = get_page_by_title($value, OBJECT, 'country')->ID;

				}
				elseif (get_page_by_title($value, OBJECT, 'beneficiary')->ID != '') {

					$values_ids = get_page_by_title($value, OBJECT, 'beneficiary')->ID;
					
				}

				return $values_ids;

			}

		}
		else {
				
			return $value;

		}

	}
?>