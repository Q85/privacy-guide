<?php
	
	error_reporting(E_ALL ^ E_NOTICE); 

	if (isset($_POST['url']) && $_POST['url'] == '') {

		// email sent to the authority
		$email = $_POST['email'];
		$language = $_POST['language'];

		$subject = ($language == '_nl' ? 'Nieuw bericht verstuurd vanuit de Privacy Register website' : 'Nouveau message envoyé à partir du site Privacy Register Data');
		$message = wordwrap($_POST['message'], 70, "\r\n");
		$authority = $_POST['authority'];

		$headers = 'From: <' . $email . ">\r\n";
		$headers .= 'Reply-To: <' . $email . ">\r\n";

		$headers = array("From: " . $email,
		    "Reply-To: " . $email,
		    "X-Mailer: PHP/" . PHP_VERSION
		);
		$headers = implode("\r\n", $headers);

		mail($authority, $subject, print_r($message,true), $headers);

		// copy of the email sent to the requestor
		$subject = ($language == '_nl' ? 'Uw bericht verstuurd naar ' . $authority : 'Copie de votre message envoyé à ' . $authority);
		$message_requestor = wordwrap($_POST['message'], 70, "\r\n");

		$headers_requestor = array("From: no-reply@privcy-guide.be",
		    "Reply-To: no-reply@privcy-guide.be",
		    "X-Mailer: PHP/" . PHP_VERSION
		);
		$headers_requestor = implode("\r\n", $headers_requestor);

		mail($email, $subject, print_r($message_requestor,true), $headers_requestor);

		echo $email . "<br>";
		echo $language . "<br>";
		echo $message . "<br>";
		echo $subject . "<br>";
		echo $authority . "<br>";

	}

?>