
		<footer class="blog-footer text-left">
		    <div class="container-fluid">
		    	<div class="row justify-content-between mb-3">
		    		<div class="col-12 col-sm-6 col-lg-4 col-xl-3 mb-4">
		    			<img src="<?= get_bloginfo("template_url"); ?>/img/logo-transparent.png" width="120" alt="" />
		    		</div>
		    		<div class="col-12 col-sm-6 col-lg-4 col-xl-3 mb-4">
		    			<div class="footer-category-title text-uppercase pb-2">
		    				<?php echo get_custom_label('footer_category_links_title'); ?>
		    			</div>
		    			<div class="footer-links">
		    				<ul class="list-group">
		    					<?php foreach (get_main_menu_items('footer_menu') as $footer_menu_item) { ?>
			    					<li class="list-group-item">
			    						<a class="nav-link pl-0" href="<?php echo $footer_menu_item->url; ?>"><?php echo $footer_menu_item->title; ?></a>
			    					</li>
			    				<?php } ?>
			    			</ul>
		    			</div>
		    		</div>
		    	</div>

		    	<div class="row">
		    		<div class="col-12">
		    			<p class="footer-copyright">
		    				<?php echo sprintf(get_custom_label('footer_copyright'), date('Y')); ?>
		    			</p>
		    		</div>
		    	</div>
		    </div>
		</footer>
		
		<script src="https://code.jquery.com/jquery-3.2.1.min.js" integrity="sha256-hwg4gsxgFZhOsEEamdOYGBf13FyQuiTwlAQgxVSNgt4=" crossorigin="anonymous"></script>
		<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.3/umd/popper.min.js" integrity="sha384-vFJXuSJphROIrBnz7yo7oB41mKfc8JzQZiCq4NCceLEaO4IHwicKwpJf9c9IpFgh" crossorigin="anonymous"></script>

		<?php wp_footer(); ?>
		
		<?php if (get_post_field('post_name') == 'accueil' || get_post_field('post_name') == 'hoofdpagina') { ?>
			
			<?php $graph_data = get_graph_data(); ?>
			
			<script>

			    // Once the document is ready we set javascript and page settings
			    var screenWidth;
			    var screenHeight;

			    $(document).ready(function () {

			        // var rect;
			        // if (self==top) {
			        //     rect = document.body.getBoundingClientRect();
			        // }
			        // else {
			        //     rect = parent.document.body.getBoundingClientRect();
			        // }

			        var rect = document.getElementById('halo_container').getBoundingClientRect();

			        // Set display size based on window size.
			        screenWidth = (rect.width < 960) ? Math.round(rect.width*.85) : Math.round((rect.width - 210) *.85)
			        screenHeight = Math.min(parent.innerHeight * 0.75, screenWidth);
			        screenWidth = screenHeight;

			        d3.select("#currentDisplay").attr("item_value", screenWidth + "," + screenHeight);

			        // Set the size of our container element.
			        viz_container = d3.selectAll("#viz_container").style("width", screenWidth + "px").style("height", screenHeight + "px");

				 	var data = <?= $graph_data; ?>;

				 	initialize(data);

			    });

			</script>

		<?php } ?>
		
	</body>
</html>
