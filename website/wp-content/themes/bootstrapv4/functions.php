<?php

// error_reporting(-1);
// ini_set('display_errors', 'On');

function bootstrapstarter_enqueue_styles() {
    wp_register_style('bootstrap', get_template_directory_uri() . '/css/bootstrap.min.css' );
    $dependencies = array('bootstrap');
	wp_enqueue_style( 'bootstrapstarter-style', get_stylesheet_uri(), $dependencies ); 
}

function bootstrapstarter_enqueue_scripts() {
    $dependencies = array('jquery');
    wp_enqueue_script('bootstrap', get_template_directory_uri().'/js/bootstrap.min.js', $dependencies, '', true);
    wp_enqueue_script('custom_scripts', get_template_directory_uri().'/js/custom_scripts.js', $dependencies, '', true);
}

add_action( 'wp_enqueue_scripts', 'bootstrapstarter_enqueue_styles' );
add_action( 'wp_enqueue_scripts', 'bootstrapstarter_enqueue_scripts' );

function bootstrapstarter_wp_setup() {
    add_theme_support( 'title-tag' );
}

add_action( 'after_setup_theme', 'bootstrapstarter_wp_setup' );

function bootstrapstarter_register_menu() {
    register_nav_menu('header_menu', __( 'Header Menu' ));
}

add_action('init', 'bootstrapstarter_register_menu');

function get_custom_label( $post_name, $post_type = 'custom_label' ) {
    return get_page_by_path($post_name . get_current_language(), OBJECT, $post_type)->post_title;
}

function get_current_language() {
	return "_" . substr(get_locale(),0,2);
}

function get_main_menu_items($menu) {
	return wp_get_nav_menu_items($menu . get_current_language());
}

function get_main_menu_languages() {
	return icl_get_languages('skip_missing=Y&orderby=id&order=desc&link_empty_to=str');
}

function get_privacy_register_data() {

	$prd_ids = get_posts(array(
		'posts_per_page' => -1,
		'post_status' => 'publish',
		'orderby' => 'ID',
		'order' => 'ASC',
		'post_type' => 'privacy_register',
		'fields' => 'ids'
	));

	return $prd_ids;

}

// this function is used to get the data category themes
function get_data_categories_themes() {

	$data_categories_themes = get_posts(array(
		'posts_per_page' => -1,
		'post_type' => 'data_category_theme',
		'fields' => 'id'
	));

	$data_categories_themes_array = array();

	foreach ($data_categories_themes as $data_category_theme) {

		$id = $data_category_theme->ID;
		$label = get_post_meta($id, 'data_category_theme' . get_current_language());
		$color = get_post_meta($id, 'data_category_theme_color');
		$name = substr(strrchr($data_category_theme->post_name, "-"), 1);
		$home_image = get_field('data_category_theme_home_image', $id)['sizes']['large'];
		$data_category_image = get_field('data_category_theme_data_category_image', $id)['sizes']['large'];
		$nav_image = get_field('data_category_theme_navigation_image', $id)['sizes']['large'];

		$data_categories_themes_array[] = array(
			'id' => $id,
			'label' => $label,
			'color' => $color,
			'name' => $name,
			'home_image' => $home_image,
			'data_category_image' => $data_category_image,
			'nav_image' => $nav_image
		);

	}

	uasort($data_categories_themes_array, function ($a, $b){
	    if ($a == $b)
            return 0;
        return ($a['label'] < $b['label']) ? -1 : 1;
	});

	return $data_categories_themes_array;

}

// this function is used in the header.php to get the data categories for each data category theme
function get_data_categories_records_per_theme() {

	$collator = new Collator('fr_FR.utf8');

	$data_categories_themes_array = get_data_categories_themes();

	$data_categories = get_posts(array(
		'posts_per_page' => -1,
		'order_by' => 'data_category_name' . get_current_language(),
		'order' => 'ASC',
		'post_type' => 'data_category',
		'fields' => 'id'
	));

	$prd_ids = get_privacy_register_data();

	$data_categories_ids = array();

	// putting all selected data categories in an array
	foreach ($prd_ids as $prd_id) {

		// for each row in the "prd_data_data_exchange" repeater
		while (have_rows('prd_data_data_exchange', $prd_id)) : the_row();

			// for each selected data category
			foreach (get_sub_field('prd_data_data_exchange_data_category') as $data_category) {

				$data_categories_ids[] = $data_category->ID;

			}

	    endwhile;

	}

	$data_categories_ids = array_unique($data_categories_ids);

	$data_categories_array = array();

	// for each data category theme
	foreach ($data_categories_themes_array as $data_categories_theme){

		$data_categories_theme_id = $data_categories_theme['id'];

		$data_categories_records_array = array();

		// for each data category
		foreach ($data_categories as $data_category) {

			// gettng data category id
			$data_category_id = $data_category->ID;

			if (get_post_meta($data_category_id, 'data_category_theme', true)[0] == $data_categories_theme_id) {

				// if that data category is present in the data_categories_ids array
				if (in_array($data_category_id, $data_categories_ids)) {

					// checking whether there is a page created for that data category
					$data_category_page_id = get_page_by_title(get_post_meta($data_category_id, 'data_category_name' . get_current_language())[0], OBJECT, 'page')->ID;

					// if a page is created for that data category
					if ($data_category_page_id != '') {

						$data_categories_records_array[] = array(
							'name' => get_post_meta($data_category_id, 'data_category_name' . get_current_language())[0],
							'link' => get_permalink($data_category_page_id)
						);

					}

				}

			}

		}

		// if there are data categories for that data category theme
		if (count($data_categories_records_array) > 0) {

			$collator->sort($data_categories_records_array);

			$data_categories_array[] = array(
				'id' => $data_categories_theme['id'],
				'label' => $data_categories_theme['label'][0],
				'color' => $data_categories_theme['color'][0],
				'name' => $data_categories_theme['name'],
				'home_image' => $data_categories_theme['home_image'],
				'data_category_image' => $data_categories_theme['data_category_image'],
				'nav_image' => $data_categories_theme['nav_image'],
				'data' => $data_categories_records_array
			);

		}
		
	}

	return $data_categories_array;

}

// this function is used in the header.php to get the authorities categories
function get_authorities_categories() {

	$authority_categories = get_terms([
	    'taxonomy' => 'authority_category',
	    'hide_empty' => false,
	]);

	$authority_categories_array = array();

	foreach ($authority_categories as $authority_category) {

		$id = $authority_category->term_id;
		$label = (get_current_language() == '_nl' ? explode(" / ", $authority_category->name)[0] : explode(" / ", $authority_category->name)[1]);
		$slug = substr(strrchr($authority_category->slug, "-"), 1);

		// removing Lokale Politie / Police locale from the menu
		if ($slug != 'localpolice') {
			$authority_categories_array[] = array('id' => $id, 'label' => $label, 'slug' => $slug);
		}
		
	}

	uasort($authority_categories_array, function ($a, $b){
	    if ($a == $b)
            return 0;
        return ($a['label'] < $b['label']) ? -1 : 1;
	});

	return $authority_categories_array;

}

function get_authorities_categories_records_per_category() {

	$collator = new Collator('fr_FR.utf8');

	$authority_categories = get_authorities_categories();

	$prd_ids = get_privacy_register_data();

	$authority_categories_array = array();

	foreach ($authority_categories as $authority_category) {

		$posts_with_taxonomy = get_posts(
            array(
            	'posts_per_page' => -1,
                'post_type' => 'authority',
                'tax_query' => array(
                    array(
                    'taxonomy' => 'authority_category',
                    'field' => 'id',
                    'terms' => $authority_category['id']
                    )
                )
            )
        );

		$post_with_taxonomy_array = array();

        foreach ($posts_with_taxonomy as $post_with_taxonomy) {

    		$post_alias = get_post_meta($post_with_taxonomy->ID, 'authority_post_alias' . get_current_language());
    		$post_link_id = get_page_by_title($post_alias[0], OBJECT, 'page')->ID;

    		// if a page is created for that authority
			if ($post_link_id != '') {

				$post_name = get_post_meta($post_with_taxonomy->ID, 'authority_post_name' . get_current_language());
				$post_link = get_permalink(get_page_by_title($post_alias[0], OBJECT, 'page')->ID);
				$post_logo = get_field('authority_post_logo', $post_with_taxonomy->ID)['sizes']['large'];

    			$post_with_taxonomy_array[] = array(
    				'alias' => $post_alias,
    				'name' => $post_name,
    				'link' => $post_link,
    				'logo' => $post_logo
    			);

    		}

        }

        // if there are authorities
		if (count($post_with_taxonomy_array) > 0) {

	        $collator->sort($post_with_taxonomy_array);

	        $authority_categories_array[] = array(
	        	'id' => $authority_category['id'],
	        	'label' => $authority_category['label'],
	        	'slug' => $authority_category['slug'],
	        	'data' => $post_with_taxonomy_array
	        );

	    }

	}

	// echo json_encode($authority_categories_array);

	return $authority_categories_array;

}

function get_details_per_region($prd_id) {

	$collator = new Collator('fr_FR.utf8');

	$not_shared_temp_data = array();
	$belgium_temp_data = array();
	$europe_temp_data = array();
	$world_temp_data = array();

	$belgium_temp_beneficiaries = array();
	$europe_temp_beneficiaries = array();
	$world_temp_beneficiaries = array();


	// for each row in the repeater
	while (have_rows('prd_data_data_exchange', $prd_id)) : the_row();

		// DATA CATEGORIES
		$data_categories = array();

		// for each selected data categories
		foreach (get_sub_field('prd_data_data_exchange_data_category') as $data_category) {

			$data_category_id = $data_category->ID;
			$data_category_name = get_field('data_category_name' . get_current_language(), $data_category_id);
			$data_category_link = get_permalink(get_page_by_title($data_category_name, OBJECT, 'page')->ID);
			
	    	$data_categories[] = array(
	    		'id' => $data_category_id,
	    		'name' => $data_category_name,
	    		'link' => $data_category_link,
	    		'theme_id' => get_field('data_category_theme', $data_category_id)[0]->ID,
	    		'theme_name' => (get_current_language() == '_nl' ? explode(" / ", get_field('data_category_theme', $data_category_id)[0]->post_title)[0] : explode(" / ", get_field('data_category_theme', $data_category_id)[0]->post_title)[1])
	    	);

	    }

	    // BENEFICIARIES
	    $beneficiaries = array();

		// if the data is not shared
		if (get_sub_field('prd_data_data_exchange_data_shared', $prd_id) == 0) {

			$not_shared_temp_data[] = $data_categories;

		}
		// else if the data is shared
		elseif (get_sub_field('prd_data_data_exchange_data_shared', $prd_id) == 1) {

			$belgium_temp_data[] = $data_categories;

			// getting selected beneficiaries names and links
			foreach (get_sub_field('prd_data_data_exchange_beneficiaries') as $beneficiary) {

				$beneficiary_name = get_field('beneficiary_category' . get_current_language(), $beneficiary->ID);
				$beneficiary_link = get_permalink(get_page_by_title($beneficiary_name, OBJECT, 'page')->ID);

				$beneficiaries[] = array(
					'name' => $beneficiary_name,
					'link' => $beneficiary_link
				);

			}

			$belgium_temp_beneficiaries[] = $beneficiaries;

			// if data is shared in Europe
	    	if (get_sub_field('prd_data_data_exchange_data_shared_in_europe') == 1) {

	    		$europe_temp_data[] = $data_categories;
	    		$europe_temp_beneficiaries[] = $beneficiaries;

	    	}

	    	// if data is shared outside of Europe
	    	if (get_sub_field('prd_data_data_exchange_data_shared_outside_europe') == 1) {

	    		$world_temp_data[] = $data_categories;
	    		$world_temp_beneficiaries[] = $beneficiaries;

	    	}

		}
    	

	endwhile;

	// DATA CATEGORIES
	// removing duplicates of the 2D array
	$not_shared_temp_data = array_map('unserialize', array_unique(array_map('serialize', $not_shared_temp_data)));
	$belgium_temp_data = array_map('unserialize', array_unique(array_map('serialize', $belgium_temp_data)));
	$europe_temp_data = array_map('unserialize', array_unique(array_map('serialize', $europe_temp_data)));
	$world_temp_data = array_map('unserialize', array_unique(array_map('serialize', $world_temp_data)));

	
	/* NOT SHARED DATA */
	$not_shared_data = array();

	foreach ($not_shared_temp_data as $not_shared_temp_data_details) {
		if (count($not_shared_temp_data_details) > 1) {

			foreach($not_shared_temp_data_details as $not_shared_temp_data_detail) {
				$not_shared_data[] = $not_shared_temp_data_detail;
			}
		}
		else {
			$not_shared_data[] = $not_shared_temp_data_details[0];
		}
	}

	$not_shared_data = array_map('unserialize', array_unique(array_map('serialize', $not_shared_data)));
	$collator->sort($not_shared_data);

	/* BELGIUM DATA */
	$belgium_data = array();

	foreach ($belgium_temp_data as $belgium_temp_data_details) {
		if (count($belgium_temp_data_details) > 1) {

			foreach($belgium_temp_data_details as $belgium_temp_data_detail) {
				$belgium_data[] = $belgium_temp_data_detail;
			}
		}
		else {
			$belgium_data[] = $belgium_temp_data_details[0];
		}
	}

	$belgium_data = array_map('unserialize', array_unique(array_map('serialize', $belgium_data)));
	$collator->sort($belgium_data);

	/* EUROPE DATA */
	$europe_data = array();

	foreach ($europe_temp_data as $europe_temp_data_details) {
		if (count($europe_temp_data_details) > 1) {

			foreach($europe_temp_data_details as $europe_temp_data_detail) {
				$europe_data[] = $europe_temp_data_detail;
			}
		}
		else {
			$europe_data[] = $europe_temp_data_details[0];
		}
	}

	$europe_data = array_map('unserialize', array_unique(array_map('serialize', $europe_data)));
	$collator->sort($europe_data);

	/* WORLD DATA */
	$world_data = array();

	foreach ($world_temp_data as $world_temp_data_details) {
		if (count($world_temp_data_details) > 1) {

			foreach($world_temp_data_details as $world_temp_data_detail) {
				$world_data[] = $world_temp_data_detail;
			}
		}
		else {
			$world_data[] = $world_temp_data_details[0];
		}
	}

	$world_data = array_map('unserialize', array_unique(array_map('serialize', $world_data)));
	$collator->sort($world_data);
	

	$not_shared_data_temp_theme_ids = array();
	$not_shared_data_final_data_categories = array();

	// foreach not_shared data category
	foreach ($not_shared_data as $not_shared_datas) {
		$not_shared_data_temp_theme_ids[] = $not_shared_datas['theme_id'];
	}

	// removing duplicates
	$not_shared_data_unique_theme_ids = array_unique($not_shared_data_temp_theme_ids);

	// foreach not_shared data category theme id
	foreach ($not_shared_data_unique_theme_ids as $not_shared_data_unique_theme_id) {

		$not_shared_data_temp_data_categories = array();

		foreach ($not_shared_data as $not_shared_datas) {

			if ($not_shared_data_unique_theme_id == $not_shared_datas['theme_id']) {

				$not_shared_data_temp_data_categories[] = array(
					'name' => $not_shared_datas['name'],
					'link' => $not_shared_datas['link']
				);

			}

		}

		$not_shared_data_temp_data_categories = array_map('unserialize', array_unique(array_map('serialize', $not_shared_data_temp_data_categories)));
		$collator->sort($not_shared_data_temp_data_categories);

		$not_shared_data_final_data_categories[] = array(
			'data_category_theme' => get_field('data_category_theme' . get_current_language(), $not_shared_data_unique_theme_id),
			'data_category_theme_color' => get_field('data_category_theme_color', $not_shared_data_unique_theme_id),
			'data_category_names' => $not_shared_data_temp_data_categories
		);

	}


	$belgium_data_temp_theme_ids = array();
	$belgium_data_final_data_categories = array();

	// foreach belgium data category
	foreach ($belgium_data as $belgium_datas) {
		$belgium_data_temp_theme_ids[] = $belgium_datas['theme_id'];
	}

	// removing duplicates
	$belgium_data_unique_theme_ids = array_unique($belgium_data_temp_theme_ids);

	// foreach belgium data category theme id
	foreach ($belgium_data_unique_theme_ids as $belgium_data_unique_theme_id) {

		$belgium_data_temp_data_categories = array();

		foreach ($belgium_data as $belgium_datas) {

			if ($belgium_data_unique_theme_id == $belgium_datas['theme_id']) {

				$belgium_data_temp_data_categories[] = array(
					'name' => $belgium_datas['name'],
					'link' => $belgium_datas['link']
				);

			}

		}

		$belgium_data_temp_data_categories = array_map('unserialize', array_unique(array_map('serialize', $belgium_data_temp_data_categories)));
		$collator->sort($belgium_data_temp_data_categories);

		$belgium_data_final_data_categories[] = array(
			'data_category_theme' => get_field('data_category_theme' . get_current_language(), $belgium_data_unique_theme_id),
			'data_category_theme_color' => get_field('data_category_theme_color', $belgium_data_unique_theme_id),
			'data_category_names' => $belgium_data_temp_data_categories
		);

	}

	$europe_data_temp_theme_ids = array();
	$europe_data_final_data_categories = array();

	// foreach europe data category
	foreach ($europe_data as $europe_datas) {
		$europe_data_temp_theme_ids[] = $europe_datas['theme_id'];
	}

	// removing duplicates
	$europe_data_unique_theme_ids = array_unique($europe_data_temp_theme_ids);

	// foreach europe data category theme id
	foreach ($europe_data_unique_theme_ids as $europe_data_unique_theme_id) {

		$europe_data_temp_data_categories = array();

		foreach ($europe_data as $europe_datas) {

			if ($europe_data_unique_theme_id == $europe_datas['theme_id']) {

				$europe_data_temp_data_categories[] = array(
					'name' => $europe_datas['name'],
					'link' => $europe_datas['link']
				);

			}

		}

		$europe_data_temp_data_categories = array_map('unserialize', array_unique(array_map('serialize', $europe_data_temp_data_categories)));
		$collator->sort($europe_data_temp_data_categories);

		$europe_data_final_data_categories[] = array(
			'data_category_theme' => get_field('data_category_theme' . get_current_language(), $europe_data_unique_theme_id),
			'data_category_theme_color' => get_field('data_category_theme_color', $europe_data_unique_theme_id),
			'data_category_names' => $europe_data_temp_data_categories
		);

	}

	$world_data_temp_theme_ids = array();
	$world_data_final_data_categories = array();

	// foreach world data category
	foreach ($world_data as $world_datas) {
		$world_data_temp_theme_ids[] = $world_datas['theme_id'];
	}

	// removing duplicates
	$world_data_unique_theme_ids = array_unique($world_data_temp_theme_ids);

	// foreach world data category theme id
	foreach ($world_data_unique_theme_ids as $world_data_unique_theme_id) {

		$world_data_temp_data_categories = array();

		foreach ($world_data as $world_datas) {

			if ($world_data_unique_theme_id == $world_datas['theme_id']) {

				$world_data_temp_data_categories[] = array(
					'name' => $world_datas['name'],
					'link' => $world_datas['link']
				);

			}

		}

		$world_data_temp_data_categories = array_map('unserialize', array_unique(array_map('serialize', $world_data_temp_data_categories)));
		$collator->sort($world_data_temp_data_categories);

		$world_data_final_data_categories[] = array(
			'data_category_theme' => get_field('data_category_theme' . get_current_language(), $world_data_unique_theme_id),
			'data_category_theme_color' => get_field('data_category_theme_color', $world_data_unique_theme_id),
			'data_category_names' => $world_data_temp_data_categories
		);

	}


	// BENEFICIARIES
	// removing duplicates of the 2D array
	$belgium_temp_beneficiaries = array_map('unserialize', array_unique(array_map('serialize', $belgium_temp_beneficiaries)));
	$europe_temp_beneficiaries = array_map('unserialize', array_unique(array_map('serialize', $europe_temp_beneficiaries)));
	$world_temp_beneficiaries = array_map('unserialize', array_unique(array_map('serialize', $world_temp_beneficiaries)));

	$belgium_beneficiaries = array();
	$europe_beneficiaries = array();
	$world_beneficiaries = array();

	foreach ($belgium_temp_beneficiaries as $belgium_temp_beneficiary) {
		if (count($belgium_temp_beneficiary) > 1) {

			foreach($belgium_temp_beneficiary as $belgium_temp_beneficiary_sub) {
				$belgium_beneficiaries[] = $belgium_temp_beneficiary_sub;
			}
		}
		else {
			$belgium_beneficiaries[] = $belgium_temp_beneficiary[0];
		}
	}

	$belgium_beneficiaries = array_map('unserialize', array_unique(array_map('serialize', $belgium_beneficiaries)));
	$collator->sort($belgium_beneficiaries);

	foreach ($europe_temp_beneficiaries as $europe_temp_beneficiary) {
		if (count($europe_temp_beneficiary) > 1) {

			foreach($europe_temp_beneficiary as $europe_temp_beneficiary_sub) {
				$europe_beneficiaries[] = $europe_temp_beneficiary_sub;
			}
		}
		else {
			$europe_beneficiaries[] = $europe_temp_beneficiary[0];
		}
	}

	$europe_beneficiaries = array_map('unserialize', array_unique(array_map('serialize', $europe_beneficiaries)));
	$collator->sort($europe_beneficiaries);

	foreach ($world_temp_beneficiaries as $world_temp_beneficiary) {
		if (count($world_temp_beneficiary) > 1) {

			foreach($world_temp_beneficiary as $world_temp_beneficiary_sub) {
				$world_beneficiaries[] = $world_temp_beneficiary_sub;
			}
		}
		else {
			$world_beneficiaries[] = $world_temp_beneficiary[0];
		}
	}

	$world_beneficiaries = array_map('unserialize', array_unique(array_map('serialize', $world_beneficiaries)));
	$collator->sort($world_beneficiaries);

	return array(
		$not_shared_data_final_data_categories,
		$belgium_data_final_data_categories,
		$europe_data_final_data_categories,
		$world_data_final_data_categories,
		$belgium_beneficiaries,
		$europe_beneficiaries,
		$world_beneficiaries
	);

}

function get_beneficiaries_categories() {

	$prd_ids = get_privacy_register_data();

	$beneficiaries_categories_ids = array();

	// for each prd
	foreach ($prd_ids as $prd_id) {

		// for each row in the "prd_data_data_exchange" repeater
		while (have_rows('prd_data_data_exchange', $prd_id)) : the_row();

			// getting selected beneficiaries ids
			foreach (get_sub_field('prd_data_data_exchange_beneficiaries') as $beneficiary) {

				$beneficiaries_categories_ids[] = $beneficiary->ID;

			}

	    endwhile;

	}

	$beneficiaries_categories_ids = array_unique($beneficiaries_categories_ids);

	$beneficiaries_categories = get_posts(array(
		'posts_per_page' => -1,
		'orderby' => 'ID',
		'order' => 'ASC',
		'post_type' => 'beneficiary',
		'fields' => 'ids'
	));

	$beneficiaries_categories_array = array();

	foreach ($beneficiaries_categories as $beneficiary_category) {

		// if the beneficiary is selected in at least one prd 
    	if (in_array($beneficiary_category, $beneficiaries_categories_ids)) {

    		$beneficiary_link_id = get_page_by_title(get_post_meta($beneficiary_category, 'beneficiary_category' . get_current_language())[0], OBJECT, 'page')->ID;

    		// if there is a page created for that beneficiary
    		if ($beneficiary_link_id != '') {

    			$beneficiaries_categories_array[] = array(
	    			'name' => get_post_meta($beneficiary_category, 'beneficiary_category' . get_current_language())[0],
	    			'link' => get_permalink($beneficiary_link_id)
	    		);

    		}

    	}

	}

	return $beneficiaries_categories_array;

}

function get_private_data_amount($filtered_prd_id) {

	$data_amount = 0;

	foreach (get_field('prd_data_data_exchange', $filtered_prd_id) as $data) {

		$data_amount++;

	}

	return $data_amount;

}

function get_data_page_data($data_category_id) {

	$collator = new Collator('fr_FR.utf8');

	$prd_ids = get_privacy_register_data();

	// this array will include all filtered privacy register data id's
	$filtered_prd_ids = array();

	// STEP 1: for each privacy register data id
	foreach ($prd_ids as $prd_id) {

		// for each row in the "prd_data_data_exchange" repeater
		while (have_rows('prd_data_data_exchange', $prd_id)) : the_row();

			// for each selected data category
			foreach (get_sub_field('prd_data_data_exchange_data_category') as $data_category) {

				if ($data_category_id == $data_category->ID) {

					$filtered_prd_ids[] = $prd_id;

				}

			}

	    endwhile;

    }

    // we now have the unique list of all filtered privacy register data ids
    $filtered_prd_ids = array_unique($filtered_prd_ids);

    $beneficiaries = array();
    $beneficiaries_ids = array();
    $authorities_ids = array();

    // STEP 2: for each filtered privacy register data, get the beneficiaries
    foreach ($filtered_prd_ids as $filtered_prd_id) {

    	$authorities_ids[] = get_field('prd_identification_authority', $filtered_prd_id)[0]->ID;

		// for each row in the repeater ... 
		while (have_rows('prd_data_data_exchange', $filtered_prd_id)) : the_row();

        	// getting selected beneficiaries names and ids
        	foreach (get_sub_field('prd_data_data_exchange_beneficiaries') as $beneficiary) {

        		$beneficiaries[] = get_field('beneficiary_category' . get_current_language(), $beneficiary->ID);
        		$beneficiaries_ids[] = $beneficiary->ID;

        	}

		endwhile;

    }

    $authorities_ids = array_unique($authorities_ids);

    // removing duplicates of beneficiaries names
	$beneficiaries = array_unique($beneficiaries);

    // sorting the array of beneficiaries alphabetically		
	$collator->sort($beneficiaries);

	// adding the "Services publics fédéraux uniquement" in the list of beneficiaries
	array_unshift($beneficiaries, get_custom_label('data_page_all_beneficiaries_item'));

	// removing duplicates of beneficiaries ids
	$beneficiaries_ids = array_unique($beneficiaries_ids);

	$final_data = array();

	// for each beneficiary, we need to get the prd that have this beneficiary
	foreach ($beneficiaries_ids as $key => $beneficiaries_id) {

		$public_data = array();
    	$private_data_ids = array();
    	$semi_public_data = array();

	    // for each filtered privacy register data, get the beneficiaries
	    foreach ($filtered_prd_ids as $filtered_prd_id) {

			// for each row in the repeater ... 
			while (have_rows('prd_data_data_exchange', $filtered_prd_id)) : the_row();

	        	// for each selected beneficiary
	        	foreach (get_sub_field('prd_data_data_exchange_beneficiaries') as $beneficiary) {

	        		if ($beneficiaries_id == $beneficiary->ID) {

	        			$visibility = get_field('prd_processing_description_visibility', $filtered_prd_id);

	        			// if the visibility is set on 'Privaat / privé'
	        			if ($visibility == 'Privaat / privé') {

		        			$private_data_ids[] = $filtered_prd_id;

	        			}
	        			// if the visibility is set on semi-public
	        			elseif ($visibility == 'Beperkt publiek / publique avec restrictions') {

		        			$semi_public_data[] = array(
			        			'id' => $filtered_prd_id,
			        			'name' => get_field('prd_processing_description_name' . get_current_language(), $filtered_prd_id),
			        			'description' => get_field('prd_processing_description_description' . get_current_language(), $filtered_prd_id),
			        			'objective' => get_field('processing_objective_name' . get_current_language(), get_field('prd_processing_description_main_processing_objective', $filtered_prd_id)[0]->ID)
			        		);

	        			}
	        			// if the visibility is set on public
	        			else {

		        			$public_data[] = array(
			        			'id' => $filtered_prd_id,
			        			'name' => get_field('prd_processing_description_name' . get_current_language(), $filtered_prd_id),
			        			'description' => get_field('prd_processing_description_description' . get_current_language(), $filtered_prd_id),
			        			'objective' => get_field('processing_objective_name' . get_current_language(), get_field('prd_processing_description_main_processing_objective', $filtered_prd_id)[0]->ID)
			        		);

	        			}

	        		}

	        	}

			endwhile;

	    }

	    if (count($public_data) > 0) {

	    	// removing duplicates
	    	$public_data = array_map('unserialize', array_unique(array_map('serialize', $public_data)));

	    	$final_data[] = array(
	    		'beneficiary_name' => get_field('beneficiary_category' . get_current_language(), $beneficiaries_id),
	    		'visibility' => 'Publiek / publique',
	    		'private_data_amount' => 0,
	    		'prd_data' => $public_data
	    	);

	    }

	    if (count($semi_public_data) > 0) {

	    	// removing duplicates
	    	$semi_public_data = array_map('unserialize', array_unique(array_map('serialize', $semi_public_data)));

	    	$final_data[] = array(
	    		'beneficiary_name' => get_field('beneficiary_category' . get_current_language(), $beneficiaries_id),
	    		'visibility' => 'Beperkt publiek / publique avec restrictions',
	    		'private_data_amount' => 0,
	    		'prd_data' => $semi_public_data
	    	);

	    }

	    if (count($private_data_ids) > 0) {

	    	$private_data_ids = array_unique($private_data_ids);

	    	$final_data[] = array(
	    		'beneficiary_name' => get_field('beneficiary_category' . get_current_language(), $beneficiaries_id),
	    		'visibility' => 'Privaat / privé',
	    		'private_data_amount' => count($private_data_ids),
	    		'prd_data' => ''
	    	);

	    }

	}

	// now we need to get all the data for the first item in the list of beneficiaries
	$all_data_ids = array();

	// this array will include all the cards
	$all_data = array();

	// for each privacy register data id
	foreach ($prd_ids as $prd_id) {

		// for each row in the "prd_data_data_exchange" repeater
		while (have_rows('prd_data_data_exchange', $prd_id)) : the_row();

			// for each selected data category
			foreach (get_sub_field('prd_data_data_exchange_data_category') as $data_category) {

				if ($data_category_id == $data_category->ID) {

					$all_data_ids[] = $prd_id;

				}

			}

	    endwhile;

    }

    // removing duplicates of $all_data_ids array
    $all_data_ids = array_unique($all_data_ids);

    $all_private_data_ids = array();
    $all_semi_public_data = array();
    $all_public_data = array();

    // for each all data ids
    foreach ($all_data_ids as $all_data_id) {

		$visibility = get_field('prd_processing_description_visibility', $all_data_id);

		// if the visibility is set on 'Privaat / privé'
		if ($visibility == 'Privaat / privé') {

			$all_private_data_ids[] = $all_data_id;

		}
		// if the visibility is set on semi-public
		elseif ($visibility == 'Beperkt publiek / publique avec restrictions') {

			$all_semi_public_data[] = array(
    			'id' => $all_data_id,
    			'name' => get_field('prd_processing_description_name' . get_current_language(), $all_data_id),
    			'description' => get_field('prd_processing_description_description' . get_current_language(), $all_data_id),
    			'objective' => get_field('processing_objective_name' . get_current_language(), get_field('prd_processing_description_main_processing_objective', $all_data_id)[0]->ID)
    		);

		}
		// if the visibility is set on public
		else {

			$all_public_data[] = array(
    			'id' => $all_data_id,
    			'name' => get_field('prd_processing_description_name' . get_current_language(), $all_data_id),
    			'description' => get_field('prd_processing_description_description' . get_current_language(), $all_data_id),
    			'objective' => get_field('processing_objective_name' . get_current_language(), get_field('prd_processing_description_main_processing_objective', $all_data_id)[0]->ID)
    		);

		}

    }

    if (count($all_public_data) > 0) {

    	// removing duplicates
    	$all_public_data = array_map('unserialize', array_unique(array_map('serialize', $all_public_data)));

    	$final_data[] = array(
    		'beneficiary_name' => get_custom_label('data_page_all_beneficiaries_item'),
    		'visibility' => 'Publiek / publique',
    		'private_data_amount' => 0,
    		'prd_data' => $all_public_data
    	);

    }

    if (count($all_semi_public_data) > 0) {

    	// removing duplicates
    	$all_semi_public_data = array_map('unserialize', array_unique(array_map('serialize', $all_semi_public_data)));

    	$final_data[] = array(
    		'beneficiary_name' => get_custom_label('data_page_all_beneficiaries_item'),
    		'visibility' => 'Beperkt publiek / publique avec restrictions',
    		'private_data_amount' => 0,
    		'prd_data' => $all_semi_public_data
    	);

    }

    if (count($all_private_data_ids) > 0) {

    	$all_private_data_ids = array_unique($all_private_data_ids);

    	$final_data[] = array(
    		'beneficiary_name' => get_custom_label('data_page_all_beneficiaries_item'),
    		'visibility' => 'Privaat / privé',
    		'private_data_amount' => count($all_private_data_ids),
    		'prd_data' => ''
    	);

    }

    // removing duplicates
    $final_data = array_map('unserialize', array_unique(array_map('serialize', $final_data)));

    // sorting the array
    $collator->sort($final_data);

	// removing duplicates
	$all_data = array_map('unserialize', array_unique(array_map('serialize', $all_data)));

	// sorting the array alphabetically		
	$collator->sort($all_data);

	// gathering of data details for the modal
	$modals_data_details = array();

	foreach ($filtered_prd_ids as $filtered_prd_id) {

		$prd_id = $filtered_prd_id;
		$prd_name = get_field('prd_processing_description_name' . get_current_language(), $prd_id);
		$prd_description = get_field('prd_processing_description_description' . get_current_language(), $prd_id);
		$prd_main_objective = get_field('processing_objective_name' . get_current_language(), get_field('prd_processing_description_main_processing_objective', $prd_id)[0]->ID);

		$prd_authority;

		// checking whether the authority is a local police one
		$taxonomies_police = get_the_terms(get_field('prd_identification_authority', $prd_id)[0]->ID, "authority_category")[0];

		$slug_police = '';

		foreach ($taxonomies_police as $key_police => $value_police) {

			if ($key_police == "slug") {
				$slug_police = $value_police;
			}

		}
 
		if ($slug_police == "authority-category-localpolice") {

			$prd_authority = array(
				'authority_name' => (get_current_language() == '_nl' ? "Politie" : "Police"),
				'authority_link' => get_permalink(get_page_by_title((get_current_language() == '_nl' ? "Politie" : "Police"), OBJECT, 'page')->ID)
			);

		}
		else {

			$prd_authority = array(
				'authority_name' => get_field('authority_post_alias' . get_current_language(), get_field('prd_identification_authority', $prd_id)[0]->ID),
				'authority_link' => get_permalink(get_page_by_title(get_field('authority_post_alias' . get_current_language(), get_field('prd_identification_authority', $prd_id)[0]->ID), OBJECT, 'page')->ID)
			);

		}

		
		$prd_visibility = get_field('prd_processing_description_visibility', $prd_id);

		$prd_involved_parties_categories = array();

		foreach (get_field('prd_data_involved_parties_categories', $prd_id) as $prd_involved_parties_categories_data) {
			$prd_involved_parties_categories[] = get_field('involved_party' . get_current_language(), $prd_involved_parties_categories_data->ID);
		}

		$collator->sort($prd_involved_parties_categories);

		$details_per_region = get_details_per_region($prd_id);

		$not_shared_data = $details_per_region[0];
		$belgium_data = $details_per_region[1];
		$europe_data = $details_per_region[2];
		$world_data = $details_per_region[3];
		$belgium_beneficiaries = $details_per_region[4];
		$europe_beneficiaries = $details_per_region[5];
		$world_beneficiaries = $details_per_region[6];

		$modals_data_details[] = array(
			'id' => $prd_id,
			'name' => $prd_name,
			'description' => $prd_description,
			'main_objective' => $prd_main_objective,
			'visibility' => $prd_visibility,
			'authority' => $prd_authority,
			'involved_parties_categories' => $prd_involved_parties_categories,
			'not_shared_data' => $not_shared_data,
			'belgium_data' => $belgium_data,
			'europe_data' => $europe_data,
			'world_data' => $world_data,
			'belgium_beneficiaries' => $belgium_beneficiaries,
			'europe_beneficiaries' => $europe_beneficiaries,
			'world_beneficiaries' => $world_beneficiaries
		);

	}

	return array(
		$authorities_ids,
		$beneficiaries,
		$final_data,
		$modals_data_details
	);

}

function get_authority_page_data($authority_id, $slugg) {

	$collator = new Collator('fr_FR.utf8');

	$prd_ids = get_privacy_register_data();

	// this array will include all filtered privacy register data id's
	$filtered_prd_ids = array();

	$total_number_prd_for_that_authority = 0;

	// STEP 1: browse all the privacy register data and keep only the ones where the field prd_identification_authority is the current authority
	// if we are on the "Police" authority page
	if ($slugg == 'police' || $slugg == 'politie') {

		// we need to take all privacy register data where the selected authority is part of the group ""
		foreach ($prd_ids as $prd_id) {

			$taxonomies_police = get_the_terms(get_field('prd_identification_authority', $prd_id)[0]->ID, "authority_category")[0];

			$slug_police = '';

			foreach ($taxonomies_police as $key_police => $value_police) {

				if ($key_police == "slug") {
					$slug_police = $value_police;
				}

			}

			// if the selected authority in the privacy register data is the current one 
			if ($slug_police == "authority-category-localpolice") {

				while (have_rows('prd_data_data_exchange', $prd_id)) : the_row();

		        	$filtered_prd_ids[] = $prd_id;

			    endwhile;

			}

		}
	}
	else {

		foreach ($prd_ids as $prd_id) {

			// if the selected authority in the privacy register data is the current one 
			if (get_field('prd_identification_authority', $prd_id)[0]->ID == $authority_id) {

				while (have_rows('prd_data_data_exchange', $prd_id)) : the_row();

		        	$filtered_prd_ids[] = $prd_id;

			    endwhile;

			}

		}

	}

	// removing duplicates - this array now includes all the privacy register data where the current authority is selected
	$filtered_prd_ids = array_unique($filtered_prd_ids);


	// go through the list of filtered records and get the list of all data category themes and the data categories to be displayed for each theme
	$data_category_themes_and_records = array();

	// for each filtered privacy register data
	foreach ($filtered_prd_ids as $filtered_prd_id) {

		// getting the list of data categories selected in the "prd_data_data_exchange" repeater ...
		while (have_rows('prd_data_data_exchange', $filtered_prd_id)) : the_row();

	    	// then getting the values in the "prd_data_data_exchange_data_category" relationship field
	        foreach (get_sub_field('prd_data_data_exchange_data_category') as $data) {

	        	// data category id
	        	$data_category_id = $data->ID;

	        	// data category name in current language
	        	$data_category_name = get_field('data_category_name' . get_current_language(), $data_category_id);

        		// data category theme id
        		$data_category_theme_id = get_field('data_category_theme', $data_category_id)[0]->ID;

        		// data category theme name in current language
        		$data_category_theme_name = (get_current_language() == '_nl' ? explode(" / ", get_field('data_category_theme', $data_category_id)[0]->post_title)[0] : explode(" / ", get_field('data_category_theme', $data_category_id)[0]->post_title)[1]);

        		// data category theme color
        		$data_category_theme_color = get_field('data_category_theme_color', $data_category_theme_id);

	        	// storing all data
	    		$data_category_themes_and_records[] = array(
	    			'data_category_id' => $data_category_id,
	    			'data_category_name' => $data_category_name,
					'data_category_theme_id' => $data_category_theme_id,
					'data_category_theme_name' => $data_category_theme_name,
					'data_category_theme_color' => $data_category_theme_color
	    		);

	        }

		endwhile;

	}

	// removing duplicates of the 2D array
	$data_category_themes_and_records = array_map('unserialize', array_unique(array_map('serialize', $data_category_themes_and_records)));

	// removing duplicates from the array of unique data category themes
	$data_category_unique_themes = array_unique(array_map(function ($i) { return $i['data_category_theme_name']; }, $data_category_themes_and_records));

	// sorting the array alphabetically		
	$collator->sort($data_category_unique_themes);

	// based on the unique, alphabetically sorted data category themes array, creating a final array that includes all data category records per data category theme
	$final_data_category_themes_and_records = array();
	$final_data_category_names = array();

	// for each data category theme
	foreach ($data_category_unique_themes as $data_category_unique_theme) {

		$temp_data_category_names = array();
		$data_category_theme_color = '';

		foreach ($data_category_themes_and_records as $data_category_themes_and_record) {

			// if the data category theme is found in the data_category_themes_and_records array
			if ($data_category_unique_theme == $data_category_themes_and_record['data_category_theme_name']) {

				// pushing data category names in a temp array
				$temp_data_category_names[] = $final_data_category_names[] = $data_category_themes_and_record['data_category_name'];

				$data_category_theme_color = $data_category_themes_and_record['data_category_theme_color'];

			}

		}

		// removing duplicates from the temp array
		$temp_data_category_names = array_unique($temp_data_category_names);

		// sorting the temp array alphabetically		
		$collator->sort($temp_data_category_names);

		// storing the data category theme and its related records in the final array
		$final_data_category_themes_and_records[] = array(
			'data_category_theme' => $data_category_unique_theme,
			'data_category_theme_color' => $data_category_theme_color,
			'data_category_names' => $temp_data_category_names
		);

	}

	// removing duplicates from the data category names array
	$final_data_category_names = array_unique($final_data_category_names);

	// sorting the data category names array alphabetically		
	$collator->sort($final_data_category_names);


	$final_data = array();

	// STEP 3: go through the list of filtered records
	foreach($final_data_category_names as $final_data_category_name) {

		$semi_public_data = array();
		$public_data = array();
		$private_data_ids = array();

		foreach ($filtered_prd_ids as $filtered_prd_id) {

			// getting the list of data categories selected in the "prd_data_data_exchange" repeater ...
			while (have_rows('prd_data_data_exchange', $filtered_prd_id)) : the_row();

		    	// ... in the "prd_data_data_exchange_data_category" relationship field
		        foreach (get_sub_field('prd_data_data_exchange_data_category') as $data) {

		        	if ($final_data_category_name == get_field('data_category_name' . get_current_language(), $data->ID)) {

	        			$visibility = get_field('prd_processing_description_visibility', $filtered_prd_id);

	        			// if the visibility is set on 'Privaat / privé'
	        			if ($visibility == 'Privaat / privé') {

		        			$private_data_ids[] = $filtered_prd_id;

	        			}
	        			// if the visibility is set on semi-public
	        			elseif ($visibility == 'Beperkt publiek / publique avec restrictions') {

		        			$semi_public_data[] = array(
			        			'id' => $filtered_prd_id,
			        			'name' => get_field('prd_processing_description_name' . get_current_language(), $filtered_prd_id),
			        			'description' => get_field('prd_processing_description_description' . get_current_language(), $filtered_prd_id),
			        			'objective' => get_field('processing_objective_name' . get_current_language(), get_field('prd_processing_description_main_processing_objective', $filtered_prd_id)[0]->ID)
			        		);

	        			}
	        			// if the visibility is set on public
	        			else {

		        			$public_data[] = array(
			        			'id' => $filtered_prd_id,
			        			'name' => get_field('prd_processing_description_name' . get_current_language(), $filtered_prd_id),
			        			'description' => get_field('prd_processing_description_description' . get_current_language(), $filtered_prd_id),
			        			'objective' => get_field('processing_objective_name' . get_current_language(), get_field('prd_processing_description_main_processing_objective', $filtered_prd_id)[0]->ID)
			        		);

	        			}

		        	}

		        }

			endwhile;

		}

		if (count($public_data) > 0) {

			// removing duplicates
			$public_data = array_map('unserialize', array_unique(array_map('serialize', $public_data)));

			$final_data[] = array(
				'data_category_name' => $final_data_category_name,
				'visibility' => 'Publiek / publique',
				'private_data_amount' => 0,
				'prd_data' => $public_data
			);

		}

		if (count($semi_public_data) > 0) {

			// removing duplicates
			$semi_public_data = array_map('unserialize', array_unique(array_map('serialize', $semi_public_data)));

			$final_data[] = array(
				'data_category_name' => $final_data_category_name,
				'visibility' => 'Beperkt publiek / publique avec restrictions',
				'private_data_amount' => 0,
				'prd_data' => $semi_public_data
			);

		}

		if (count($private_data_ids) > 0) {

			$private_data_ids = array_unique($private_data_ids);

			$final_data[] = array(
				'data_category_name' => $final_data_category_name,
				'visibility' => 'Privaat / privé',
				'private_data_amount' => count($private_data_ids),
				'prd_data' => ''
			);

		}

	}

	$final_data = array_map('unserialize', array_unique(array_map('serialize', $final_data)));

	foreach ($final_data as $final_data_records) {

		foreach ($final_data_records['prd_data'] as $final_data_record) {

			$total_number_prd_for_that_authority++;

		}

	}

	// gathering of data details for the modal
	$modals_data_details = array();

	// for each privacy register data
	foreach ($filtered_prd_ids as $filtered_prd_id) {

		$prd_id = $filtered_prd_id;
		$prd_name = get_field('prd_processing_description_name' . get_current_language(), $prd_id);
		$prd_description = get_field('prd_processing_description_description' . get_current_language(), $prd_id);
		$prd_main_objective = get_field('processing_objective_name' . get_current_language(), get_field('prd_processing_description_main_processing_objective', $prd_id)[0]->ID);

		$prd_authority;

		// checking whether the authority is a loca police one
		$taxonomies_police = get_the_terms(get_field('prd_identification_authority', $prd_id)[0]->ID, "authority_category")[0];

		$slug_police = '';

		foreach ($taxonomies_police as $key_police => $value_police) {

			if ($key_police == "slug") {
				$slug_police = $value_police;
			}

		}
		
		if ($slug_police == "authority-category-localpolice") {

			$prd_authority = array(
				'authority_name' => (get_current_language() == '_nl' ? "Politie" : "Police"),
				'authority_link' => get_permalink(get_page_by_title((get_current_language() == '_nl' ? "Politie" : "Police"), OBJECT, 'page')->ID)
			);

		}
		else {

			$prd_authority = array(
				'authority_name' => get_field('authority_post_alias' . get_current_language(), get_field('prd_identification_authority', $prd_id)[0]->ID),
				'authority_link' => get_permalink(get_page_by_title(get_field('authority_post_alias' . get_current_language(), get_field('prd_identification_authority', $prd_id)[0]->ID), OBJECT, 'page')->ID)
			);

		}

		
		$prd_visibility = get_field('prd_processing_description_visibility', $prd_id);

		// involved parties
		$prd_involved_parties_categories = array();

		foreach (get_field('prd_data_involved_parties_categories', $prd_id) as $prd_involved_parties_categories_data) {
			$prd_involved_parties_categories[] = get_field('involved_party' . get_current_language(), $prd_involved_parties_categories_data->ID);
		}

		$collator->sort($prd_involved_parties_categories);

		$details_per_region = get_details_per_region($prd_id);

		$not_shared_data = $details_per_region[0];
		$belgium_data = $details_per_region[1];
		$europe_data = $details_per_region[2];
		$world_data = $details_per_region[3];
		$belgium_beneficiaries = $details_per_region[4];
		$europe_beneficiaries = $details_per_region[5];
		$world_beneficiaries = $details_per_region[6];

		$modals_data_details[] = array(
			'id' => $prd_id,
			'name' => $prd_name,
			'description' => $prd_description,
			'main_objective' => $prd_main_objective,
			'visibility' => $prd_visibility,
			'authority' => $prd_authority,
			'involved_parties_categories' => $prd_involved_parties_categories,
			'not_shared_data' => $not_shared_data,
			'belgium_data' => $belgium_data,
			'europe_data' => $europe_data,
			'world_data' => $world_data,
			'belgium_beneficiaries' => $belgium_beneficiaries,
			'europe_beneficiaries' => $europe_beneficiaries,
			'world_beneficiaries' => $world_beneficiaries
		);

	}

	return array(
		$data_category_themes_and_records,
		$total_number_prd_for_that_authority,
		$final_data_category_themes_and_records,
		$final_data,
		$modals_data_details
	);

}

function get_beneficiary_page_data($beneficiary_id) {

	$collator = new Collator('fr_FR.utf8');

	$prd_ids = get_privacy_register_data();

	// this array will include all filtered privacy register data id's
	$filtered_prd_ids = array();

	// for each pr data
	foreach ($prd_ids as $prd_id) {

		// keeping only the privacy register data where the current beneficiary is selected in one of the rows of the repeater
		while (have_rows('prd_data_data_exchange', $prd_id)) : the_row();

	    	// checking the list of beneficiaries
	        foreach (get_sub_field('prd_data_data_exchange_beneficiaries') as $beneficiary) {

	        	if ($beneficiary_id == $beneficiary->ID) {

	        		$filtered_prd_ids[] = $prd_id;

	        		break;

	        	}

	        }

		endwhile;

	}

	// removing duplicates - this array now includes all the privacy register data where the current beneficiary is selected in one of the rows of the repeater
	$filtered_prd_ids = array_unique($filtered_prd_ids);

	// go through the list of filtered records and get the list of all data category themes and the data categories to be displayed for each theme
	$data_category_themes_and_records = array();

	// for each filtered privacy register data
	foreach ($filtered_prd_ids as $filtered_privacy_register_record) {

		// getting the list of data categories selected in the "prd_data_data_exchange" repeater ...
		while (have_rows('prd_data_data_exchange', $filtered_privacy_register_record)) : the_row();

	    	// then getting the values in the "prd_data_data_exchange_data_category" relationship field
	        foreach (get_sub_field('prd_data_data_exchange_data_category') as $data) {

	        	// data category id
	        	$data_category_id = $data->ID;

	        	// data category name in current language
	        	$data_category_name = get_field('data_category_name' . get_current_language(), $data_category_id);

        		// data category theme id
        		$data_category_theme_id = get_field('data_category_theme', $data_category_id)[0]->ID;

        		// data category theme name in current language
        		$data_category_theme_name = (get_current_language() == '_nl' ? explode(" / ", get_field('data_category_theme', $data_category_id)[0]->post_title)[0] : explode(" / ", get_field('data_category_theme', $data_category_id)[0]->post_title)[1]);

        		// data category theme color
        		$data_category_theme_color = get_field('data_category_theme_color', $data_category_theme_id);

	        	// storing all data
	    		$data_category_themes_and_records[] = array(
	    			'data_category_id' => $data_category_id,
	    			'data_category_name' => $data_category_name,
					'data_category_theme_id' => $data_category_theme_id,
					'data_category_theme_name' => $data_category_theme_name,
					'data_category_theme_color' => $data_category_theme_color
	    		);

	        }

		endwhile;

	}

	// removing duplicates of the 2D array
	$data_category_themes_and_records = array_map('unserialize', array_unique(array_map('serialize', $data_category_themes_and_records)));

	// removing duplicates from the array of unique data category themes
	$data_category_unique_themes = array_unique(array_map(function ($i) { return $i['data_category_theme_name']; }, $data_category_themes_and_records));

	// sorting the array alphabetically		
	$collator->sort($data_category_unique_themes);

	// based on the unique, alphabetically sorted data category themes array, creating a final array that includes all data category records per data category theme
	$final_data_category_themes_and_records = array();
	$final_data_category_names = array();

	// for each data category theme
	foreach ($data_category_unique_themes as $data_category_unique_theme) {

		$temp_data_category_names = array();
		$data_category_theme_color = '';

		foreach ($data_category_themes_and_records as $data_category_themes_and_record) {

			// if the data category theme is found in the data_category_themes_and_records array
			if ($data_category_unique_theme == $data_category_themes_and_record['data_category_theme_name']) {

				// pushing data category names in a temp array
				$temp_data_category_names[] = $final_data_category_names[] = $data_category_themes_and_record['data_category_name'];

				$data_category_theme_color = $data_category_themes_and_record['data_category_theme_color'];

			}

		}

		// removing duplicates from the temp array
		$temp_data_category_names = array_unique($temp_data_category_names);

		// sorting the temp array alphabetically		
		$collator->sort($temp_data_category_names);

		// storing the data category theme and its related records in the final array
		$final_data_category_themes_and_records[] = array(
			'data_category_theme' => $data_category_unique_theme,
			'data_category_theme_color' => $data_category_theme_color,
			'data_category_names' => $temp_data_category_names
		);

	}

	// removing duplicates from the data category names array
	$final_data_category_names = array_unique($final_data_category_names);

	// sorting the data category names array alphabetically		
	$collator->sort($final_data_category_names);


	$final_data = array();

	// STEP 3: go through the list of filtered records
	foreach ($final_data_category_names as $final_data_category_name) {

		$private_data_ids = array();
		$semi_public_data = array();
		$public_data = array();

		foreach ($filtered_prd_ids as $filtered_prd_id) {

			// getting the list of data categories selected in the "prd_data_data_exchange" repeater ...
			while (have_rows('prd_data_data_exchange', $filtered_prd_id)) : the_row();

		    	// ... in the "prd_data_data_exchange_data_category" relationship field
		        foreach (get_sub_field('prd_data_data_exchange_data_category') as $data) {

		        	if ($final_data_category_name == get_field('data_category_name' . get_current_language(), $data->ID)) {

	        			$visibility = get_field('prd_processing_description_visibility', $filtered_prd_id);

	        			// if the visibility is set on 'Privaat / privé'
	        			if ($visibility == 'Privaat / privé') {

		        			$private_data_ids[] = $filtered_prd_id;

	        			}
	        			// if the visibility is set on semi-public
	        			elseif ($visibility == 'Beperkt publiek / publique avec restrictions') {

		        			$semi_public_data[] = array(
			        			'id' => $filtered_prd_id,
			        			'name' => get_field('prd_processing_description_name' . get_current_language(), $filtered_prd_id),
			        			'description' => get_field('prd_processing_description_description' . get_current_language(), $filtered_prd_id),
			        			'objective' => get_field('processing_objective_name' . get_current_language(), get_field('prd_processing_description_main_processing_objective', $filtered_prd_id)[0]->ID)
			        		);

	        			}
	        			// if the visibility is set on public
	        			else {

		        			$public_data[] = array(
			        			'id' => $filtered_prd_id,
			        			'name' => get_field('prd_processing_description_name' . get_current_language(), $filtered_prd_id),
			        			'description' => get_field('prd_processing_description_description' . get_current_language(), $filtered_prd_id),
			        			'objective' => get_field('processing_objective_name' . get_current_language(), get_field('prd_processing_description_main_processing_objective', $filtered_prd_id)[0]->ID)
			        		);

	        			}

		        	}

		        }

			endwhile;

		}

		if (count($public_data) > 0) {

	    	// removing duplicates
	    	$public_data = array_map('unserialize', array_unique(array_map('serialize', $public_data)));

	    	$final_data[] = array(
	    		'data_category_name' => $final_data_category_name,
	    		'visibility' => 'Publiek / publique',
	    		'private_data_amount' => 0,
	    		'prd_data' => $public_data
	    	);

	    }

	    if (count($semi_public_data) > 0) {

	    	// removing duplicates
	    	$semi_public_data = array_map('unserialize', array_unique(array_map('serialize', $semi_public_data)));

	    	$final_data[] = array(
	    		'data_category_name' => $final_data_category_name,
	    		'visibility' => 'Beperkt publiek / publique avec restrictions',
	    		'private_data_amount' => 0,
	    		'prd_data' => $semi_public_data
	    	);

	    }

	    if (count($private_data_ids) > 0) {

	    	$private_data_ids = array_unique($private_data_ids);

	    	$final_data[] = array(
	    		'data_category_name' => $final_data_category_name,
	    		'visibility' => 'Privaat / privé',
	    		'private_data_amount' => count($private_data_ids),
	    		'prd_data' => ''
	    	);

	    }

	}

	$final_data = array_map('unserialize', array_unique(array_map('serialize', $final_data)));

	// gathering of data details for the modal
	$modals_data_details = array();

	foreach ($filtered_prd_ids as $filtered_prd_id) {

		$prd_id = $filtered_prd_id;
		$prd_name = get_field('prd_processing_description_name' . get_current_language(), $prd_id);
		$prd_description = get_field('prd_processing_description_description' . get_current_language(), $prd_id);
		$prd_main_objective = get_field('processing_objective_name' . get_current_language(), get_field('prd_processing_description_main_processing_objective', $prd_id)[0]->ID);

		$prd_authority;

		// checking whether the authority is a loca police one
		$taxonomies_police = get_the_terms(get_field('prd_identification_authority', $prd_id)[0]->ID, "authority_category")[0];

		$slug_police = '';

		foreach ($taxonomies_police as $key_police => $value_police) {

			if ($key_police == "slug") {
				$slug_police = $value_police;
			}

		}
 
		if ($slug_police == "authority-category-localpolice") {

			$prd_authority = array(
				'authority_name' => (get_current_language() == '_nl' ? "Politie" : "Police"),
				'authority_link' => get_permalink(get_page_by_title((get_current_language() == '_nl' ? "Politie" : "Police"), OBJECT, 'page')->ID)
			);

		}
		else {

			$prd_authority = array(
				'authority_name' => get_field('authority_post_alias' . get_current_language(), get_field('prd_identification_authority', $prd_id)[0]->ID),
				'authority_link' => get_permalink(get_page_by_title(get_field('authority_post_alias' . get_current_language(), get_field('prd_identification_authority', $prd_id)[0]->ID), OBJECT, 'page')->ID)
			);

		}

		$prd_visibility = get_field('prd_processing_description_visibility', $prd_id);

		$prd_involved_parties_categories = array();

		foreach (get_field('prd_data_involved_parties_categories', $prd_id) as $prd_involved_parties_categories_data) {
			$prd_involved_parties_categories[] = get_field('involved_party' . get_current_language(), $prd_involved_parties_categories_data->ID);
		}

		$collator->sort($prd_involved_parties_categories);

		$details_per_region = get_details_per_region($prd_id);

		$not_shared_data = $details_per_region[0];
		$belgium_data = $details_per_region[1];
		$europe_data = $details_per_region[2];
		$world_data = $details_per_region[3];
		$belgium_beneficiaries = $details_per_region[4];
		$europe_beneficiaries = $details_per_region[5];
		$world_beneficiaries = $details_per_region[6];

		$modals_data_details[] = array(
			'id' => $prd_id,
			'name' => $prd_name,
			'description' => $prd_description,
			'main_objective' => $prd_main_objective,
			'visibility' => $prd_visibility,
			'authority' => $prd_authority,
			'involved_parties_categories' => $prd_involved_parties_categories,
			'not_shared_data' => $not_shared_data,
			'belgium_data' => $belgium_data,
			'europe_data' => $europe_data,
			'world_data' => $world_data,
			'belgium_beneficiaries' => $belgium_beneficiaries,
			'europe_beneficiaries' => $europe_beneficiaries,
			'world_beneficiaries' => $world_beneficiaries
		);

	}

	return array(
		$data_category_themes_and_records,
		count($filtered_prd_ids),
		$final_data_category_themes_and_records,
		$final_data,
		$modals_data_details
	);

}

function get_graph_data() {

	$graph_data = array();

	$prd_ids = get_privacy_register_data();

	// for each pr data
	foreach ($prd_ids as $prd_id) {

		// getting current authority
		$authority_id = get_field('prd_identification_authority', $prd_id)[0]->ID;

		// checking whether the authority is a local police one
		$taxonomies_police = get_the_terms($authority_id, "authority_category")[0];

		$authority_name = '';
		$slug_police = '';

		foreach ($taxonomies_police as $key_police => $value_police) {

			if ($key_police == "slug") {
				$slug_police = $value_police;
			}

		}
		
		if ($slug_police == "authority-category-localpolice") {

			$authority_name = (get_current_language() == '_nl' ? "Politie" : "Police");

		}
		else {

			$authority_name = get_field('authority_post_alias' . get_current_language(), $authority_id);

		}

		// for each row in the repeater
		while (have_rows('prd_data_data_exchange', $prd_id)) : the_row();

    		// checking the list of selected data categories
	        foreach (get_sub_field('prd_data_data_exchange_data_category') as $data_category) {

        		// data category id
	        	$data_category_id = $data_category->ID;

	        	// data category name in current language
	        	$data_category_name = get_field('data_category_name' . get_current_language(), $data_category_id);

        		// data category theme id
        		$data_category_theme_id = get_field('data_category_theme', $data_category_id)[0]->ID;

        		// data category theme name in current language
        		$data_category_theme_name = (get_current_language() == '_nl' ? explode(" / ", get_field('data_category_theme', $data_category_id)[0]->post_title)[0] : explode(" / ", get_field('data_category_theme', $data_category_id)[0]->post_title)[1]);

        		// data category theme color
        		$data_category_theme_color = get_field('data_category_theme_color', $data_category_theme_id);

        		$graph_data[] = array(
				    "CAND_ID" => $data_category_id,
				    "CAND_NAME" => $data_category_name,
				    "PTY" => $data_category_theme_name,
				    "PTY_COLOR" => $data_category_theme_color,
				    "CMTE_ID" => $authority_id,
				    "CMTE_NM" => $authority_name
				);

	        }

		endwhile;

	}

	// removing duplicates
	$graph_data = array_map('unserialize', array_unique(array_map('serialize', $graph_data)));

	// looping through the array one last time to get the amount per data category
	$graph_data_final = array();

	foreach ($graph_data as $graph_data_records) {

		$amount = 0;

		foreach ($prd_ids as $prd_id) {

			// for each row in the repeater
			while (have_rows('prd_data_data_exchange', $prd_id)) : the_row();

        		// checking the list of selected data categories
		        foreach (get_sub_field('prd_data_data_exchange_data_category') as $data_category) {

	        		// data category id
		        	if ($data_category->ID == $graph_data_records['CAND_ID']) {

		        		$amount++;

		        	}

		        }

		     endwhile;

		}

	    $graph_data_final[] = array(
		    "TRANSACTION_AMT" => $amount,
		    "CAND_ID" => $graph_data_records['CAND_ID'],
		    "CAND_NAME" => $graph_data_records['CAND_NAME'],
		    "PTY" => $graph_data_records['PTY'],
		    "PTY_COLOR" => $graph_data_records['PTY_COLOR'],
		    "CMTE_ID" => $graph_data_records['CMTE_ID'],
		    "CMTE_NM" => $graph_data_records['CMTE_NM']
		);

	}

	return json_encode($graph_data_final);

}

?>