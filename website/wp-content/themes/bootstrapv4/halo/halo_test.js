/*
Copyright (c) 2016, BrightPoint Consulting, Inc.

This source code is covered under the following license: http://vizuly.io/license/

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO
THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS
OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

// @version 1.1.54

//**************************************************************************************************************
//
//  This is a test/example file that shows you one way you could use a vizuly object.
//  We have tried to make these examples easy enough to follow, while still using some more advanced
//  techniques.  Vizuly does not rely on any libraries other than D3.  These examples do use jQuery and
//  materialize.css to simplify the examples and provide a decent UI framework.
//
//**************************************************************************************************************


var viz;            // vizuly ui object
var viz_container;  // html element that holds the viz (d3 selection)
var theme;          // Theme variable to be used by our viz.

// D3 formatters we will use for labels
var formatDate = d3.time.format("%b %d, 20%y");

//Initializes chart and container.
function initialize(data) {

    // Here we set our <code>viz</code> variable by instantiating the <code>vizuly.viz.corona</code> function.
    // All vizuly components require a parent DOM element at initialization so they know where to inject their display elements.
    viz = vizuly.viz.halo_cluster(document.getElementById("viz_container"));

    viz.data(data)
        .width(800).height(600)                     // Initial display size
        .haloKey(function (d) {
            return d.CMTE_ID; })                    // The property that determines each PAC
        .nodeKey(function (d) {
            return d.CAND_ID; })                    // The property that determines Candidate
        .nodeGroupKey(function (d) {
            return d.PTY; })                        // The property that determines candidate Party affiliation
        .value(function (d) {
            return Number(d.TRANSACTION_AMT); })    // The property that determines the weight/size of the link path
        .on("nodeover",node_onMouseOver)            // Callback for mouseover on the candidates
        .on("nodeout",onMouseOut)                   // Callback for mouseout on from the candidate
        .on("arcover",arc_onMouseOver)              // Callback for mouseover on each PAC
        .on("arcout",onMouseOut)                    // Callback for mouseout on each PAC
        .on("linkover",link_onMouseOver)            // Callback for mousover on each contribution
        .on("linkout",onMouseOut)                   // Callback for mouseout on each contribution
        .on("nodeclick",node_onClick)

    theme = vizuly.theme.halo(viz);
    theme.skins()["custom"] = customSkin;
    theme.skin("custom");

    //Update the size of the component
    changeSize(d3.select("#currentDisplay").attr("item_value"));

}

// An example of how you could respond to a node click event
function node_onClick(e,d,i) {
    // console.log("You have just clicked on " + d.values[0].CAND_NAME);
}

// For each mouse over on the node we want to create a datatip that shows information about the candidate
// and any assoicated PACs that have contributed to the candidate.
function node_onMouseOver(e,d,i) {

    // Find all node links (candidates) and create a label for each arc
    var haloLabels = {};
    var links = viz.selection().selectAll(".vz-halo-link-path.node-key_" + d.key);

    //For each link we want to dynamically total the transactions to display them on the datatip.
    links.each(function (d) {

        var halos=viz.selection().selectAll(".vz-halo-arc.halo-key_" + viz.haloKey()(d.data));
        halos.each(function (d) {
            if (!haloLabels[d.data.key]) {
                haloLabels[d.data.key]=1;
                createPacLabel(d.x, d.y,d.data.values[0].CMTE_NM);
            }
        })
    });

    // Create and position the datatip
    var rect = d3.selectAll(".vz-halo-arc-plot")[0][0].getBoundingClientRect();

    var node = viz.selection().selectAll(".vz-halo-node.node-key_" + d.key);

    //Create and position the datatip
    var rect = node[0][0].getBoundingClientRect();

    var x = rect.left;
    var y = rect.top + document.body.scrollTop;

    createDataTip(x + d.r, y + d.r + 25, d.values[0].CAND_NAME);

}

// For each PAC we want to create a datatip that shows the total of all contributions by that PAC
function arc_onMouseOver(e,d,i) {

    //Find all links from a PAC and create totals
    // var links = viz.selection().selectAll(".vz-halo-link-path.halo-key_" + d.data.key);
    // var total = 0;
    // links.each(function (d) {
    //     total += viz.value()(d.data);
    // });

    // //Create and position the datatip
    // var rect = d3.selectAll(".vz-halo-node-plot")[0][0].getBoundingClientRect();
    // createDataTip(d.x + rect.left  + rect.width/2, (d.y + rect.top + 100), d.data.values[0].CMTE_NM);

    d.data.values.forEach(function(data_array) {
        link_onMouseOver(e, {'data' : data_array}, i);
    });

}

// When the user rolls over a link we want to create a lable for the PAC and a data tip for the candidate that the
// contribution went to.
function link_onMouseOver(e,d,i) {

    // find the associated candidate and get values for the datatip
    var cand = viz.selection().selectAll(".vz-halo-node.node-key_" + viz.nodeKey()(d.data));
    var datum = cand.datum();

    // create and position the datatip
    var rect = d3.selectAll(".vz-halo-arc-plot")[0][0].getBoundingClientRect();
    createDataTip(datum.x + rect.left + datum.r, datum.y + datum.r + 25 + rect.top, d.data.CAND_NAME);

    // find the pac and create a label for it.
    var pac = viz.selection().selectAll(".vz-halo-arc.halo-key_" + viz.haloKey()(d.data));
    datum = pac.datum();
    createPacLabel(datum.x, datum.y, datum.data.values[0].CMTE_NM);

}

var datatip = '<div class="halo_tooltip" style="background-opacity:.8"><div class="header2">NAME</div></div>';

// This function uses the above html template to replace values and then creates a new <div> that it appends to the
// document.body.  This is just one way you could implement a data tip.
function createDataTip(x,y,data_category) {

    var html = datatip.replace("NAME", data_category);

    d3.select("#halo_container")
        .append("div")
        .attr("class", "vz-halo-label")
        .style("position", "absolute")
        .style("top", (y - 250) + "px")
        .style("left", (x - 925) + "px")
        .style("opacity",0)
        .html(html)
        .transition().style("opacity",1);

}

// This function creates a highlight label with the PAC name when an associated link or candidate has issued a mouseover
// event.  It uses properties from the skin to determine the specific style of the label.
function createPacLabel(x,y,l) {

    var g = viz.selection().selectAll(".vz-halo-arc-plot").append("g")
        .attr("class","vz-halo-label")
        .style("pointer-events","none")
        .style("opacity",0);

    g.append("text")
        .style("font-size","11px")
        .style("fill",theme.skin().labelColor)
        .style("fill-opacity",.75)
        .attr("text-anchor","middle")
        .attr("x", x)
        .attr("y", y)
        .text(l);

    var rect = g[0][0].getBoundingClientRect();
    g.insert("rect","text")
        .style("shape-rendering","auto")
        .style("fill",theme.skin().labelFill)
        .style("opacity",.75)
        .attr("width",rect.width+12)
        .attr("height",rect.height+12)
        .attr("rx",3)
        .attr("ry",3)
        .attr("x", x-5 - rect.width/2)
        .attr("y", y - rect.height-3);

    g.transition().style("opacity",1);
}

// When we mouse out we want to remove all pac datatips and labels.
function onMouseOut(d,i) {
    d3.selectAll(".vz-halo-label").remove();
}

// Create a skin object that has all of the same properties as the skin objects in the /themes/halo.js vizuly file
var customSkin = {
    name: "custom",
    labelColor: "#FFF",
    labelFill: "#000",
    background_transition: function (selection) {
        // viz.selection().select(".vz-background").transition(1000).style("fill-opacity", 0);
    },
    // Here we set the contribution colors based on the data category color
    link_stroke: function (d, i) {
        return d.data.PTY_COLOR;
    },
    link_fill: function (d, i) {
        return d.data.PTY_COLOR;
    },
    link_fill_opacity:.2,
    link_node_fill_opacity:.5,
    node_stroke: function (d, i) {
        return "#FFF";
    },
    node_over_stroke: function (d, i) {
        return "#FFF";
    },
    // Here we set the candidate colors based on the data category color
    node_fill: function (d, i) {
        return d.values[0].PTY_COLOR;
    },
    arc_stroke: function (d, i) {
        return "#FFF";
    },
    // Here we set the arc contribution colors based on the data category color
    arc_fill: function (d, i) {
        return d.data.PTY_COLOR;
    },
    arc_over_fill: function (d, i) {
        return "#000";
    },
    class: "vz-skin-graph"
}

function changeSize(val) {
    var s = String(val).split(",");
    viz_container.transition().duration(300).style('width', s[0] + 'px').style('height', s[1] + 'px');
    viz.width(Number(s[0])).height(Number(s[1])).update();
    theme.apply();
}
