<?php get_header(); ?>
    
    <?php if (get_post_field('post_name') == 'a-propos' || get_post_field('post_name') == 'over-deze-website') { ?>
        
            <section id="about-introduction-title" style="background: url('<?= get_bloginfo("template_url"); ?>/img/about-intro-title.png') no-repeat center bottom; background-size:contain;height:calc(50vh - 2.5rem);">
                <div class="container">
                    <div class="row align-items-center">
                        <div class="col-12 col-sm-10 offset-sm-1 col-md-8 offset-md-2 col-lg-6 offset-lg-3 text-center">
                            <h2 class="mb-4 mb-sm-5"><?php echo get_custom_label('about_introduction_title'); ?></h2>
                            <p class="mb-5"><?php echo get_custom_label('about_introduction_text'); ?></p>
                        </div>
                    </div>
                </div>
            </section>

            <section id="about-introduction-cta">
                <div class="container">
                    <div class="row align-items-center">
                        <div class="col-12 col-sm-10 offset-sm-1 col-md-8 offset-md-2 col-lg-6 offset-lg-3 text-center py-3">
                            <p class="mb-0 text-white"><?php echo get_custom_label('about_introduction_cta_text'); ?></p>
                        </div>
                        <div class="col-12 text-center py-2">
                            <a href="#about-gpdr" onclick="scrollToSection('#about-gpdr');"><i class="fa fa-angle-down fa-2x text-white"></i></a>
                        </div>
                    </div>
                </div>
            </section>

            <section id="about-gpdr">
                <div class="container">
                    <div class="row align-items-center" style="background: url('<?= get_bloginfo("template_url"); ?>/img/about-gpdr.png') no-repeat center bottom; background-size:contain;height:calc(100vh - 5rem);">
                        <div class="col-12 col-sm-10 offset-sm-1 col-md-8 offset-md-2 text-center py-2">
                            <h2><?php echo get_custom_label('about_gpdr_title'); ?></h2>
                        </div>
                        <div class="col-12 col-sm-10 offset-sm-1 col-md-8 offset-md-2 text-center">
                            <h5 class="dark-color mb-0"><?php echo get_custom_label('about_gpdr_subtitle'); ?></h5>
                        </div>
                        <div class="col-12 col-sm-10 offset-sm-1 col-md-8 offset-md-2 text-center">
                            <p class="mb-0"><?php echo get_custom_label('about_gpdr_paragraph_1'); ?></p>
                        </div>
                        <div class="d-none d-sm-block col-sm-10 offset-sm-1 col-md-8 offset-md-2 text-center">
                            <p class="mb-0"><?php echo get_custom_label('about_gpdr_paragraph_2'); ?></p>
                        </div>
                        <div class="col-12 text-center mt-3 mb-auto">
                            <button type="button" class="btn btn-secondary" data-link="<?php echo get_custom_label('about_gpdr_button_link'); ?>" onclick="goToLink(this);"><?php echo get_custom_label('about_gpdr_button'); ?></button>
                        </div>
                        <div class="col-12 text-center py-2">
                            <a href="#about-rights" onclick="scrollToSection('#about-rights');"><i class="fa fa-angle-down fa-2x dark-color"></i></a>
                        </div>
                    </div>
                </div>
            </section>

            <section id="about-rights">
                <div class="container">
                    <div class="row align-items-center">
                        <div id="about-rights-title" class="col-12 col-sm-10 offset-sm-1 text-center py-2 d-flex align-items-center">
                            <h2 class="text-white"><?php echo get_custom_label('about_rights_title'); ?></h2>
                        </div>
                        <div class="col-12 col-sm-10 offset-sm-1 col-md-8 offset-md-2 col-lg-6 offset-lg-3 text-center">
                            <div id="about-rights-carousel" class="carousel slide" data-ride="carousel">
                                <!-- <a class="carousel-control-prev" href="#carouselExampleIndicators" role="button" data-slide="prev">
                                    <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                                    <span class="sr-only">Previous</span>
                                </a>
                                <a class="carousel-control-next" href="#carouselExampleIndicators" role="button" data-slide="next">
                                    <span class="carousel-control-next-icon" aria-hidden="true"></span>
                                    <span class="sr-only">Next</span>
                                </a> -->
                                <div class="carousel-inner">
                                    <div class="carousel-item active text-center">
                                        <img class="" height="75px" src="<?= get_bloginfo("template_url"); ?>/img/about-info.png" alt="">
                                        <h5 class="text-white mt-4 mb-4"><?php echo get_custom_label('about_rights_title_slide_1'); ?></h5>
                                        <p class="text-white"><?php echo get_custom_label('about_rights_description_slide_1'); ?></p>
                                    </div>
                                    <div class="carousel-item text-center">
                                        <img class="" height="75px" src="<?= get_bloginfo("template_url"); ?>/img/about-access.png" alt="">
                                        <h5 class="text-white mt-4 mb-4"><?php echo get_custom_label('about_rights_title_slide_2'); ?></h5>
                                        <p class="text-white"><?php echo get_custom_label('about_rights_description_slide_2'); ?></p>
                                    </div>
                                    <div class="carousel-item text-center">
                                        <img class="" height="75px" src="<?= get_bloginfo("template_url"); ?>/img/about-magnify.png" alt="">
                                        <h5 class="text-white mt-4 mb-4"><?php echo get_custom_label('about_rights_title_slide_3'); ?></h5>
                                        <p class="text-white"><?php echo get_custom_label('about_rights_description_slide_3'); ?></p>
                                    </div>
                                    <div class="carousel-item text-center">
                                        <img class="" height="75px" src="<?= get_bloginfo("template_url"); ?>/img/about-delete.png" alt="">
                                        <h5 class="text-white mt-4 mb-4"><?php echo get_custom_label('about_rights_title_slide_4'); ?></h5>
                                        <p class="text-white"><?php echo get_custom_label('about_rights_description_slide_4'); ?></p>
                                    </div>
                                    <div class="carousel-item text-center">
                                        <img class="" height="75px" src="<?= get_bloginfo("template_url"); ?>/img/about-mobility.png" alt="">
                                        <h5 class="text-white mt-4 mb-4"><?php echo get_custom_label('about_rights_title_slide_5'); ?></h5>
                                        <p class="text-white"><?php echo get_custom_label('about_rights_description_slide_5'); ?></p>
                                    </div>
                                </div>
                                <ol class="carousel-indicators mx-0">
                                    <li data-target="#about-rights-carousel" data-slide-to="0" class="active mx-4">
                                        <img src="<?= get_bloginfo("template_url"); ?>/img/about-info.png" alt="">
                                    </li>
                                    <li data-target="#about-rights-carousel" data-slide-to="1" class="mx-4">
                                        <img src="<?= get_bloginfo("template_url"); ?>/img/about-access.png" alt="">
                                    </li>
                                    <li data-target="#about-rights-carousel" data-slide-to="2" class="mx-4">
                                        <img src="<?= get_bloginfo("template_url"); ?>/img/about-magnify.png" alt="">
                                    </li>
                                    <li data-target="#about-rights-carousel" data-slide-to="3" class="mx-4">
                                        <img src="<?= get_bloginfo("template_url"); ?>/img/about-delete.png" alt="">
                                    </li>
                                    <li data-target="#about-rights-carousel" data-slide-to="4" class="mx-4">
                                        <img src="<?= get_bloginfo("template_url"); ?>/img/about-mobility.png" alt="">
                                    </li>
                                </ol>
                            </div>  
                        </div>
                        <div id="about-rights-bottom" class="col-12 text-center py-2">
                            <a href="#about-public" onclick="scrollToSection('#about-public');"><i class="fa fa-angle-down fa-2x text-white"></i></a>
                        </div>
                    </div>
            </section>

            <section id="about-public">
                <div class="container">
                    <div class="row align-items-center">
                        <div class="col-12 col-sm-10 offset-sm-1 col-md-8 offset-md-2 text-center py-2">
                            <h2><?php echo get_custom_label('about_public_title'); ?></h2>
                        </div>
                        <div class="col-12 col-sm-10 offset-sm-1 col-md-8 offset-md-2 text-center">
                            <h5 class="dark-color mb-0"><?php echo get_custom_label('about_public_subtitle'); ?></h5>
                        </div>
                        <div class="col-12 col-sm-10 offset-sm-1 col-md-8 offset-md-2 text-center">
                            <p class="mb-3"><?php echo get_custom_label('about_public_text'); ?></p>
                        </div>
                        <div class="col-12 text-center py-2">
                            <a href="#about-dpo" onclick="scrollToSection('#about-dpo');"><i class="fa fa-angle-down fa-2x dark-color"></i></a>
                        </div>
                    </div>
                </div>
            </section>

            <section id="about-dpo">
                <div class="container">
                    <div class="row align-items-center">
                        <div class="col-12 col-sm-10 offset-sm-1 col-md-8 offset-md-2 text-center py-2">
                            <h2><?php echo get_custom_label('about_dpo_title'); ?></h2>
                        </div>
                        <div class="col-12 col-sm-6 offset-sm-3 col-md-4 offset-md-4 col-lg-2 offset-lg-5 text-center">
                            <img class="img-fluid" style="max-height: 130px" src="<?= get_bloginfo("template_url"); ?>/img/about-dpo.png" alt="" />
                        </div>
                        <div class="col-12 col-sm-10 offset-sm-1 col-md-8 offset-md-2 text-center">
                            <h5 class="dark-color mb-0"><?php echo get_custom_label('about_dpo_subtitle'); ?></h5>
                        </div>
                        <div class="col-12 col-sm-10 offset-sm-1 col-md-8 offset-md-2 text-center mb-auto">
                            <p class="mb-3"><?php echo get_custom_label('about_dpo_text'); ?></p>
                        </div>
                        <div class="col-12 text-center py-2">
                            <a href="#about-contact" onclick="scrollToSection('#about-contact');"><i class="fa fa-angle-down fa-2x dark-color"></i></a>
                        </div>
                    </div>
                </div>
            </section>

            <section id="about-contact">
                <div class="container">
                    <div class="row align-items-center">
                        <div class="col-12 col-sm-10 offset-sm-1 col-md-8 offset-md-2 text-center py-2">
                            <h2><?php echo get_custom_label('about_contact_first_question'); ?></h2>
                        </div>
                        <div class="col-12 col-sm-10 offset-sm-1 col-md-8 offset-md-2 text-center mb-auto">
                            <p class="mb-3"><?php echo get_custom_label('about_contact_first_answer'); ?></p>
                            <h5 class="dark-color my-5"><?php echo get_custom_label('about_contact_second_question'); ?></h5>
                            <p class="mb-3"><?php echo get_custom_label('about_contact_second_answer'); ?></p>
                        </div>
                    </div>
                </div>
            </section>

    <?php } elseif (get_post_field('post_name') == 'accueil' || get_post_field('post_name') == 'hoofdpagina') { ?>
        
        <div id="currentDisplay" class="d-none" item_value=""></div>

        <section id="homepage-introduction">
            <div class="container-fluid" style="background: url('<?= get_bloginfo("template_url"); ?>/img/homepage-intro.png') no-repeat center bottom;height:calc(100vh - 5rem);background-size: 100%;">
                <div class="container">
                    <div class="row align-items-end" style="height:calc(100vh - 5rem);">
                        <div class="col-12 col-sm-10 offset-sm-1 col-md-8 offset-md-2 text-center">
                            <div class="row">
                                <div class="col-12">
                                    <h1 class="text-white mb-4 mb-sm-5"><?php echo get_custom_label('homepage_introduction_title'); ?></h1>
                                    <p class="mb-0"><?php echo get_custom_label('homepage_introduction_text'); ?></p>  
                                </div>
                                <div class="col-12">
                                    <div class="row d-sm-none mt-5">
                                        <div class="col-12 text-center text-uppercase mb-3 homepage-search-text">
                                            <?php echo get_custom_label('homepage_introduction_search_text'); ?>
                                        </div>
                                        <div class="col-12 text-center">
                                            <button type="button" class="btn btn-primary mb-3" onclick="scrollToSection('#homepage-data-categories');"><?php echo get_custom_label('homepage_introduction_data_search_button'); ?></button>
                                        </div>
                                        <div class="col-12 text-center">
                                            <button type="button" class="btn btn-primary mb-3" onclick="scrollToSection('#homepage-authorities');"><?php echo get_custom_label('homepage_introduction_authorities_button'); ?></button>
                                        </div>
                                    </div>
                                    <div class="row d-none d-sm-flex">
                                        <div class="col-12 text-center text-uppercase mt-5 mb-3 homepage-search-text">
                                            <?php echo get_custom_label('homepage_introduction_search_text'); ?>
                                        </div>
                                        <div class="col-12 text-center">
                                            <button type="button" class="btn btn-primary mx-2 mb-5" onclick="scrollToSection('#homepage-data-categories');"><?php echo get_custom_label('homepage_introduction_data_search_button'); ?></button>
                                            <button type="button" class="btn btn-primary mx-2 mb-5" onclick="scrollToSection('#homepage-authorities');"><?php echo get_custom_label('homepage_introduction_authorities_button'); ?></button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-12 text-center py-3">
                            <a href="#homepage-data" onclick="scrollToSection('#homepage-data');"><i class="fa fa-angle-down fa-2x text-white"></i></a>
                        </div>
                    </div>
                </div>
            </h1>
        </section>

        <section class="container-fluid" id="homepage-data">
            <div class="row align-items-center" style="background: url('<?= get_bloginfo("template_url"); ?>/img/homepage-data.png') no-repeat center bottom; background-size:contain;height:calc(100vh - 5rem);">
                <div class="col-12 col-md-10 offset-md-1 col-lg-8 offset-lg-2 col-xl-6 offset-xl-3 text-center">
                    <h2 class="mb-4 mb-sm-5"><?php echo get_custom_label('homepage_data_title'); ?></h2>
                    <p class="mb-0"><?php echo get_custom_label('homepage_data_text'); ?></p>
                </div>
                <div class="col-12 text-center">
                    <div class="mb-5" id="homepage-data-number"><?php echo count(get_privacy_register_data()); ?></div>
                    <p id="homepage-data-registered"><?php echo get_custom_label('homepage_data_registered'); ?></p>
                </div>
                <div class="col-12 text-center mb-auto">
                    <a role="button" class="btn btn-primary mb-3" href="<?php echo get_permalink(get_page_by_path((get_current_language() == "_nl" ? 'over-deze-website': 'a-propos'), OBJECT, 'page')->ID); ?>"><?php echo get_custom_label('homepage_data_more_info_button'); ?></a>
                </div>
            </div>
        </section>

        <section class="container-fluid" id="homepage-data-categories">
            <div class="row align-items-center">
                <div class="col-12 pl-0 pr-0">
                    <div id="homepage-data-categories-carousel" class="carousel slide" data-ride="carousel">
                        <?php $homepage_data_categories = get_data_categories_records_per_theme(); ?>
                        <div class="carousel-inner">
                            <?php $loops = 0; ?>
                            <?php foreach ($homepage_data_categories as $homepage_data_category) { ?>
                                <div class="carousel-item<?php echo ($loops == 0 ? ' active' : ''); ?> text-center" style="background-color: <?php echo $homepage_data_category['color']; ?>">
                                    <div class="carousel-title pt-5" style="background: url('<?php echo $homepage_data_category['home_image']; ?>') no-repeat center bottom; background-size:50%">
                                        <h2 class="pt-auto text-center text-white"><?php echo sprintf(get_custom_label('homepage_data_categories_title'), "<b>" . strtolower($homepage_data_category['label']) . "</b>"); ?></h2>    
                                    </div>
                                    <div class="data-categories">
                                        <?php foreach ($homepage_data_category['data'] as $data_category) { ?>
                                        <p class="mb-0 text-left text-white">
                                            <a href="<?php echo $data_category['link']; ?>"><?php echo $data_category['name']; ?></a>
                                        </p>
                                        <?php } ?>
                                    </div>
                                </div>
                            <?php $loops++; ?>
                            <?php } ?>
                        </div>
                        <a class="carousel-control-prev" href="#homepage-data-categories-carousel" role="button" data-slide="prev">
                            <span class="px-3 py-2" style="background-color: #3a3a3a">
                                <i class="fa fa-angle-left fa-3x"></i>
                            </span>
                            <span class="sr-only">Previous</span>
                        </a>
                        <a class="carousel-control-next" href="#homepage-data-categories-carousel" role="button" data-slide="next">
                            <span class="px-3 py-2" style="background-color: #3a3a3a">
                                <i class="fa fa-angle-right fa-3x"></i>
                            </span>
                            <span class="sr-only">Next</span>
                        </a>
                    </div>  
                </div>
            </div>
        </section>

        <section class="container-fluid" id="homepage-authorities">
            <div class="container">
                <div class="row align-items-center" id="homepage-authorities-title">
                    <div class="col-12 col-sm-10 offset-sm-1 col-md-8 offset-md-2 text-center">
                        <h2 class="py-3"><?php echo get_custom_label('homepage_authorities_title'); ?></h2>
                        <div class="mb-2 mb-sm-3 mx-sm-5" id="homepage-authorities-picklist-container">
                            <select id="homepage-authorities-picklist">
                                <?php foreach (get_authorities_categories_records_per_category() as $key => $authority_category) { ?>
                                    <option data-target="#homepage-authorities-tab-<?php echo $authority_category['id']; ?>" <?php echo ($key == 0 ? 'selected' : ''); ?>><?php echo $authority_category['label']; ?></option>
                                <?php } ?>
                            </select>
                        </div>
                    </div>
                </div>
                <div class="row" id="homepage-authorities-list">
                    <div class="col-12 text-center">
                        <ul class="nav nav-tabs d-none" role="tablist">
                            <?php foreach (get_authorities_categories() as $key1 => $authority_category) { ?>
                            <li class="nav-item">
                                <a class="nav-link<?php echo ($key1 == 0 ? ' active' : ''); ?>" data-toggle="tab" href="#homepage-authorities-tab-<?php echo $authority_category['id']; ?>" role="tab" aria-controls="homepage-authorities-tab-<?php echo $authority_category['id']; ?>" aria-selected="<?php echo ($key1 == 0 ? 'true' : 'false'); ?>"></a>
                            </li>
                            <?php } ?>
                        </ul>
                        <div class="tab-content">
                            <?php foreach (get_authorities_categories_records_per_category() as $key2 => $authority_category) { ?>
                                <div class="tab-pane show fade<?php echo ($key2 == 0 ? ' active' : ''); ?>" id="homepage-authorities-tab-<?php echo $authority_category['id']; ?>">
                                    <div class="row">
                                        <?php foreach ($authority_category['data'] as $authority_category_data) { ?>
                                            <div class="col-6 col-md-4 col-lg-3 col-xl-2 mb-4">
                                                <div class="card bg-white h-100">
                                                    <div class="card-body d-flex align-items-center h-100">
                                                        <a href="<?php echo $authority_category_data['link']; ?>">
                                                            <img class="card-img-top" src="<?php echo $authority_category_data['logo']; ?>" alt="<?php echo $authority_category_data['name'][0]; ?>" />
                                                        </a>
                                                    </div>
                                                </div>
                                            </div>
                                        <?php } ?>
                                    </div>
                                </div>
                            <?php } ?>
                        </div>
                    </div>
                </div>
            </div>
        </section>

        <section class="container-fluid">
            <div class="container" id="homepage-graph">
                <div class="row align-items-center">
                    <div class="col-12 col-lg-6 text-center text-lg-left">
                        <h2 class="my-3 my-lg-5">
                            <?php echo get_custom_label('homepage_graph_title'); ?>
                        </h2>
                        <p class="mb-3 mb-lg-5"><?php echo get_custom_label('homepage_graph_text'); ?></p>
                        <a class="btn btn-primary mb-5" href="<?php echo get_permalink(get_page_by_path((get_current_language() == "_nl" ? 'over-deze-website': 'a-propos'), OBJECT, 'page')->ID); ?>"><?php echo get_custom_label('homepage_graph_more_info_button'); ?></a>
                    </div>
                    <div id="halo_container" class="col-12 col-lg-6 halo_container">
                        <div id="viz_container"></div>
                    </div>
                </div>
            </div>
        </section>

    <?php } elseif (get_post_field('post_name') == 'declaration-de-confidentialite' || get_post_field('post_name') == 'privacy-verklaring') { ?>
        
        <section id="privacy-introduction">
            <div class="container">
                <div class="row align-items-center">
                    <div class="col-12 col-sm-10 offset-sm-1 col-md-8 offset-md-2 text-center py-2">
                        <h2><?php echo get_custom_label('privacy_introduction_title'); ?></h2>
                    </div>
                    <div class="col-12 col-sm-10 offset-sm-1 col-md-8 offset-md-2 text-center mb-auto">
                        <p class="mb-3"><?php echo get_custom_label('privacy_introduction_text'); ?></p>
                        <h5 class="my-4"><?php echo get_custom_label('privacy_introduction_heading_1'); ?></h5>
                        <p class="mb-3"><?php echo get_custom_label('privacy_introduction_text_2'); ?></p>
                        <h5 class="my-4"><?php echo get_custom_label('privacy_introduction_heading_2'); ?></h5>
                        <p class="mb-3"><?php echo get_custom_label('privacy_introduction_text_3'); ?></p>
                    </div>
                </div>
            </div>

        </section>

    <?php } elseif (get_post_field('post_name') == 'disclaimer') { ?>
        
        <section id="disclaimer-introduction">
            <div class="container">
                <div class="row align-items-center">
                    <div class="col-12 col-sm-10 offset-sm-1 col-md-8 offset-md-2 text-center py-2">
                        <h2><?php echo get_custom_label('disclaimer_introduction_title'); ?></h2>
                    </div>
                    <div class="col-12 col-sm-10 offset-sm-1 col-md-8 offset-md-2 text-center mb-auto">
                        <p class="mb-3"><?php echo get_custom_label('disclaimer_introduction_text'); ?></p>
                    </div>
                </div>
            </div>

        </section>

    <?php } else { 

        // if data category page
        if (get_field('data_category_id', $post->ID) != '') { ?>

            <?php 
                $response_data_category = get_data_page_data(get_field('data_category_id', $post->ID));
                $data_category_bgcolor = get_field('data_category_theme_color', get_post_field('data_category_theme', get_field('data_category_id', $post->ID))[0]);
                $data_category_data_category_image = get_field('data_category_theme_data_category_image', get_post_field('data_category_theme', get_field('data_category_id', $post->ID))[0])['url'];
                $data_category_data_category_image2 = get_field('data_category_theme_data_category_image', get_post_field('data_category_theme', get_field('data_category_id', $post->ID))[0]);
            ?>
            
            <section class="d-sm-none data-page-section-intro container-fluid" style="background: -moz-linear-gradient(top, #ffffff 0%, #ffffff 50%, <?php echo $data_category_bgcolor; ?> 50%, <?php echo $data_category_bgcolor; ?> 100%);background: -webkit-linear-gradient(top, #ffffff 0%,#ffffff 50%,<?php echo $data_category_bgcolor; ?> 50%,<?php echo $data_category_bgcolor; ?> 100%);background: linear-gradient(to bottom, #ffffff 0%,#ffffff 50%,<?php echo $data_category_bgcolor; ?> 50%,<?php echo $data_category_bgcolor; ?> 100%);filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#ffffff', endColorstr='<?php echo $data_category_bgcolor; ?>',GradientType=0 );height:calc(100vh - 5rem);">
                    <div class="row data-category-image" style="background: url(<?php echo $data_category_data_category_image; ?>) no-repeat center center;">
                        <div class="col-12">
                            <div class="row data-page-title-xs align-items-center">
                                <div class="col-12 text-center">
                                    <h5 class="mb-3" style="color: <?php echo get_field('data_category_theme_color', get_post_field('data_category_theme', get_field('data_category_id', $post->ID))[0]); ?>"><?php echo get_field('data_category_theme' . get_current_language(), get_post_field('data_category_theme', get_field('data_category_id', $post->ID))[0]); ?></h5>
                                    <h1><?php echo get_the_title($post->ID); ?></h1>
                                </div>
                            </div>
                            <div class="row data-page-description-xs align-items-end">
                                <div class="col-12 text-center text-white mt-3">
                                    <?php echo get_field('data_category_description' . get_current_language(), get_field('data_category_id', $post->ID)); ?>
                                </div>
                                <div class="col-12 text-center">
                                    <button type="button" class="btn btn-light mt-5 mb-3" onclick="scrollToSection('#data-content');"><?php echo get_custom_label('data_introduction_button'); ?></button>
                                    <p class="mx-3">
                                        <a href="#data-content" onclick="scrollToSection('#data-content')"><i class="fa fa-angle-down fa-2x text-white"></i></a>
                                    </p>
                                </div>
                            </div>  
                        </div>
                    </div>
            </section>
            
            <section class="d-none d-sm-block data-page-section-intro container-fluid" style="background: -moz-linear-gradient(top, #ffffff 0%, #ffffff 50%, <?php echo $data_category_bgcolor; ?> 50%, <?php echo $data_category_bgcolor; ?> 100%);background: -webkit-linear-gradient(top, #ffffff 0%,#ffffff 50%,<?php echo $data_category_bgcolor; ?> 50%,<?php echo $data_category_bgcolor; ?> 100%);background: linear-gradient(to bottom, #ffffff 0%,#ffffff 50%,<?php echo $data_category_bgcolor; ?> 50%,<?php echo $data_category_bgcolor; ?> 100%);filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#ffffff', endColorstr='<?php echo $data_category_bgcolor; ?>',GradientType=0 );height:calc(100vh - 5rem);">
                <div class="row data-category-image" style="background: url(<?php echo $data_category_data_category_image; ?>) no-repeat center center;height:calc(100vh - 5rem);">
                    <div class="col-12">
                        <div class="row data-page-title align-items-center">
                            <div class="col-sm-10 offset-sm-1 text-center">
                                <h5 class="mb-3" style="color: <?php echo get_field('data_category_theme_color', get_post_field('data_category_theme', get_field('data_category_id', $post->ID))[0]); ?>"><?php echo get_field('data_category_theme' . get_current_language(), get_post_field('data_category_theme', get_field('data_category_id', $post->ID))[0]); ?></h5>
                                <h1 class="mb-5"><?php echo get_the_title($post->ID); ?></h1>
                            </div>
                        </div>
                        <div class="row data-page-figures align-content-center">
                            <div class="col-12 d-flex flex-column">
                                <div class="row mt-auto mb-auto align-items-center">
                                    <div class="col-12 col-sm-4 text-center">
                                        <h2 class="text-white"><?php echo count($response_data_category[3]); ?></h2>
                                        <p class="text-white"><?php echo get_custom_label('data_page_data_amount'); ?></p>
                                    </div>
                                    <div class="col-12 col-sm-4 text-center text-white">
                                        <?php echo get_field('data_category_description' . get_current_language(), get_field('data_category_id', $post->ID)); ?>
                                    </div>
                                    <div class="col-12 col-sm-4 text-center">
                                        <h2 class="text-white"><?php echo count($response_data_category[0]); ?></h2>
                                        <p class="text-white"><?php echo get_custom_label('data_page_data_shared_amount'); ?></p>
                                    </div>
                                </div>
                                <div class="row align-content-center">
                                    <div class="col-12 text-center p-2">
                                        <button type="button" class="btn btn-light" onclick="scrollToSection('#data-content');"><?php echo get_custom_label('data_introduction_button'); ?></button>
                                        <!-- <?php echo json_encode($data_category_data_category_image2); ?> -->
                                    </div>
                                    <div class="col-12 text-center py-4">
                                        <a href="#data-content" onclick="scrollToSection('#data-content')"><i class="fa fa-angle-down fa-2x text-white"></i></a>
                                    </div>
                                </div>
                            </div>
                        </div>  
                    </div>
                </div>
            </section>

            <section class="page-section container-fluid" id="data-content">
                
                <div id="data-prd-modal" class="modal" tabindex="-1" role="dialog">
                    <?php foreach ($response_data_category[3] as $prd_details_data) { ?>
                    <?php if ($prd_details_data['visibility'] == 'Publiek / publique') { ?>
                    <div id="data-modal-<?php echo $prd_details_data['id']; ?>" class="modal-dialog modal-lg d-none" role="document">
                        <div class="modal-content">
                            <div class="modal-header">
                                <h5 class="modal-title"><?php echo $prd_details_data['name']; ?></h5><br/>
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                            </div>
                            <div class="modal-body">
                                <h6 class="modal-subtitle mb-3"><?php echo $prd_details_data['description']; ?><br/><br/><span class="text-uppercase objective"><?php echo get_custom_label('modal_subtitle_objective'); ?></span>&nbsp;<span class="post-objective"><?php echo $prd_details_data['main_objective']; ?></span></h6>
                                <ul class="nav nav-tabs" id="data-modal-countries" role="tablist">
                                <?php if (! empty($prd_details_data['not_shared_data'])) { ?>
                                    <li class="nav-item">
                                        <a class="nav-link active" data-toggle="tab" href="#data-content-not-shared-<?php echo $prd_details_data['id']; ?>" role="tab" aria-controls="data-content-not-shared" aria-selected="true"><?php echo get_custom_label('modal_tab_not_shared'); ?></a>
                                    </li>
                                    <?php if (! empty($prd_details_data['belgium_data'])) { ?>
                                        <li class="nav-item">
                                            <a class="nav-link" data-toggle="tab" href="#data-content-belgium-<?php echo $prd_details_data['id']; ?>" role="tab" aria-controls="data-content-belgium" aria-selected="true"><?php echo get_custom_label('modal_tab_belgium'); ?></a>
                                        </li>
                                    <?php } ?>

                                    <?php if (! empty($prd_details_data['europe_data'])) { ?>
                                        <li class="nav-item">
                                            <a class="nav-link" data-toggle="tab" href="#data-content-europe-<?php echo $prd_details_data['id']; ?>" role="tab" aria-controls="data-content-europe" aria-selected="false"><?php echo get_custom_label('modal_tab_europe'); ?></a>
                                        </li>
                                    <?php } ?>

                                    <?php if (! empty($prd_details_data['world_data'])) { ?>
                                        <li class="nav-item">
                                            <a class="nav-link" data-toggle="tab" href="#data-content-world-<?php echo $prd_details_data['id']; ?>" role="tab" aria-controls="data-content-world" aria-selected="false"><?php echo get_custom_label('modal_tab_world'); ?></a>
                                        </li>
                                    <?php } ?>
                                <?php } else { ?>
                                    <?php if (! empty($prd_details_data['belgium_data'])) { ?>
                                        <li class="nav-item">
                                            <a class="nav-link active" data-toggle="tab" href="#data-content-belgium-<?php echo $prd_details_data['id']; ?>" role="tab" aria-controls="data-content-belgium" aria-selected="true"><?php echo get_custom_label('modal_tab_belgium'); ?></a>
                                        </li>
                                    <?php } ?>

                                    <?php if (! empty($prd_details_data['europe_data'])) { ?>
                                        <li class="nav-item">
                                            <a class="nav-link" data-toggle="tab" href="#data-content-europe-<?php echo $prd_details_data['id']; ?>" role="tab" aria-controls="data-content-europe" aria-selected="false"><?php echo get_custom_label('modal_tab_europe'); ?></a>
                                        </li>
                                    <?php } ?>

                                    <?php if (! empty($prd_details_data['world_data'])) { ?>
                                        <li class="nav-item">
                                            <a class="nav-link" data-toggle="tab" href="#data-content-world-<?php echo $prd_details_data['id']; ?>" role="tab" aria-controls="data-content-world" aria-selected="false"><?php echo get_custom_label('modal_tab_world'); ?></a>
                                        </li>
                                    <?php } ?>
                                <?php } ?>
                                </ul>
                                <div class="tab-content">
                                    <?php if (! empty($prd_details_data['not_shared_data'])) { ?>
                                        <div class="tab-pane fade show active" id="data-content-not-shared-<?php echo $prd_details_data['id']; ?>" role="tabpanel">
                                            <div class="row">
                                                <div class="col-12">
                                                    <h5><?php echo get_custom_label('modal_not_shared_tab_title'); ?></h5>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-12 col-lg-6">
                                                    <h6 class="mb-3"><i class="fa fa-bank"></i>&nbsp;&nbsp;<?php echo get_custom_label('modal_owner_heading'); ?></h6>
                                                    <p><a href="<?php echo $prd_details_data['authority']['authority_link']; ?>" class="authority-name"><?php echo $prd_details_data['authority']['authority_name']; ?></a></p>
                                                </div>
                                                <div class="col-12 col-lg-6">
                                                    <h6 class="mb-3"><i class="fa fa-users"></i>&nbsp;&nbsp;<?php echo get_custom_label('modal_involved_parties_heading'); ?></h6>
                                                    <ul>
                                                        <?php foreach($prd_details_data['involved_parties_categories'] as $involved_parties_category) { ?>
                                                            <li><?php echo $involved_parties_category; ?></li>
                                                        <?php } ?>
                                                    </ul>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-12 col-lg-6">
                                                    <h6 class="mb-3"><i class="fa fa-share-alt"></i>&nbsp;&nbsp;<?php echo get_custom_label('modal_data_stored_heading'); ?></h6>
                                                    <?php foreach ($prd_details_data['not_shared_data'] as $not_shared_data) { ?>
                                                        <p class="mb-2">
                                                            <span class="data-category-theme" style="color: <?php echo $not_shared_data['data_category_theme_color']; ?>"><?php echo $not_shared_data['data_category_theme']; ?></span><br/>
                                                            <?php $not_shared_data_category_names = ''; ?>
                                                            <?php foreach($not_shared_data['data_category_names'] as $not_shared_data_category_name) {
                                                                $not_shared_data_category_names .= '<a class="data-category" href="' . $not_shared_data_category_name['link'] . '">' . $not_shared_data_category_name['name'] . '</a>, ';
                                                            }

                                                            echo substr($not_shared_data_category_names, 0, -2);

                                                            ?>

                                                        </p>
                                                    <?php } ?>
                                                </div>
                                            </div>
                                          
                                        </div>
                                        <?php if (! empty($prd_details_data['belgium_data'])) { ?>
                                            <div class="tab-pane fade show" id="data-content-belgium-<?php echo $prd_details_data['id']; ?>" role="tabpanel">
                                                <div class="row">
                                                    <div class="col-12">
                                                        <h5><?php echo get_custom_label('modal_belgium_tab_title'); ?></h5>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-12 col-lg-6">
                                                        <h6 class="mb-3"><i class="fa fa-bank"></i>&nbsp;&nbsp;<?php echo get_custom_label('modal_owner_heading'); ?></h6>
                                                        <p><a href="<?php echo $prd_details_data['authority']['authority_link']; ?>" class="authority-name"><?php echo $prd_details_data['authority']['authority_name']; ?></a></p>
                                                    </div>
                                                    <div class="col-12 col-lg-6">
                                                        <h6 class="mb-3"><i class="fa fa-users"></i>&nbsp;&nbsp;<?php echo get_custom_label('modal_involved_parties_heading'); ?></h6>
                                                        <ul>
                                                            <?php foreach($prd_details_data['involved_parties_categories'] as $involved_parties_category) { ?>
                                                                <li><?php echo $involved_parties_category; ?></li>
                                                            <?php } ?>
                                                        </ul>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-12 col-lg-6">
                                                        <h6 class="mb-3"><i class="fa fa-share-alt"></i>&nbsp;&nbsp;<?php echo get_custom_label('modal_data_heading'); ?></h6>
                                                        <?php foreach ($prd_details_data['belgium_data'] as $belgium_data) { ?>
                                                            <p class="mb-2">
                                                                <span class="data-category-theme" style="color: <?php echo $belgium_data['data_category_theme_color']; ?>"><?php echo $belgium_data['data_category_theme']; ?></span><br/>
                                                                <?php $belgium_data_category_names = ''; ?>
                                                                <?php foreach($belgium_data['data_category_names'] as $belgium_data_category_name) {
                                                                    $belgium_data_category_names .= '<a class="data-category" href="' . $belgium_data_category_name['link'] . '">' . $belgium_data_category_name['name'] . '</a>, ';
                                                                }

                                                                echo substr($belgium_data_category_names, 0, -2);

                                                                ?>

                                                            </p>
                                                        <?php } ?>
                                                    </div>
                                                    <div class="col-12 col-lg-6">
                                                        <h6 class="mb-3"><i class="fa fa-database"></i>&nbsp;&nbsp;<?php echo get_custom_label('modal_beneficiaries_heading'); ?></h6>
                                                        <?php if (count($prd_details_data['belgium_beneficiaries']) == 0) { ?>
                                                        <p><?php echo get_custom_label('modal_no_beneficiaries'); ?></p>
                                                        <?php } else { ?>
                                                        <p> 
                                                            <?php
                                                                $belgium_beneficiaries = '';

                                                                foreach ($prd_details_data['belgium_beneficiaries'] as $beneficiary) {
                                                                    $belgium_beneficiaries .= '<a class="beneficiary" href="' . $beneficiary['link'] . '">' . $beneficiary['name'] . '</a>, ';
                                                                }

                                                                echo substr($belgium_beneficiaries, 0, -2);

                                                            ?>
                                                        </p>
                                                        <?php } ?>
                                                    </div>
                                                </div>
                                              
                                            </div>
                                        <?php } ?>

                                        <?php if (! empty($prd_details_data['europe_data'])) { ?>
                                            <div class="tab-pane fade" id="data-content-europe-<?php echo $prd_details_data['id']; ?>" role="tabpanel">
                                                <div class="row">
                                                    <div class="col-12">
                                                        <h5><?php echo get_custom_label('modal_europe_tab_title'); ?></h5>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-12 col-lg-6">
                                                        <h6 class="mb-3"><i class="fa fa-bank"></i>&nbsp;&nbsp;<?php echo get_custom_label('modal_owner_heading'); ?></h6>
                                                        <p><a href="<?php echo $prd_details_data['authority']['authority_link']; ?>" class="authority-name"><?php echo $prd_details_data['authority']['authority_name']; ?></a></p>
                                                    </div>
                                                    <div class="col-12 col-lg-6">
                                                        <h6 class="mb-3"><i class="fa fa-users"></i>&nbsp;&nbsp;<?php echo get_custom_label('modal_involved_parties_heading'); ?></h6>
                                                        <ul>
                                                            <?php foreach($prd_details_data['involved_parties_categories'] as $involved_parties_category) { ?>
                                                                <li><?php echo $involved_parties_category; ?></li>
                                                            <?php } ?>
                                                        </ul>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-12 col-lg-6">
                                                        <h6 class="mb-3"><i class="fa fa-share-alt"></i>&nbsp;&nbsp;<?php echo get_custom_label('modal_data_heading'); ?></h6>
                                                        <?php foreach($prd_details_data['europe_data'] as $europe_data) { ?>
                                                            <p class="mb-2">
                                                                <span class="data-category-theme" style="color: <?php echo $europe_data['data_category_theme_color']; ?>"><?php echo $europe_data['data_category_theme']; ?></span><br/>
                                                                <?php $europe_data_category_names = ''; ?>
                                                                <?php foreach($europe_data['data_category_names'] as $europe_data_category_name) {
                                                                    $europe_data_category_names .= '<a class="data-category" href="' . $europe_data_category_name['link'] . '">' . $europe_data_category_name['name'] . '</a>, ';
                                                                }

                                                                echo substr($europe_data_category_names, 0, -2);

                                                                ?>

                                                            </p>
                                                        <?php } ?>
                                                        
                                                    </div>
                                                    <div class="col-12 col-lg-6">
                                                        <h6 class="mb-3"><i class="fa fa-database"></i>&nbsp;&nbsp;<?php echo get_custom_label('modal_beneficiaries_heading'); ?></h6>
                                                        <?php if (count($prd_details_data['europe_beneficiaries']) == 0) { ?>
                                                            <p><?php echo get_custom_label('modal_no_beneficiaries'); ?></p>
                                                        <?php } else { ?>
                                                            <p> 
                                                                <?php
                                                                    $europe_beneficiaries = '';

                                                                    foreach ($prd_details_data['europe_beneficiaries'] as $beneficiary) {
                                                                        $europe_beneficiaries .= '<a class="beneficiary" href="' . $beneficiary['link'] . '">' . $beneficiary['name'] . '</a>, ';
                                                                    }
                                                                
                                                                    echo substr($europe_beneficiaries, 0, -2);

                                                                ?>
                                                            </p>
                                                        <?php } ?>
                                                    </div>
                                                </div>
                                              
                                            </div>
                                        <?php } ?>

                                        <?php if (! empty($prd_details_data['world_data'])) { ?>
                                            <div class="tab-pane fade" id="data-content-world-<?php echo $prd_details_data['id']; ?>" role="tabpanel">
                                                <div class="row">
                                                    <div class="col-12">
                                                        <h5><?php echo get_custom_label('modal_world_tab_title'); ?></h5>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-12 col-lg-6">
                                                        <h6 class="mb-3"><i class="fa fa-bank"></i>&nbsp;&nbsp;<?php echo get_custom_label('modal_owner_heading'); ?></h6>
                                                        <p><a href="<?php echo $prd_details_data['authority']['authority_link']; ?>" class="authority-name"><?php echo $prd_details_data['authority']['authority_name']; ?></a></p>
                                                    </div>
                                                    <div class="col-12 col-lg-6">
                                                       <h6 class="mb-3"><i class="fa fa-users"></i>&nbsp;&nbsp;<?php echo get_custom_label('modal_involved_parties_heading'); ?></h6>
                                                       <ul>
                                                           <?php foreach($prd_details_data['involved_parties_categories'] as $involved_parties_category) { ?>
                                                               <li><?php echo $involved_parties_category; ?></li>
                                                           <?php } ?>
                                                       </ul>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-12 col-lg-6">
                                                        <h6 class="mb-3"><i class="fa fa-share-alt"></i>&nbsp;&nbsp;<?php echo get_custom_label('modal_data_heading'); ?></h6>
                                                        <?php foreach($prd_details_data['world_data'] as $world_data) { ?>
                                                            <p class="mb-2">
                                                                <span class="data-category-theme" style="color: <?php echo $world_data['data_category_theme_color']; ?>"><?php echo $world_data['data_category_theme']; ?></span><br/>
                                                                <?php $world_data_category_names = ''; ?>
                                                                <?php foreach($world_data['data_category_names'] as $world_data_category_name) {
                                                                    $world_data_category_names .= '<a class="data-category" href="' . $world_data_category_name['link'] . '">' . $world_data_category_name['name'] . '</a>, ';
                                                                }

                                                                echo substr($world_data_category_names, 0, -2);

                                                                ?>

                                                            </p>
                                                        <?php } ?>
                                                    </div>
                                                    <div class="col-12 col-lg-6">
                                                        <h6 class="mb-3"><i class="fa fa-database"></i>&nbsp;&nbsp;<?php echo get_custom_label('modal_beneficiaries_heading'); ?></h6>
                                                        <?php if (count($prd_details_data['world_beneficiaries']) == 0) { ?>
                                                            <p><?php echo get_custom_label('modal_no_beneficiaries'); ?></p>
                                                        <?php } else { ?>
                                                            <p> 
                                                                <?php
                                                                $world_beneficiaries = '';

                                                                foreach ($prd_details_data['world_beneficiaries'] as $beneficiary) {
                                                                    $world_beneficiaries .= '<a class="beneficiary" href="' . $beneficiary['link'] . '">' . $beneficiary['name'] . '</a>, ';
                                                                }
                                                                
                                                                echo substr($world_beneficiaries, 0, -2);

                                                                ?>
                                                            </p>
                                                        <?php } ?>
                                                        
                                                    </div>
                                                </div>
                                            </div>
                                        <?php } ?>
                                    <?php } else { ?>
                                        <?php if (! empty($prd_details_data['belgium_data'])) { ?>
                                            <div class="tab-pane fade show active" id="data-content-belgium-<?php echo $prd_details_data['id']; ?>" role="tabpanel">
                                                <div class="row">
                                                    <div class="col-12">
                                                        <h5><?php echo get_custom_label('modal_belgium_tab_title'); ?></h5>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-12 col-lg-6">
                                                        <h6 class="mb-3"><i class="fa fa-bank"></i>&nbsp;&nbsp;<?php echo get_custom_label('modal_owner_heading'); ?></h6>
                                                        <p><a href="<?php echo $prd_details_data['authority']['authority_link']; ?>" class="authority-name"><?php echo $prd_details_data['authority']['authority_name']; ?></a></p>
                                                    </div>
                                                    <div class="col-12 col-lg-6">
                                                        <h6 class="mb-3"><i class="fa fa-users"></i>&nbsp;&nbsp;<?php echo get_custom_label('modal_involved_parties_heading'); ?></h6>
                                                        <ul>
                                                            <?php foreach($prd_details_data['involved_parties_categories'] as $involved_parties_category) { ?>
                                                                <li><?php echo $involved_parties_category; ?></li>
                                                            <?php } ?>
                                                        </ul>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-12 col-lg-6">
                                                        <h6 class="mb-3"><i class="fa fa-share-alt"></i>&nbsp;&nbsp;<?php echo get_custom_label('modal_data_heading'); ?></h6>
                                                        <?php foreach ($prd_details_data['belgium_data'] as $belgium_data) { ?>
                                                            <p class="mb-2">
                                                                <span class="data-category-theme" style="color: <?php echo $belgium_data['data_category_theme_color']; ?>"><?php echo $belgium_data['data_category_theme']; ?></span><br/>
                                                                <?php $belgium_data_category_names = ''; ?>
                                                                <?php foreach($belgium_data['data_category_names'] as $belgium_data_category_name) {
                                                                    $belgium_data_category_names .= '<a class="data-category" href="' . $belgium_data_category_name['link'] . '">' . $belgium_data_category_name['name'] . '</a>, ';
                                                                }

                                                                echo substr($belgium_data_category_names, 0, -2);

                                                                ?>

                                                            </p>
                                                        <?php } ?>
                                                    </div>
                                                    <div class="col-12 col-lg-6">
                                                        <h6 class="mb-3"><i class="fa fa-database"></i>&nbsp;&nbsp;<?php echo get_custom_label('modal_beneficiaries_heading'); ?></h6>
                                                        <?php if (count($prd_details_data['belgium_beneficiaries']) == 0) { ?>
                                                        <p><?php echo get_custom_label('modal_no_beneficiaries'); ?></p>
                                                        <?php } else { ?>
                                                        <p> 
                                                            <?php
                                                                $belgium_beneficiaries = '';

                                                                foreach ($prd_details_data['belgium_beneficiaries'] as $beneficiary) {
                                                                    $belgium_beneficiaries .= '<a class="beneficiary" href="' . $beneficiary['link'] . '">' . $beneficiary['name'] . '</a>, ';
                                                                }

                                                                echo substr($belgium_beneficiaries, 0, -2);

                                                            ?>
                                                        </p>
                                                        <?php } ?>
                                                    </div>
                                                </div>
                                            </div>
                                        <?php } ?>

                                        <?php if (! empty($prd_details_data['europe_data'])) { ?>
                                            <div class="tab-pane fade" id="data-content-europe-<?php echo $prd_details_data['id']; ?>" role="tabpanel"> 
                                                <div class="row">
                                                    <div class="col-12">
                                                        <h5><?php echo get_custom_label('modal_europe_tab_title'); ?></h5>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-12 col-lg-6">
                                                        <h6 class="mb-3"><i class="fa fa-bank"></i>&nbsp;&nbsp;<?php echo get_custom_label('modal_owner_heading'); ?></h6>
                                                        <p><a href="<?php echo $prd_details_data['authority']['authority_link']; ?>" class="authority-name"><?php echo $prd_details_data['authority']['authority_name']; ?></a></p>
                                                    </div>
                                                    <div class="col-12 col-lg-6">
                                                        <h6 class="mb-3"><i class="fa fa-users"></i>&nbsp;&nbsp;<?php echo get_custom_label('modal_involved_parties_heading'); ?></h6>
                                                        <ul>
                                                            <?php foreach($prd_details_data['involved_parties_categories'] as $involved_parties_category) { ?>
                                                                <li><?php echo $involved_parties_category; ?></li>
                                                            <?php } ?>
                                                        </ul>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-12 col-lg-6">
                                                        <h6 class="mb-3"><i class="fa fa-share-alt"></i>&nbsp;&nbsp;<?php echo get_custom_label('modal_data_heading'); ?></h6>
                                                        <?php foreach($prd_details_data['europe_data'] as $europe_data) { ?>
                                                            <p class="mb-2">
                                                                <span class="data-category-theme" style="color: <?php echo $europe_data['data_category_theme_color']; ?>"><?php echo $europe_data['data_category_theme']; ?></span><br/>
                                                                <?php $europe_data_category_names = ''; ?>
                                                                <?php foreach($europe_data['data_category_names'] as $europe_data_category_name) {
                                                                    $europe_data_category_names .= '<a class="data-category" href="' . $europe_data_category_name['link'] . '">' . $europe_data_category_name['name'] . '</a>, ';
                                                                }

                                                                echo substr($europe_data_category_names, 0, -2);

                                                                ?>

                                                            </p>
                                                        <?php } ?>
                                                        
                                                    </div>
                                                    <div class="col-12 col-lg-6">
                                                        <h6 class="mb-3"><i class="fa fa-database"></i>&nbsp;&nbsp;<?php echo get_custom_label('modal_beneficiaries_heading'); ?></h6>
                                                        <?php if (count($prd_details_data['europe_beneficiaries']) == 0) { ?>
                                                            <p><?php echo get_custom_label('modal_no_beneficiaries'); ?></p>
                                                        <?php } else { ?>
                                                            <p> 
                                                                <?php
                                                                    $europe_beneficiaries = '';

                                                                    foreach ($prd_details_data['europe_beneficiaries'] as $beneficiary) {
                                                                        $europe_beneficiaries .= '<a class="beneficiary" href="' . $beneficiary['link'] . '">' . $beneficiary['name'] . '</a>, ';
                                                                    }
                                                                
                                                                    echo substr($europe_beneficiaries, 0, -2);

                                                                ?>
                                                            </p>
                                                        <?php } ?>
                                                    </div>
                                                </div>
                                            </div>
                                        <?php } ?>

                                        <?php if (! empty($prd_details_data['world_data'])) { ?>
                                            <div class="tab-pane fade" id="data-content-world-<?php echo $prd_details_data['id']; ?>" role="tabpanel">
                                                <div class="row">
                                                    <div class="col-12">
                                                        <h5><?php echo get_custom_label('modal_world_tab_title'); ?></h5>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-12 col-lg-6">
                                                        <h6 class="mb-3"><i class="fa fa-bank"></i>&nbsp;&nbsp;<?php echo get_custom_label('modal_owner_heading'); ?></h6>
                                                        <p><a href="<?php echo $prd_details_data['authority']['authority_link']; ?>" class="authority-name"><?php echo $prd_details_data['authority']['authority_name']; ?></a></p>
                                                    </div>
                                                    <div class="col-12 col-lg-6">
                                                       <h6 class="mb-3"><i class="fa fa-users"></i>&nbsp;&nbsp;<?php echo get_custom_label('modal_involved_parties_heading'); ?></h6>
                                                       <ul>
                                                           <?php foreach($prd_details_data['involved_parties_categories'] as $involved_parties_category) { ?>
                                                               <li><?php echo $involved_parties_category; ?></li>
                                                           <?php } ?>
                                                       </ul>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-12 col-lg-6">
                                                        <h6 class="mb-3"><i class="fa fa-share-alt"></i>&nbsp;&nbsp;<?php echo get_custom_label('modal_data_heading'); ?></h6>
                                                        <?php foreach($prd_details_data['world_data'] as $world_data) { ?>
                                                            <p class="mb-2">
                                                                <span class="data-category-theme" style="color: <?php echo $world_data['data_category_theme_color']; ?>"><?php echo $world_data['data_category_theme']; ?></span><br/>
                                                                <?php $world_data_category_names = ''; ?>
                                                                <?php foreach($world_data['data_category_names'] as $world_data_category_name) {
                                                                    $world_data_category_names .= '<a class="data-category" href="' . $world_data_category_name['link'] . '">' . $world_data_category_name['name'] . '</a>, ';
                                                                }

                                                                echo substr($world_data_category_names, 0, -2);

                                                                ?>

                                                            </p>
                                                        <?php } ?>
                                                    </div>
                                                    <div class="col-12 col-lg-6">
                                                        <h6 class="mb-3"><i class="fa fa-database"></i>&nbsp;&nbsp;<?php echo get_custom_label('modal_beneficiaries_heading'); ?></h6>
                                                        <?php if (count($prd_details_data['world_beneficiaries']) == 0) { ?>
                                                            <p><?php echo get_custom_label('modal_no_beneficiaries'); ?></p>
                                                        <?php } else { ?>
                                                            <p> 
                                                                <?php
                                                                $world_beneficiaries = '';

                                                                foreach ($prd_details_data['world_beneficiaries'] as $beneficiary) {
                                                                    $world_beneficiaries .= '<a class="beneficiary" href="' . $beneficiary['link'] . '">' . $beneficiary['name'] . '</a>, ';
                                                                }
                                                                
                                                                echo substr($world_beneficiaries, 0, -2);

                                                                ?>
                                                            </p>
                                                        <?php } ?> 
                                                    </div>
                                                </div>
                                            </div>
                                        <?php } ?>
                                    <?php } ?>
                                </div>
                            </div>
                        </div>
                    </div>
                    <?php } ?>
                    <?php } ?>
                </div>

                <div class="row">
                    <div class="col-12 col-md-6 col-lg-5 col-xl-4 pl-0 pr-0" id="data-content-left-pane" style="background-color: <?php echo get_field('data_category_theme_color', get_post_field('data_category_theme', get_field('data_category_id', $post->ID))[0]); ?>">
                        <div id="data-content-left-pane-title" class="d-flex align-items-center">
                            <span class="text-white">
                                <?php echo sprintf(get_custom_label('data_content_nav_title'), "<b>" . strtolower(get_the_title($post->ID)) . "</b>"); ?>
                            </span>
                        </div>
                        <?php $beneficiaries = $response_data_category[1]; ?>
                        <div id="data-nav-xs" class="d-sm-none nav nav-pills" role="tablist" aria-orientation="vertical">
                            <div>
                                <?php for ($i = 0; $i < count($beneficiaries); $i++) { ?>
                                    <a class="nav-link" data-toggle="pill" href="#tab-pane-data-<?php echo $i; ?>" role="tab" aria-controls="tab-pane-data-<?php echo $i; ?>" aria-selected="false"><?php echo $beneficiaries[$i]; ?></a>
                                <?php } ?>
                            </div>
                        </div>
                        <div id="data-nav" class="d-none d-sm-block nav flex-column nav-pills" role="tablist" aria-orientation="vertical">
                            <?php for ($i = 0; $i < count($beneficiaries); $i++) { ?>
                                <a class="nav-link<?php echo ($i == 0 ? ' active' : ''); ?>" data-toggle="pill" href="#tab-pane-data-<?php echo $i; ?>" role="tab" aria-controls="tab-pane-data-<?php echo $i; ?>" aria-selected="<?php echo ($i == 0 ? 'true' : 'false'); ?>"><?php echo $beneficiaries[$i]; ?>
                                </a>
                            <?php } ?>
                        </div>
                    </div>
                    <div class="d-none d-sm-block col-12 col-md-6 col-lg-7 col-xl-8 pl-0 pr-0" id="data-content-right-pane">
                        <div class="tab-content pt-0">
                            <?php for ($i = 0; $i < count($beneficiaries); $i++) { ?>
                                <div class="tab-pane<?php echo ($i == 0 ? ' active' : ''); ?>" id="tab-pane-data-<?php echo $i; ?>" role="tabpanel" aria-labelledby="tab-pane-data-<?php echo $i; ?>">

                                    <div class="d-sm-none data-content-right-pane-title-xs d-flex align-items-center justify-content-start">
                                        <div class="p-3">
                                            <a href="javascript:void(0);"><i class="fa fa-chevron-left p-1"></i></a>
                                        </div>
                                        <div>
                                            <a href="javascript:void(0);"><span class="title-xs"><?php echo $beneficiaries[$i]; ?></span></a>
                                        </div>
                                    </div>

                                    <div class="d-none data-content-right-pane-title d-sm-flex align-items-center">
                                        <span>
                                            <?php echo $beneficiaries[$i]; ?>
                                        </span>
                                    </div>

                                    <div class="card-columns-container">
                                        <div class="card-columns">
                                        <!-- <?php echo json_encode(get_data_page_data(get_field('data_category_id', $post->ID))); ?> -->
                                        <?php 
                                            // for each data category
                                            foreach ($response_data_category[2] as $prd_per_benef_name) {

                                                // if the beneficiary is the current one
                                                if ($beneficiaries[$i] == $prd_per_benef_name['beneficiary_name']) {

                                                    // public items
                                                    if ($prd_per_benef_name['visibility'] == 'Publiek / publique') { 

                                                        foreach ($prd_per_benef_name['prd_data'] as $prd_data) { ?>

                                                            <div class="card public" data-prd="data-modal-<?php echo $prd_data['id']; ?>">
                                                                <div class="card-body">
                                                                    <h6 class="card-title">
                                                                        <i class="fa public" style="color: <?php echo get_field('data_category_theme_color', get_post_field('data_category_theme', get_field('data_category_id', $post->ID))[0]); ?>"></i>&nbsp;&nbsp;<?php echo $prd_data['name']; ?>
                                                                    </h6>
                                                                    <p class="card-text"><?php echo $prd_data['description']; ?></p>
                                                                    <div class="card-footer">
                                                                        <small><span class="text-uppercase objective-title"><?php echo get_custom_label('card_objective') . '</span>&nbsp;' . $prd_data['objective']; ?></small>
                                                                    </div>
                                                                </div>
                                                            </div>

                                                        <?php } ?>

                                                    <?php
                                                    // semi-public items
                                                    } elseif ($prd_per_benef_name['visibility'] == 'Beperkt publiek / publique avec restrictions') {

                                                        foreach ($prd_per_benef_name['prd_data'] as $prd_data) { ?>

                                                            <div class="card private" data-prd="data-modal-<?php echo $prd_data['id']; ?>">
                                                                <div class="card-body">
                                                                    <h6 class="card-title"><i class="fa private text-danger"></i>&nbsp;&nbsp;<?php echo $prd_data['name']; ?></h6>
                                                                    <p class="card-text"><?php echo $prd_data['description']; ?></p>
                                                                </div>
                                                            </div>

                                                        <?php } ?>

                                                    <?php } else { ?>

                                                        <div class="card private">
                                                            <div class="card-body">
                                                                <h6 class="card-title">
                                                                    <i class="fa private text-danger"></i>&nbsp;&nbsp;<?php echo sprintf(get_custom_label('card_private_title'), $prd_per_benef_name['private_data_amount']); ?>
                                                                </h6>
                                                                <p class="card-text"><?php echo get_custom_label('card_private_description'); ?></p>
                                                            </div>
                                                        </div>

                                                    <?php } ?>

                                                <?php } ?>

                                        <?php } ?>
                                        </div>
                                    </div>
                                </div>
                            <?php } ?>
                        </div>
                    </div>
                </div>

            </section>

        <?php }

        // if authority page
        elseif (get_field('authority_id', $post->ID) != '') { ?>
            
            <?php $response_authority = get_authority_page_data(get_field('authority_id', $post->ID), get_post_field('post_name', get_post())); ?>
            
            <section class="d-sm-none page-section container-fluid">
                
                <div class="row authority-intro-section-xs align-items-center" style="background: url('<?= get_bloginfo("template_url"); ?>/img/authority-header.png') no-repeat 90% bottom;background-size:cover; height:calc(25vh - 1.25rem);">
                    <div class="col-12 text-center">
                        <h1 class="mb-2"><?php echo get_the_title($post->ID); ?></h1>
                        <h5 class="text-uppercase"><?php echo get_post_meta(get_post_field('authority_id'), 'authority_post_description' . get_current_language())[0]; ?></h5>
                        <?php $authority_website = get_field('authority_post_website', get_field('authority_id', $post->ID)); ?>
                        <?php if ($authority_website != '') { ?>
                            <p class="my-3">
                                <a class="authority-link" target="_blank" href="<?php echo (substr($authority_website, 0, 3) == 'www' ? 'http://' : '') . $authority_website; ?>"><?php echo get_custom_label('authority_page_authority_website_link'); ?></a>&nbsp;&nbsp;<i class="fa fa-external-link"></i>
                            </p>
                        <?php } ?>
                    </div>
                </div>
                <div class="row authority-intro-section-xs align-items-center">
                    <div class="col-12 text-center" style="background: #FFFFFF url(<?php echo get_field('authority_post_logo', get_post_field('authority_id'))['sizes']['medium_large']; ?>) no-repeat center center; background-size:contain;height:calc(25vh - 5rem)">
                    </div>
                </div>
                <div class="row authority-intro-section-xs page-intro-figures-xs align-items-start pt-sm-5">
                    <div class="col-6 text-center">
                        <h2><?php echo count($response_authority[4]); ?></h2>
                        <p><?php echo get_custom_label('authority_page_data_figure_left'); ?></p>
                    </div>
                    <div class="col-6 text-center">
                        <h2><?php echo count($response_authority[0]); ?></h2>
                        <p><?php echo get_custom_label('authority_page_data_figure_right'); ?></p>
                    </div>
                </div>
                <div class="row authority-intro-section-xs page-intro-cta-xs align-items-center">
                    <div class="col-12 text-center">
                        <?php if ($response_authority[4] != 0) { ?>
                            <button type="button" class="btn btn-light my-2" onclick="scrollToSection('#authorities-content');"><?php echo get_custom_label('authority_discover_button'); ?></button>
                        <?php } ?>
                        <button type="button" class="btn btn-outline-light" onclick="scrollToSection('#authorities-contact');"><i class="fa fa-envelope-o"></i>&nbsp;&nbsp;<?php echo get_custom_label('authority_contact_button'); ?></button>
                    </div>
                    <div class="col-12 text-center py-2">
                        <?php if ($response_authority[4] == 0) { ?>
                            <a href="#authorities-contact" onclick="scrollToSection('#authorities-contact')"><i class="fa fa-angle-down fa-2x dark-color"></i></a>
                        <?php } else { ?>
                            <a href="#authorities-content" onclick="scrollToSection('#authorities-content')"><i class="fa fa-angle-down fa-2x dark-color"></i></a>
                        <?php } ?>
                    </div>
                </div>

            </section>
            
            <section class="d-none d-sm-block page-section container-fluid">
                <div class="row page-section-title align-items-center" style="background: url('<?= get_bloginfo("template_url"); ?>/img/authority-header.png') no-repeat 90% bottom;background-size:cover; height:calc(50vh - 2.5rem);">
                    <div class="col-12 col-sm-8 offset-sm-2 text-center">
                        <h1 class="mb-5"><?php echo get_the_title($post->ID); ?></h1>
                        <h5 class="text-uppercase"><?php echo get_post_meta(get_post_field('authority_id'), 'authority_post_description' . get_current_language())[0]; ?></h5>
                        <?php if (get_field('authority_post_website', get_field('authority_id', $post->ID)) != '') { ?>
                            <p class="my-4">
                                <a class="authority-link" target="_blank" href="<?php echo (substr($authority_website, 0, 3) == 'www' ? 'http://' : '') . $authority_website; ?>"><?php echo get_custom_label('authority_page_authority_website_link'); ?></a>&nbsp;&nbsp;<i class="fa fa-external-link"></i>
                            </p>
                        <?php } ?>
                    </div>
                </div>
                <div class="row page-section-figures align-content-center bg-white">
                    <div class="col-12 d-flex flex-column">
                        <div class="row mt-auto mb-auto align-items-start">
                            <?php if (get_field('authority_post_logo', get_post_field('authority_id'))['sizes']['medium_large'] != '') { ?>
                                <div class="col-12 col-sm-4 text-center">
                                    <h2><?php echo count($response_authority[4]); ?></h2>
                                    <p><?php echo get_custom_label('authority_page_data_figure_left'); ?></p>
                                </div>
                                <div class="col-12 col-sm-4 text-center">
                                    <img class="img-fluid" style="max-height:100px" src="<?php echo get_field('authority_post_logo', get_post_field('authority_id'))['sizes']['medium_large']; ?>" alt="" />
                                </div>
                                <div class="col-12 col-sm-4 text-center">
                                    <h2><?php echo count($response_authority[0]); ?></h2>
                                    <p><?php echo get_custom_label('authority_page_data_figure_right'); ?></p>
                                </div>
                            <?php } else { ?>
                                <div class="col-12 col-sm-6 text-center">
                                    <h2><?php echo count($response_authority[4]); ?></h2>
                                    <p><?php echo get_custom_label('authority_page_data_figure_left'); ?></p>
                                </div>
                                <div class="col-12 col-sm-6 text-center">
                                    <h2><?php echo count($response_authority[0]); ?></h2>
                                    <p><?php echo get_custom_label('authority_page_data_figure_right'); ?></p>
                                </div>
                            <?php } ?>
                        </div>
                        <div class="row align-content-center bg-white">
                            <div class="col-12 text-center p-2">
                                <?php if (count($response_authority[4]) != 0) { ?>
                                    <button type="button" class="btn btn-light mx-3" onclick="scrollToSection('#authorities-content');"><?php echo get_custom_label('authority_discover_button'); ?></button>
                                <?php } ?>
                                <button type="button" class="btn btn-outline-light mx-3" onclick="scrollToSection('#authorities-contact');"><i class="fa fa-envelope-o"></i>&nbsp;&nbsp;<?php echo get_custom_label('authority_contact_button'); ?></button>
                            </div>
                            <div class="col-12 text-center py-4">
                                <?php if (count($response_authority[4]) == 0) { ?>
                                    <a href="#authorities-contact" onclick="scrollToSection('#authorities-contact')"><i class="fa fa-angle-down fa-2x dark-color"></i></a>
                                <?php } else { ?>
                                    <a href="#authorities-content" onclick="scrollToSection('#authorities-content')"><i class="fa fa-angle-down fa-2x dark-color"></i></a>
                                <?php } ?>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
            
            <?php if (count($response_authority[4]) != 0) { ?>
                <section class="page-section authorities-content container-fluid" id="authorities-content">
                    
                    <div id="authority-prd-modal" class="modal" tabindex="-1" role="dialog">
                    <?php

                        foreach ($response_authority[4] as $modal_data) { 

                            if ($modal_data['visibility'] == 'Publiek / publique') { ?>
                            <div id="authority-modal-<?php echo $modal_data['id']; ?>" class="modal-dialog modal-lg d-none" role="document">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <h5 class="modal-title"><?php echo $modal_data['name']; ?></h5><br/>
                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                            <span aria-hidden="true">&times;</span>
                                        </button>
                                    </div>
                                    <div class="modal-body">
                                        <h6 class="modal-subtitle mb-3"><?php echo $modal_data['description']; ?><br/><br/><span class="text-uppercase objective"><?php echo get_custom_label('modal_subtitle_objective'); ?></span>&nbsp;<span class="post-objective"><?php echo $modal_data['main_objective']; ?></span></h6>
                                        <ul class="nav nav-tabs" id="authority-modal-countries" role="tablist">
                                        <?php if (! empty($modal_data['not_shared_data'])) { ?>
                                            <li class="nav-item">
                                                <a class="nav-link active" data-toggle="tab" href="#authority-content-not-shared-<?php echo $modal_data['id']; ?>" role="tab" aria-controls="authority-content-not-shared" aria-selected="true"><?php echo get_custom_label('modal_tab_not_shared'); ?></a>
                                            </li>
                                            <?php if (! empty($modal_data['belgium_data'])) { ?>
                                                <li class="nav-item">
                                                    <a class="nav-link" data-toggle="tab" href="#authority-content-belgium-<?php echo $modal_data['id']; ?>" role="tab" aria-controls="authority-content-belgium" aria-selected="true"><?php echo get_custom_label('modal_tab_belgium'); ?></a>
                                                </li>
                                            <?php } ?>

                                            <?php if (! empty($modal_data['europe_data'])) { ?>
                                                <li class="nav-item">
                                                    <a class="nav-link" data-toggle="tab" href="#authority-content-europe-<?php echo $modal_data['id']; ?>" role="tab" aria-controls="authority-content-europe" aria-selected="false"><?php echo get_custom_label('modal_tab_europe'); ?></a>
                                                </li>
                                            <?php } ?>

                                            <?php if (! empty($modal_data['world_data'])) { ?>
                                                <li class="nav-item">
                                                    <a class="nav-link" data-toggle="tab" href="#authority-content-world-<?php echo $modal_data['id']; ?>" role="tab" aria-controls="authority-content-world" aria-selected="false"><?php echo get_custom_label('modal_tab_world'); ?></a>
                                                </li>
                                            <?php } ?>
                                        <?php } else { ?>
                                            <?php if (! empty($modal_data['belgium_data'])) { ?>
                                                <li class="nav-item">
                                                    <a class="nav-link active" data-toggle="tab" href="#authority-content-belgium-<?php echo $modal_data['id']; ?>" role="tab" aria-controls="authority-content-belgium" aria-selected="true"><?php echo get_custom_label('modal_tab_belgium'); ?></a>
                                                </li>
                                            <?php } ?>

                                            <?php if (! empty($modal_data['europe_data'])) { ?>
                                                <li class="nav-item">
                                                    <a class="nav-link" data-toggle="tab" href="#authority-content-europe-<?php echo $modal_data['id']; ?>" role="tab" aria-controls="authority-content-europe" aria-selected="false"><?php echo get_custom_label('modal_tab_europe'); ?></a>
                                                </li>
                                            <?php } ?>

                                            <?php if (! empty($modal_data['world_data'])) { ?>
                                                <li class="nav-item">
                                                    <a class="nav-link" data-toggle="tab" href="#authority-content-world-<?php echo $modal_data['id']; ?>" role="tab" aria-controls="authority-content-world" aria-selected="false"><?php echo get_custom_label('modal_tab_world'); ?></a>
                                                </li>
                                            <?php } ?>
                                        <?php } ?>
                                        </ul>
                                        <div class="tab-content">
                                            <?php if (! empty($modal_data['not_shared_data'])) { ?>

                                                <div class="tab-pane fade show active" id="authority-content-not-shared-<?php echo $modal_data['id']; ?>" role="tabpanel">
                                                    <div class="row">
                                                        <div class="col-12">
                                                            <h5><?php echo get_custom_label('modal_not_shared_tab_title'); ?></h5>
                                                        </div>
                                                    </div>
                                                    <div class="row">
                                                        <div class="col-12 col-lg-6">
                                                            <h6 class="mb-3"><i class="fa fa-bank"></i>&nbsp;&nbsp;<?php echo get_custom_label('modal_owner_heading'); ?></h6>
                                                            <p><a href="<?php echo $modal_data['authority']['authority_link']; ?>" class="authority-name"><?php echo $modal_data['authority']['authority_name']; ?></a></p>
                                                        </div>
                                                        <div class="col-12 col-lg-6">
                                                            <h6 class="mb-3"><i class="fa fa-users"></i>&nbsp;&nbsp;<?php echo get_custom_label('modal_involved_parties_heading'); ?></h6>
                                                            <ul>
                                                                <?php foreach ($modal_data['involved_parties_categories'] as $involved_parties_category) { ?>
                                                                    <li><?php echo $involved_parties_category; ?></li>
                                                                <?php } ?>
                                                            </ul>
                                                        </div>
                                                    </div>
                                                    <div class="row">
                                                        <div class="col-12 col-lg-6">
                                                            <h6 class="mb-3"><i class="fa fa-share-alt"></i>&nbsp;&nbsp;<?php echo get_custom_label('modal_data_stored_heading'); ?></h6>
                                                            <?php foreach ($modal_data['not_shared_data'] as $not_shared_data) { ?>
                                                            <p class="mb-2">
                                                                <span class="data-category-theme" style="color: <?php echo $not_shared_data['data_category_theme_color']; ?>"><?php echo $not_shared_data['data_category_theme']; ?></span><br/>
                                                                <?php
                                                                    $not_shared_data_category_names = '';

                                                                    foreach ($not_shared_data['data_category_names'] as $not_shared_data_category_name) {
                                                                        $not_shared_data_category_names .= '<a class="data-category" href="' . $not_shared_data_category_name['link'] . '">' . $not_shared_data_category_name['name'] . '</a>, ';
                                                                    }

                                                                    echo substr($not_shared_data_category_names, 0, -2);
                                                                ?>
                                                            </p>
                                                            <?php } ?>
                                                        </div>
                                                    </div>
                                                </div>

                                                <?php if (! empty($modal_data['belgium_data'])) { ?>
                                                    <div class="tab-pane fade show" id="authority-content-belgium-<?php echo $modal_data['id']; ?>" role="tabpanel">
                                                        <div class="row">
                                                            <div class="col-12">
                                                                <h5><?php echo get_custom_label('modal_belgium_tab_title'); ?></h5>
                                                            </div>
                                                        </div>
                                                        <div class="row">
                                                            <div class="col-12 col-lg-6">
                                                                <h6 class="mb-3"><i class="fa fa-bank"></i>&nbsp;&nbsp;<?php echo get_custom_label('modal_owner_heading'); ?></h6>
                                                                <p><a href="<?php echo $modal_data['authority']['authority_link']; ?>" class="authority-name"><?php echo $modal_data['authority']['authority_name']; ?></a></p>
                                                            </div>
                                                            <div class="col-12 col-lg-6">
                                                                <h6 class="mb-3"><i class="fa fa-users"></i>&nbsp;&nbsp;<?php echo get_custom_label('modal_involved_parties_heading'); ?></h6>
                                                                <ul>
                                                                    <?php foreach ($modal_data['involved_parties_categories'] as $involved_parties_category) { ?>
                                                                        <li><?php echo $involved_parties_category; ?></li>
                                                                    <?php } ?>
                                                                </ul>
                                                            </div>
                                                        </div>
                                                        <div class="row">
                                                            <div class="col-12 col-lg-6">
                                                                <h6 class="mb-3"><i class="fa fa-share-alt"></i>&nbsp;&nbsp;<?php echo get_custom_label('modal_data_heading'); ?></h6>
                                                                <?php foreach ($modal_data['belgium_data'] as $belgium_data) { ?>
                                                                <p class="mb-2">
                                                                    <span class="data-category-theme" style="color: <?php echo $belgium_data['data_category_theme_color']; ?>"><?php echo $belgium_data['data_category_theme']; ?></span><br/>
                                                                    <?php
                                                                        $belgium_data_category_names = '';

                                                                        foreach ($belgium_data['data_category_names'] as $belgium_data_category_name) {
                                                                            $belgium_data_category_names .= '<a class="data-category" href="' . $belgium_data_category_name['link'] . '">' . $belgium_data_category_name['name'] . '</a>, ';
                                                                        }

                                                                        echo substr($belgium_data_category_names, 0, -2);
                                                                    ?>
                                                                </p>
                                                                <?php } ?>
                                                            </div>
                                                            <div class="col-12 col-lg-6">
                                                                <h6 class="mb-3"><i class="fa fa-database"></i>&nbsp;&nbsp;<?php echo get_custom_label('modal_beneficiaries_heading'); ?></h6>
                                                                <?php if (count($modal_data['belgium_beneficiaries']) == 0) { ?>
                                                                <p><?php echo get_custom_label('modal_no_beneficiaries'); ?></p>
                                                                <?php } else { ?>
                                                                <p> 
                                                                    <?php
                                                                        
                                                                        $belgium_data_beneficiaries = '';
                                                                        
                                                                        foreach($modal_data['belgium_beneficiaries'] as $beneficiary) {
                                                                            $belgium_data_beneficiaries .= '<a class="beneficiary" href="' . $beneficiary['link'] . '">' . $beneficiary['name'] . '</a>, ';
                                                                        
                                                                        }

                                                                        echo substr($belgium_data_beneficiaries, 0, -2);

                                                                    ?>
                                                                </p>
                                                                <?php } ?>
                                                            </div>
                                                        </div>
                                                    </div>
                                                <?php } ?>

                                                <?php if (! empty($modal_data['europe_data'])) { ?>
                                                    <div class="tab-pane fade" id="authority-content-europe-<?php echo $modal_data['id']; ?>" role="tabpanel">
                                                        <div class="row">
                                                            <div class="col-12">
                                                                <h5><?php echo get_custom_label('modal_europe_tab_title'); ?></h5>
                                                            </div>
                                                        </div>
                                                        <div class="row">
                                                            <div class="col-12 col-lg-6">
                                                                <h6 class="mb-3"><i class="fa fa-bank"></i>&nbsp;&nbsp;<?php echo get_custom_label('modal_owner_heading'); ?></h6>
                                                                <p><a href="<?php echo $modal_data['authority']['authority_link']; ?>" class="authority-name"><?php echo $modal_data['authority']['authority_name']; ?></a></p>
                                                            </div>
                                                            <div class="col-12 col-lg-6">
                                                                <h6 class="mb-3"><i class="fa fa-users"></i>&nbsp;&nbsp;<?php echo get_custom_label('modal_involved_parties_heading'); ?></h6>
                                                                <ul>
                                                                    <?php foreach($modal_data['involved_parties_categories'] as $involved_parties_category) { ?>
                                                                        <li><?php echo $involved_parties_category; ?></li>
                                                                    <?php } ?>
                                                                </ul>
                                                            </div>
                                                        </div>
                                                        <div class="row">
                                                            <div class="col-12 col-lg-6">
                                                                <h6 class="mb-3"><i class="fa fa-share-alt"></i>&nbsp;&nbsp;<?php echo get_custom_label('modal_data_heading'); ?></h6>
                                                                <?php foreach($modal_data['europe_data'] as $europe_data) { ?>
                                                                    <p class="mb-2">
                                                                        <span class="data-category-theme" style="color: <?php echo $europe_data['data_category_theme_color']; ?>"><?php echo $europe_data['data_category_theme']; ?></span><br/>
                                                                        <?php $europe_data_category_names = ''; ?>
                                                                        <?php foreach($europe_data['data_category_names'] as $europe_data_category_name) {
                                                                            $europe_data_category_names .= '<a class="data-category" href="' . $europe_data_category_name['link'] . '">' . $europe_data_category_name['name'] . '</a>, ';
                                                                        }

                                                                        echo substr($europe_data_category_names, 0, -2);

                                                                        ?>

                                                                    </p>
                                                                <?php } ?>
                                                                
                                                            </div>
                                                            <div class="col-12 col-lg-6">
                                                                <h6 class="mb-3"><i class="fa fa-database"></i>&nbsp;&nbsp;<?php echo get_custom_label('modal_beneficiaries_heading'); ?></h6>
                                                                <?php if (count($modal_data['europe_beneficiaries']) == 0) { ?>
                                                                    <p><?php echo get_custom_label('modal_no_beneficiaries'); ?></p>
                                                                <?php } else { ?>
                                                                    <p> 
                                                                    <?php

                                                                        $europe_data_beneficiaries = '';

                                                                        foreach($modal_data['europe_beneficiaries'] as $beneficiary) {
                                                                            $europe_data_beneficiaries .= '<a class="beneficiary" href="' . $beneficiary['link'] . '">' . $beneficiary['name'] . '</a>, ';    
                                                                        }

                                                                        echo substr($europe_data_beneficiaries, 0, -2);

                                                                    ?>
                                                                    </p>
                                                                <?php } ?>
                                                            </div>
                                                        </div>
                                                    </div>
                                                <?php } ?>

                                                <?php if (! empty($modal_data['world_data'])) { ?>
                                                    <div class="tab-pane fade" id="authority-content-world-<?php echo $modal_data['id']; ?>" role="tabpanel">
                                                        <div class="row">
                                                            <div class="col-12">
                                                                <h5><?php echo get_custom_label('modal_world_tab_title'); ?></h5>
                                                            </div>
                                                        </div>
                                                        <div class="row">
                                                            <div class="col-12 col-lg-6">
                                                                <h6 class="mb-3"><i class="fa fa-bank"></i>&nbsp;&nbsp;<?php echo get_custom_label('modal_owner_heading'); ?></h6>
                                                                <p><a href="<?php echo $modal_data['authority']['authority_link']; ?>" class="authority-name"><?php echo $modal_data['authority']['authority_name']; ?></a></p>
                                                            </div>
                                                            <div class="col-12 col-lg-6">
                                                               <h6 class="mb-3"><i class="fa fa-users"></i>&nbsp;&nbsp;<?php echo get_custom_label('modal_involved_parties_heading'); ?></h6>
                                                               <ul>
                                                                   <?php foreach($modal_data['involved_parties_categories'] as $involved_parties_category) { ?>
                                                                       <li><?php echo $involved_parties_category; ?></li>
                                                                   <?php } ?>
                                                               </ul>
                                                            </div>
                                                        </div>
                                                        <div class="row">
                                                            <div class="col-12 col-lg-6">
                                                                <h6 class="mb-3"><i class="fa fa-share-alt"></i>&nbsp;&nbsp;<?php echo get_custom_label('modal_data_heading'); ?></h6>
                                                                <?php foreach($modal_data['world_data'] as $world_data) { ?>
                                                                    <p class="mb-2">
                                                                        <span class="data-category-theme" style="color: <?php echo $world_data['data_category_theme_color']; ?>"><?php echo $world_data['data_category_theme']; ?></span><br/>
                                                                        <?php $world_data_category_names = ''; ?>
                                                                        <?php foreach($world_data['data_category_names'] as $world_data_category_name) {
                                                                            $world_data_category_names .= '<a class="data-category" href="' . $world_data_category_name['link'] . '">' . $world_data_category_name['name'] . '</a>, ';
                                                                        }

                                                                        echo substr($world_data_category_names, 0, -2);

                                                                        ?>
                                                                    </p>
                                                                <?php } ?>
                                                            </div>
                                                            <div class="col-12 col-lg-6">
                                                                <h6 class="mb-3"><i class="fa fa-database"></i>&nbsp;&nbsp;<?php echo get_custom_label('modal_beneficiaries_heading'); ?></h6>
                                                                <?php if (count($modal_data['world_beneficiaries']) == 0) { ?>
                                                                    <p><?php echo get_custom_label('modal_no_beneficiaries'); ?></p>
                                                                <?php } else { ?>
                                                                    <p> 
                                                                        <?php
                                                                            $world_data_beneficiaries = '';

                                                                            foreach ($modal_data['world_beneficiaries'] as $beneficiary) {
                                                                                $world_data_beneficiaries .= '<a class="beneficiary" href="' . $beneficiary['link'] . '">' . $beneficiary['name'] . '</a>, ';    
                                                                            }

                                                                            echo substr($world_data_beneficiaries, 0, -2);

                                                                        ?>
                                                                    </p>
                                                                <?php } ?>
                                                                
                                                            </div>
                                                        </div>
                                                    </div>
                                                <?php } ?>
                                            <?php } else { ?>
                                                <?php if (! empty($modal_data['belgium_data'])) { ?>
                                                    <div class="tab-pane fade show active" id="authority-content-belgium-<?php echo $modal_data['id']; ?>" role="tabpanel">
                                                        <div class="row">
                                                            <div class="col-12">
                                                                <h5><?php echo get_custom_label('modal_belgium_tab_title'); ?></h5>
                                                            </div>
                                                        </div>
                                                        <div class="row">
                                                            <div class="col-12 col-lg-6">
                                                                <h6 class="mb-3"><i class="fa fa-bank"></i>&nbsp;&nbsp;<?php echo get_custom_label('modal_owner_heading'); ?></h6>
                                                                <p><a href="<?php echo $modal_data['authority']['authority_link']; ?>" class="authority-name"><?php echo $modal_data['authority']['authority_name']; ?></a></p>
                                                            </div>
                                                            <div class="col-12 col-lg-6">
                                                                <h6 class="mb-3"><i class="fa fa-users"></i>&nbsp;&nbsp;<?php echo get_custom_label('modal_involved_parties_heading'); ?></h6>
                                                                <ul>
                                                                    <?php foreach ($modal_data['involved_parties_categories'] as $involved_parties_category) { ?>
                                                                        <li><?php echo $involved_parties_category; ?></li>
                                                                    <?php } ?>
                                                                </ul>
                                                            </div>
                                                        </div>
                                                        <div class="row">
                                                            <div class="col-12 col-lg-6">
                                                                <h6 class="mb-3"><i class="fa fa-share-alt"></i>&nbsp;&nbsp;<?php echo get_custom_label('modal_data_heading'); ?></h6>
                                                                <?php foreach ($modal_data['belgium_data'] as $belgium_data) { ?>
                                                                <p class="mb-2">
                                                                    <span class="data-category-theme" style="color: <?php echo $belgium_data['data_category_theme_color']; ?>"><?php echo $belgium_data['data_category_theme']; ?></span><br/>
                                                                    <?php
                                                                        $belgium_data_category_names = '';

                                                                        foreach ($belgium_data['data_category_names'] as $belgium_data_category_name) {
                                                                            $belgium_data_category_names .= '<a class="data-category" href="' . $belgium_data_category_name['link'] . '">' . $belgium_data_category_name['name'] . '</a>, ';
                                                                        }

                                                                        echo substr($belgium_data_category_names, 0, -2);
                                                                    ?>
                                                                </p>
                                                                <?php } ?>
                                                            </div>
                                                            <div class="col-12 col-lg-6">
                                                                <h6 class="mb-3"><i class="fa fa-database"></i>&nbsp;&nbsp;<?php echo get_custom_label('modal_beneficiaries_heading'); ?></h6>
                                                                <?php if (count($modal_data['belgium_beneficiaries']) == 0) { ?>
                                                                <p><?php echo get_custom_label('modal_no_beneficiaries'); ?></p>
                                                                <?php } else { ?>
                                                                <p> 
                                                                    <?php
                                                                        
                                                                        $belgium_data_beneficiaries = '';
                                                                        
                                                                        foreach($modal_data['belgium_beneficiaries'] as $beneficiary) {
                                                                            $belgium_data_beneficiaries .= '<a class="beneficiary" href="' . $beneficiary['link'] . '">' . $beneficiary['name'] . '</a>, ';
                                                                        
                                                                        }

                                                                        echo substr($belgium_data_beneficiaries, 0, -2);

                                                                    ?>
                                                                </p>
                                                                <?php } ?>
                                                            </div>
                                                        </div>
                                                    </div>
                                                <?php } ?>

                                                <?php if (! empty($modal_data['europe_data'])) { ?>
                                                    <div class="tab-pane fade" id="authority-content-europe-<?php echo $modal_data['id']; ?>" role="tabpanel">
                                                        <div class="row">
                                                            <div class="col-12">
                                                                <h5><?php echo get_custom_label('modal_europe_tab_title'); ?></h5>
                                                            </div>
                                                        </div>
                                                        <div class="row">
                                                            <div class="col-12 col-lg-6">
                                                                <h6 class="mb-3"><i class="fa fa-bank"></i>&nbsp;&nbsp;<?php echo get_custom_label('modal_owner_heading'); ?></h6>
                                                                <p><a href="<?php echo $modal_data['authority']['authority_link']; ?>" class="authority-name"><?php echo $modal_data['authority']['authority_name']; ?></a></p>
                                                            </div>
                                                            <div class="col-12 col-lg-6">
                                                                <h6 class="mb-3"><i class="fa fa-users"></i>&nbsp;&nbsp;<?php echo get_custom_label('modal_involved_parties_heading'); ?></h6>
                                                                <ul>
                                                                    <?php foreach($modal_data['involved_parties_categories'] as $involved_parties_category) { ?>
                                                                        <li><?php echo $involved_parties_category; ?></li>
                                                                    <?php } ?>
                                                                </ul>
                                                            </div>
                                                        </div>
                                                        <div class="row">
                                                            <div class="col-12 col-lg-6">
                                                                <h6 class="mb-3"><i class="fa fa-share-alt"></i>&nbsp;&nbsp;<?php echo get_custom_label('modal_data_heading'); ?></h6>
                                                                <?php foreach($modal_data['europe_data'] as $europe_data) { ?>
                                                                    <p class="mb-2">
                                                                        <span class="data-category-theme" style="color: <?php echo $europe_data['data_category_theme_color']; ?>"><?php echo $europe_data['data_category_theme']; ?></span><br/>
                                                                        <?php $europe_data_category_names = ''; ?>
                                                                        <?php foreach($europe_data['data_category_names'] as $europe_data_category_name) {
                                                                            $europe_data_category_names .= '<a class="data-category" href="' . $europe_data_category_name['link'] . '">' . $europe_data_category_name['name'] . '</a>, ';
                                                                        }

                                                                        echo substr($europe_data_category_names, 0, -2);

                                                                        ?>

                                                                    </p>
                                                                <?php } ?>
                                                                
                                                            </div>
                                                            <div class="col-12 col-lg-6">
                                                                <h6 class="mb-3"><i class="fa fa-database"></i>&nbsp;&nbsp;<?php echo get_custom_label('modal_beneficiaries_heading'); ?></h6>
                                                                <?php if (count($modal_data['europe_beneficiaries']) == 0) { ?>
                                                                    <p><?php echo get_custom_label('modal_no_beneficiaries'); ?></p>
                                                                <?php } else { ?>
                                                                    <p> 
                                                                    <?php

                                                                        $europe_data_beneficiaries = '';

                                                                        foreach($modal_data['europe_beneficiaries'] as $beneficiary) {
                                                                            $europe_data_beneficiaries .= '<a class="beneficiary" href="' . $beneficiary['link'] . '">' . $beneficiary['name'] . '</a>, ';    
                                                                        }

                                                                        echo substr($europe_data_beneficiaries, 0, -2);

                                                                    ?>
                                                                    </p>
                                                                <?php } ?>
                                                            </div>
                                                        </div>
                                                    </div>
                                                <?php } ?>

                                                <?php if (! empty($modal_data['world_data'])) { ?>
                                                    <div class="tab-pane fade" id="authority-content-world-<?php echo $modal_data['id']; ?>" role="tabpanel">
                                                        <div class="row">
                                                            <div class="col-12">
                                                                <h5><?php echo get_custom_label('modal_world_tab_title'); ?></h5>
                                                            </div>
                                                        </div>
                                                        <div class="row">
                                                            <div class="col-12 col-lg-6">
                                                                <h6 class="mb-3"><i class="fa fa-bank"></i>&nbsp;&nbsp;<?php echo get_custom_label('modal_owner_heading'); ?></h6>
                                                                <p><a href="<?php echo $modal_data['authority']['authority_link']; ?>" class="authority-name"><?php echo $modal_data['authority']['authority_name']; ?></a></p>
                                                            </div>
                                                            <div class="col-12 col-lg-6">
                                                               <h6 class="mb-3"><i class="fa fa-users"></i>&nbsp;&nbsp;<?php echo get_custom_label('modal_involved_parties_heading'); ?></h6>
                                                               <ul>
                                                                   <?php foreach($modal_data['involved_parties_categories'] as $involved_parties_category) { ?>
                                                                       <li><?php echo $involved_parties_category; ?></li>
                                                                   <?php } ?>
                                                               </ul>
                                                            </div>
                                                        </div>
                                                        <div class="row">
                                                            <div class="col-12 col-lg-6">
                                                                <h6 class="mb-3"><i class="fa fa-share-alt"></i>&nbsp;&nbsp;<?php echo get_custom_label('modal_data_heading'); ?></h6>
                                                                <?php foreach($modal_data['world_data'] as $world_data) { ?>
                                                                    <p class="mb-2">
                                                                        <span class="data-category-theme" style="color: <?php echo $world_data['data_category_theme_color']; ?>"><?php echo $world_data['data_category_theme']; ?></span><br/>
                                                                        <?php $world_data_category_names = ''; ?>
                                                                        <?php foreach($world_data['data_category_names'] as $world_data_category_name) {
                                                                            $world_data_category_names .= '<a class="data-category" href="' . $world_data_category_name['link'] . '">' . $world_data_category_name['name'] . '</a>, ';
                                                                        }

                                                                        echo substr($world_data_category_names, 0, -2);

                                                                        ?>
                                                                    </p>
                                                                <?php } ?>
                                                            </div>
                                                            <div class="col-12 col-lg-6">
                                                                <h6 class="mb-3"><i class="fa fa-database"></i>&nbsp;&nbsp;<?php echo get_custom_label('modal_beneficiaries_heading'); ?></h6>
                                                                <?php if (count($modal_data['world_beneficiaries']) == 0) { ?>
                                                                    <p><?php echo get_custom_label('modal_no_beneficiaries'); ?></p>
                                                                <?php } else { ?>
                                                                    <p> 
                                                                        <?php

                                                                            $world_data_beneficiaries = '';

                                                                            foreach ($modal_data['world_beneficiaries'] as $beneficiary) {
                                                                                $world_data_beneficiaries .= '<a class="beneficiary" href="' . $beneficiary['link'] . '">' . $beneficiary['name'] . '</a>, ';    
                                                                            }

                                                                            echo substr($world_data_beneficiaries, 0, -2);

                                                                        ?>
                                                                    </p>
                                                                <?php } ?>
                                                            </div>
                                                        </div>
                                                    </div>
                                                <?php } ?>
                                            <?php } ?>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <?php

                                }
                            } 
                        ?>
                    </div>

                    <div class="row">
                        <div class="col-12 col-md-6 col-lg-5 col-xl-4 pl-0 pr-0" id="authorities-content-left-pane">
                            <div id="authorities-content-left-pane-title" class="d-flex align-items-center">
                                <span>
                                    <?php echo sprintf(get_custom_label('authority_content_nav_title'), "<b>" . get_field('authority_post_alias' . get_current_language(), get_field('authority_id', $post->ID)) . "</b>"); ?>
                                </span>
                            </div>
                            <div id="authority-accordion-xs" class="d-sm-none" role="tablist">
                                <?php $data_categories_themes = $response_authority[2]; ?>
                                <?php for ($i = 0; $i < count($data_categories_themes); $i++) { ?>
                                    <div class="card" style="background: <?php echo $data_categories_themes[$i]['data_category_theme_color']; ?>">
                                        <div class="card-header" role="tab" style="background: <?php echo $data_categories_themes[$i]['data_category_theme_color']; ?>">
                                            <h5 class="mb-0">
                                                <a class="collapsed" data-toggle="collapse" href="#authority-category-xs-<?php echo $i; ?>" aria-expanded="false" aria-controls="authority-category-xs-<?php echo $i; ?>">
                                                    <?php echo $data_categories_themes[$i]['data_category_theme']; ?>
                                                </a>
                                            </h5>
                                        </div>

                                        <div id="authority-category-xs-<?php echo $i; ?>" class="collapse" role="tabpanel" data-parent="#authority-accordion-xs">
                                            <div class="card-body py-0 pr-0">
                                                <div class="nav flex-column nav-pills" role="tablist" aria-orientation="vertical">
                                                <?php $j = 0; ?>
                                                <?php foreach ($data_categories_themes[$i]['data_category_names'] as $data_category_names) { ?>
                                                    <a class="nav-link" data-toggle="pill" href="#tab-pane-authority-<?php echo $i; ?>-<?php echo $j; ?>" role="tab" aria-controls="tab-pane-authority-<?php echo $i; ?>-<?php echo $j; ?>" aria-selected="false"><?php echo $data_category_names; ?></a>
                                                <?php $j++; ?>
                                                <?php } ?>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                <?php } ?>
                            </div>
                            <div id="authority-accordion" class="d-none d-sm-block" role="tablist">
                                <?php for ($i = 0; $i < count($data_categories_themes); $i++) { ?>
                                    <div class="card" style="background: <?php echo $data_categories_themes[$i]['data_category_theme_color']; ?>">
                                        <div class="card-header" role="tab" style="background: <?php echo $data_categories_themes[$i]['data_category_theme_color']; ?>">
                                            <h5 class="mb-0">
                                                <a class="<?php echo ($i == 0 ? '' : 'collapsed'); ?>" data-toggle="collapse" href="#data-category-<?php echo $i; ?>" aria-expanded="<?php echo ($i == 0 ? 'true' : 'false'); ?>" aria-controls="data-category-<?php echo $i; ?>">
                                                    <?php echo $data_categories_themes[$i]['data_category_theme']; ?>
                                                </a>
                                            </h5>
                                        </div>

                                        <div id="data-category-<?php echo $i; ?>" class="collapse <?php echo ($i == 0 ? 'show' : ''); ?>" role="tabpanel" data-parent="#authority-accordion">
                                            <div class="card-body py-0 pr-0">
                                                <div class="nav flex-column nav-pills" role="tablist" aria-orientation="vertical">
                                                <?php $j = 0; ?>
                                                <?php foreach ($data_categories_themes[$i]['data_category_names'] as $data_category_names) { ?>
                                                    <a class="nav-link<?php echo ($i == 0 && $j == 0 ? ' active' : ''); ?>" data-toggle="pill" href="#tab-pane-authority-<?php echo $i; ?>-<?php echo $j; ?>" role="tab" aria-controls="tab-pane-authority-<?php echo $i; ?>-<?php echo $j; ?>" aria-selected="<?php echo ($i == 0 && $j == 0 ? 'true' : 'false'); ?>"><?php echo $data_category_names; ?></a>
                                                <?php $j++; ?>
                                                <?php } ?>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                <?php } ?>
                            </div>
                        </div>
                        <div class="d-none d-sm-block col-12 col-md-6 col-lg-7 col-xl-8 pl-0 pr-0" id="authorities-content-right-pane">
                            <div class="tab-content pt-0">
                                <?php 
                                for ($i = 0; $i < count($data_categories_themes); $i++) {

                                    $j = 0; 

                                    foreach ($data_categories_themes[$i]['data_category_names'] as $data_category_name) { ?>

                                        <div class="tab-pane<?php echo ($i == 0 && $j == 0 ? ' active' : ''); ?>" id="tab-pane-authority-<?php echo $i; ?>-<?php echo $j; ?>" role="tabpanel" aria-labelledby="tab-pane-authority-<?php echo $i; ?>-<?php echo $j; ?>">

                                            <div class="d-sm-none authorities-content-right-pane-title-xs d-flex align-items-center justify-content-start">
                                                <div class="p-3">
                                                    <a href="javascript:void(0);"><i class="fa fa-chevron-left p-1"></i></a>
                                                </div>
                                                <div>
                                                    <a href="javascript:void(0);"><span class="title-xs"><?php echo $data_category_name; ?></span></a>
                                                </div>
                                            </div>

                                            <div class="d-none authorities-content-right-pane-title d-sm-flex align-items-center">
                                                <span>
                                                    <?php echo $data_category_name; ?>
                                                </span>
                                            </div>

                                            <div class="card-columns-container">
                                                <div class="card-columns">

                                                    <?php foreach ($response_authority[3] as $prd_per_dc_name) {

                                                        // if the data category is the current one
                                                        if ($data_category_name == $prd_per_dc_name['data_category_name']) {

                                                            // public items
                                                            if ($prd_per_dc_name['visibility'] == 'Publiek / publique') { 

                                                                foreach ($prd_per_dc_name['prd_data'] as $data) { ?>

                                                                    <div class="card public" data-prd="authority-modal-<?php echo $data['id']; ?>">
                                                                        <div class="card-body">
                                                                            <h6 class="card-title">
                                                                                <i class="fa public" style="color: <?php echo $data_categories_themes[$i]['data_category_theme_color']; ?>"></i>&nbsp;&nbsp;<?php echo $data['name']; ?>
                                                                            </h6>
                                                                            <p class="card-text"><?php echo $data['description']; ?></p>
                                                                            <div class="card-footer">
                                                                                <small><span class="text-uppercase objective-title"><?php echo get_custom_label('card_objective') . '</span>&nbsp;' . $data['objective']; ?></small>
                                                                            </div>
                                                                        </div>
                                                                    </div>

                                                                <?php } ?>

                                                            <?php
                                                            // semi-public items
                                                            } elseif ($prd_per_dc_name['visibility'] == 'Beperkt publiek / publique avec restrictions') {

                                                                foreach ($prd_per_dc_name['prd_data'] as $data) { ?>

                                                                    <div class="card private" data-prd="authority-modal-<?php echo $data['id']; ?>">
                                                                        <div class="card-body">
                                                                            <h6 class="card-title"><i class="fa private text-danger"></i>&nbsp;&nbsp;<?php echo $data['name']; ?></h6>
                                                                            <p class="card-text"><?php echo $data['description']; ?></p>
                                                                        </div>
                                                                    </div>

                                                                <?php } ?>

                                                            <?php } else { ?>

                                                                <div class="card private">
                                                                    <div class="card-body">
                                                                        <h6 class="card-title">
                                                                            <i class="fa private text-danger"></i>&nbsp;&nbsp;<?php echo sprintf(get_custom_label('card_private_title'), $prd_per_dc_name['private_data_amount']); ?>
                                                                        </h6>
                                                                        <p class="card-text"><?php echo get_custom_label('card_private_description'); ?></p>
                                                                    </div>
                                                                </div>

                                                            <?php } ?>

                                                        <?php } ?>
            
                                                    <?php } ?>

                                                </div>
                                            </div>

                                        </div>

                                    <?php

                                        $j++;
            
                                    }

                                }

                             ?>
                            </div>
                        </div>
                    </div>

                </section>
            <?php } ?>

            <section class="page-section container-fluid" id="authorities-contact">
                <div class="row">
                    <div class="d-flex flex-column col-12 col-sm-8 col-lg-6 col-xl-4 offset-sm-2 offset-lg-3 offset-xl-4 text-center">
                        <div class="mt-auto mb-auto">
                            <h2 class="my-5"><?php echo get_custom_label('contact_form_title'); ?></h2>
                            <h5 class="mb-3"><?php echo get_custom_label('contact_form_subtitle'); ?></h5>
                            <p class="mt-0 mb-4"><?php echo sprintf(get_custom_label('contact_form_introduction'), "<b>" . get_the_title($post->ID) . "</b>"); ?></p>
                        
                            <form id="authorities-contact-form">

                                <input id="authorities-contact-form-email" class="form-control mt-4" placeholder="<?php echo get_custom_label('contact_form_email_placeholder'); ?>" type="text" name="email" />
                                <div class="invalid-field-feedback d-none text-left"><?php echo get_custom_label('contact_form_email_feedback'); ?></div>

                                <textarea id="authorities-contact-form-message" class="form-control mt-4" placeholder="<?php echo get_custom_label('contact_form_message_placeholder'); ?>" name="message" rows="5" style="resize:none"></textarea>
                                <div class="invalid-field-feedback d-none text-left"><?php echo get_custom_label('contact_form_message_feedback'); ?></div>

                                <input type="text" name="url" class="d-none" />

                                <button class="btn btn-light my-5" data-text="<?php echo get_custom_label('contact_form_send_button'); ?>" data-loading="<?php echo get_custom_label('contact_form_send_button_loading'); ?>" data-success="<?php echo get_custom_label('contact_form_send_button_success'); ?>" type="button"><?php echo get_custom_label('contact_form_send_button'); ?></button>

                            </form>
                        </div>
                    </div>
                </div>

            </section>

        <?php }

        // if beneficiary page
        elseif (get_field('beneficiary_id', $post->ID) != '') { ?>

            <?php $response_beneficiaries = get_beneficiary_page_data(get_field('beneficiary_id', $post->ID)); ?>
        
            <section class="d-sm-none page-section container-fluid">
                <div class="row beneficiary-intro-section-xs align-items-center" style="background: url('<?= get_bloginfo("template_url"); ?>/img/beneficiary-header.png') no-repeat 90% bottom;background-size:cover; height:calc(25vh - 1.25rem);">
                    <div class="col-12 text-center">
                        <h1><?php echo get_the_title($post->ID); ?></h1>
                        <h5 class="text-uppercase"><?php echo get_custom_label('beneficiary_page_subtitle'); ?></h5>
                    </div>
                </div>
                <div class="row beneficiary-intro-section-xs d-flex align-items-center">
                    <div class="col-12 text-center">
                        <?php echo get_field('beneficiary_description' . get_current_language(), get_field('beneficiary_id', $post->ID)); ?>
                    </div>
                </div>
                <div class="row beneficiary-intro-section-xs page-intro-figures-xs align-items-start pt-sm-5" style="background-color: <?php echo get_field('data_category_theme_color', get_post_field('data_category_theme', get_field('beneficiary_id', $post->ID))[0]); ?>">
                    <div class="col-6 text-center">
                        <h2><?php echo count($response_beneficiaries[0]); ?></h2>
                        <p><?php echo get_custom_label('beneficiary_page_data_amount'); ?></p>
                    </div>
                    <div class="col-6 text-center">
                        <h2><?php echo $response_beneficiaries[1]; ?></h2>
                        <p><?php echo get_custom_label('beneficiary_page_data_shared_amount'); ?></p>
                    </div>
                </div>
                <div class="row beneficiary-intro-section-xs page-intro-cta-xs d-flex align-items-center" style="background-color: <?php echo get_field('data_category_theme_color', get_post_field('data_category_theme', get_field('beneficiary_id', $post->ID))[0]); ?>">
                    <div class="col-12 text-center">
                        <button type="button" class="btn btn-light mt-4 mb-2" onclick="scrollToSection('#beneficiary-content');"><?php echo get_custom_label('beneficiary_introduction_button'); ?></button>
                    </div>
                    <div class="col-12 text-center py-2">
                        <a href="#beneficiary-content" onclick="scrollToSection('#beneficiary-content')"><i class="fa fa-angle-down fa-2x dark-color"></i></a>
                    </div>
                </div>

            </section>
            
            <section class="d-none d-sm-block page-section container-fluid">
                <div class="row page-section-title align-items-center" style="background: url('<?= get_bloginfo("template_url"); ?>/img/beneficiary-header.png') no-repeat 90% bottom;background-size:cover; height:calc(50vh - 2.5rem);">
                    <div class="col-12 col-sm-8 offset-sm-2 text-center">
                        <h1 class="mb-5"><?php echo get_the_title($post->ID); ?></h1>
                        <h5 class="text-uppercase"><?php echo get_custom_label('beneficiary_page_subtitle'); ?></h5>
                    </div>
                </div>
                <div class="row page-section-figures align-content-center" style="background-color: <?php echo get_field('data_category_theme_color', get_post_field('data_category_theme', get_field('beneficiary_id', $post->ID))[0]); ?>">
                    <div class="col-12 d-flex flex-column">
                        <div class="row mt-auto mb-auto align-items-center">
                            <div class="col-12 col-sm-4 text-center">
                                <h2><?php echo count($response_beneficiaries[0]); ?></h2>
                                <p><?php echo get_custom_label('beneficiary_page_data_amount'); ?></p>
                            </div>
                            <div class="col-12 col-sm-4 text-center">
                                <?php echo get_field('beneficiary_description' . get_current_language(), get_field('beneficiary_id', $post->ID)); ?>
                            </div>
                            <div class="col-12 col-sm-4 text-center">
                                <h2><?php echo $response_beneficiaries[1]; ?></h2>
                                <p><?php echo get_custom_label('beneficiary_page_data_shared_amount'); ?></p>
                            </div>
                        </div>
                        <div class="row align-content-center" style="background-color: <?php echo get_field('data_category_theme_color', get_post_field('data_category_theme', get_field('beneficiary_id', $post->ID))[0]); ?>">
                            <div class="col-12 text-center p-2">
                                <button type="button" class="btn btn-light" onclick="scrollToSection('#beneficiary-content');"><?php echo get_custom_label('beneficiary_introduction_button'); ?></button>
                            </div>
                            <div class="col-12 text-center py-4">
                                <a href="#beneficiary-content" onclick="scrollToSection('#beneficiary-content')"><i class="fa fa-angle-down fa-2x dark-color"></i></a>
                            </div>
                        </div>
                    </div>
                </div>
            </section>

            <section class="page-section beneficiary-content container-fluid" id="beneficiary-content">
                
                <div id="beneficiary-prd-modal" class="modal" tabindex="-1" role="dialog">

                    <?php foreach ($response_beneficiaries[4] as $modal_data) { ?>

                    <?php if ($modal_data['visibility'] == 'Publiek / publique') { ?>

                    <div id="beneficiary-modal-<?php echo $modal_data['id']; ?>" class="modal-dialog modal-lg d-none" role="document">
                        <div class="modal-content">
                            <div class="modal-header">
                                <h5 class="modal-title"><?php echo $modal_data['name']; ?></h5><br/>
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                            </div>
                            <div class="modal-body">
                                <h6 class="modal-subtitle mb-3"><?php echo $modal_data['description']; ?><br/><br/><span class="text-uppercase objective"><?php echo get_custom_label('modal_subtitle_objective'); ?></span>&nbsp;<span class="post-objective"><?php echo $modal_data['main_objective']; ?></span></h6>
                                <ul class="nav nav-tabs" id="beneficiary-modal-countries" role="tablist">
                                <?php if (! empty($modal_data['not_shared_data'])) { ?>
                                    <li class="nav-item">
                                        <a class="nav-link active" data-toggle="tab" href="#beneficiary-content-not-shared-<?php echo $modal_data['id']; ?>" role="tab" aria-controls="beneficiary-content-not-shared" aria-selected="true"><?php echo get_custom_label('modal_tab_not_shared'); ?></a>
                                    </li>
                                    <?php if (! empty($modal_data['belgium_data'])) { ?>
                                        <li class="nav-item">
                                            <a class="nav-link" data-toggle="tab" href="#beneficiary-content-belgium-<?php echo $modal_data['id']; ?>" role="tab" aria-controls="beneficiary-content-belgium" aria-selected="true"><?php echo get_custom_label('modal_tab_belgium'); ?></a>
                                        </li>
                                    <?php } ?>

                                    <?php if (! empty($modal_data['europe_data'])) { ?>
                                        <li class="nav-item">
                                            <a class="nav-link" data-toggle="tab" href="#beneficiary-content-europe-<?php echo $modal_data['id']; ?>" role="tab" aria-controls="beneficiary-content-europe" aria-selected="false"><?php echo get_custom_label('modal_tab_europe'); ?></a>
                                        </li>
                                    <?php } ?>

                                    <?php if (! empty($modal_data['world_data'])) { ?>
                                        <li class="nav-item">
                                            <a class="nav-link" data-toggle="tab" href="#beneficiary-content-world-<?php echo $modal_data['id']; ?>" role="tab" aria-controls="beneficiary-content-world" aria-selected="false"><?php echo get_custom_label('modal_tab_world'); ?></a>
                                        </li>
                                    <?php } ?>
                                <?php } else { ?>
                                    <?php if (! empty($modal_data['belgium_data'])) { ?>
                                        <li class="nav-item">
                                            <a class="nav-link active" data-toggle="tab" href="#beneficiary-content-belgium-<?php echo $modal_data['id']; ?>" role="tab" aria-controls="beneficiary-content-belgium" aria-selected="true"><?php echo get_custom_label('modal_tab_belgium'); ?></a>
                                        </li>
                                    <?php } ?>

                                    <?php if (! empty($modal_data['europe_data'])) { ?>
                                        <li class="nav-item">
                                            <a class="nav-link" data-toggle="tab" href="#beneficiary-content-europe-<?php echo $modal_data['id']; ?>" role="tab" aria-controls="beneficiary-content-europe" aria-selected="false"><?php echo get_custom_label('modal_tab_europe'); ?></a>
                                        </li>
                                    <?php } ?>

                                    <?php if (! empty($modal_data['world_data'])) { ?>
                                        <li class="nav-item">
                                            <a class="nav-link" data-toggle="tab" href="#beneficiary-content-world-<?php echo $modal_data['id']; ?>" role="tab" aria-controls="beneficiary-content-world" aria-selected="false"><?php echo get_custom_label('modal_tab_world'); ?></a>
                                        </li>
                                    <?php } ?>
                                <?php } ?>
                                </ul>
                                <div class="tab-content">
                                    <?php if (! empty($modal_data['not_shared_data'])) { ?>
                                        
                                        <div class="tab-pane fade show" id="beneficiary-content-not-shared-<?php echo $modal_data['id']; ?>" role="tabpanel">
                                            <div class="row">
                                                <div class="col-12">
                                                    <h5><?php echo get_custom_label('modal_not_shared_tab_title'); ?></h5>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-12 col-lg-6">
                                                    <h6 class="mb-3"><i class="fa fa-bank"></i>&nbsp;&nbsp;<?php echo get_custom_label('modal_owner_heading'); ?></h6>
                                                    <p><a href="<?php echo $modal_data['authority']['authority_link']; ?>" class="authority-name"><?php echo $modal_data['authority']['authority_name']; ?></a></p>
                                                </div>
                                                <div class="col-12 col-lg-6">
                                                    <h6 class="mb-3"><i class="fa fa-users"></i>&nbsp;&nbsp;<?php echo get_custom_label('modal_involved_parties_heading'); ?></h6>
                                                    <ul>
                                                        <?php foreach($modal_data['involved_parties_categories'] as $involved_parties_category) { ?>
                                                            <li><?php echo $involved_parties_category; ?></li>
                                                        <?php } ?>
                                                    </ul>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-12 col-lg-6">
                                                    <h6 class="mb-3"><i class="fa fa-share-alt"></i>&nbsp;&nbsp;<?php echo get_custom_label('modal_data_heading'); ?></h6>
                                                    <?php foreach ($modal_data['not_shared_data'] as $not_shared_data) { ?>
                                                        <p class="mb-2">
                                                            <span class="data-category-theme" style="color: <?php echo $not_shared_data['data_category_theme_color']; ?>"><?php echo $not_shared_data['data_category_theme']; ?></span><br/>
                                                            <?php $not_shared_data_category_names = ''; ?>
                                                            <?php foreach($not_shared_data['data_category_names'] as $not_shared_data_category_name) {
                                                                $not_shared_data_category_names .= '<a class="data-category" href="' . $not_shared_data_category_name['link'] . '">' . $not_shared_data_category_name['name'] . '</a>, ';
                                                            }

                                                            echo substr($not_shared_data_category_names, 0, -2);

                                                            ?>

                                                        </p>
                                                    <?php } ?>
                                                </div>
                                                <div class="col-12 col-lg-6">
                                                </div>
                                            </div>
                                        </div>

                                        <?php if (! empty($modal_data['belgium_data'])) { ?>
                                            <div class="tab-pane fade show" id="beneficiary-content-belgium-<?php echo $modal_data['id']; ?>" role="tabpanel">
                                                <div class="row">
                                                    <div class="col-12">
                                                        <h5><?php echo get_custom_label('modal_belgium_tab_title'); ?></h5>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-12 col-lg-6">
                                                        <h6 class="mb-3"><i class="fa fa-bank"></i>&nbsp;&nbsp;<?php echo get_custom_label('modal_owner_heading'); ?></h6>
                                                        <p><a href="<?php echo $modal_data['authority']['authority_link']; ?>" class="authority-name"><?php echo $modal_data['authority']['authority_name']; ?></a></p>
                                                    </div>
                                                    <div class="col-12 col-lg-6">
                                                        <h6 class="mb-3"><i class="fa fa-users"></i>&nbsp;&nbsp;<?php echo get_custom_label('modal_involved_parties_heading'); ?></h6>
                                                        <ul>
                                                            <?php foreach($modal_data['involved_parties_categories'] as $involved_parties_category) { ?>
                                                                <li><?php echo $involved_parties_category; ?></li>
                                                            <?php } ?>
                                                        </ul>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-12 col-lg-6">
                                                        <h6 class="mb-3"><i class="fa fa-share-alt"></i>&nbsp;&nbsp;<?php echo get_custom_label('modal_data_heading'); ?></h6>
                                                        <?php foreach ($modal_data['belgium_data'] as $belgium_data) { ?>
                                                            <p class="mb-2">
                                                                <span class="data-category-theme" style="color: <?php echo $belgium_data['data_category_theme_color']; ?>"><?php echo $belgium_data['data_category_theme']; ?></span><br/>
                                                                <?php $belgium_data_category_names = ''; ?>
                                                                <?php foreach($belgium_data['data_category_names'] as $belgium_data_category_name) {
                                                                    $belgium_data_category_names .= '<a class="data-category" href="' . $belgium_data_category_name['link'] . '">' . $belgium_data_category_name['name'] . '</a>, ';
                                                                }

                                                                echo substr($belgium_data_category_names, 0, -2);

                                                                ?>

                                                            </p>
                                                        <?php } ?>
                                                    </div>
                                                    <div class="col-12 col-lg-6">
                                                        <h6 class="mb-3"><i class="fa fa-database"></i>&nbsp;&nbsp;<?php echo get_custom_label('modal_beneficiaries_heading'); ?></h6>
                                                        <?php if (count($modal_data['belgium_beneficiaries']) == 0) { ?>
                                                        <p><?php echo get_custom_label('modal_no_beneficiaries'); ?></p>
                                                        <?php } else { ?>
                                                        <p> 
                                                            <?php
                                                                $belgium_beneficiaries = '';

                                                                foreach ($modal_data['belgium_beneficiaries'] as $beneficiary) {
                                                                    $belgium_beneficiaries .= '<a class="beneficiary" href="' . $beneficiary['link'] . '">' . $beneficiary['name'] . '</a>, ';
                                                                
                                                                }

                                                                echo substr($belgium_beneficiaries, 0, -2);

                                                            ?>
                                                        </p>
                                                        <?php } ?>
                                                    </div>
                                                </div>
                                            </div>
                                        <?php } ?>

                                        <?php if (! empty($modal_data['europe_data'])) { ?>
                                            <div class="tab-pane fade" id="beneficiary-content-europe-<?php echo $modal_data['id']; ?>" role="tabpanel">
                                                <div class="row">
                                                    <div class="col-12">
                                                        <h5><?php echo get_custom_label('modal_europe_tab_title'); ?></h5>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-12 col-lg-6">
                                                        <h6 class="mb-3"><i class="fa fa-bank"></i>&nbsp;&nbsp;<?php echo get_custom_label('modal_owner_heading'); ?></h6>
                                                        <p><a href="<?php echo $modal_data['authority']['authority_link']; ?>" class="authority-name"><?php echo $modal_data['authority']['authority_name']; ?></a></p>
                                                    </div>
                                                    <div class="col-12 col-lg-6">
                                                        <h6 class="mb-3"><i class="fa fa-users"></i>&nbsp;&nbsp;<?php echo get_custom_label('modal_involved_parties_heading'); ?></h6>
                                                        <ul>
                                                            <?php foreach($modal_data['involved_parties_categories'] as $involved_parties_category) { ?>
                                                                <li><?php echo $involved_parties_category; ?></li>
                                                            <?php } ?>
                                                        </ul>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-12 col-lg-6">
                                                        <h6 class="mb-3"><i class="fa fa-share-alt"></i>&nbsp;&nbsp;<?php echo get_custom_label('modal_data_heading'); ?></h6>
                                                        <?php foreach($modal_data['europe_data'] as $europe_data) { ?>
                                                            <p class="mb-2">
                                                                <span class="data-category-theme" style="color: <?php echo $europe_data['data_category_theme_color']; ?>"><?php echo $europe_data['data_category_theme']; ?></span><br/>
                                                                <?php $europe_data_category_names = ''; ?>
                                                                <?php foreach($europe_data['data_category_names'] as $europe_data_category_name) {
                                                                    $europe_data_category_names .= '<a class="data-category" href="' . $europe_data_category_name['link'] . '">' . $europe_data_category_name['name'] . '</a>, ';
                                                                }

                                                                echo substr($europe_data_category_names, 0, -2);

                                                                ?>

                                                            </p>
                                                        <?php } ?>
                                                        
                                                    </div>
                                                    <div class="col-12 col-lg-6">
                                                        <h6 class="mb-3"><i class="fa fa-database"></i>&nbsp;&nbsp;<?php echo get_custom_label('modal_beneficiaries_heading'); ?></h6>
                                                        <?php if (count($modal_data['europe_beneficiaries']) == 0) { ?>
                                                            <p><?php echo get_custom_label('modal_no_beneficiaries'); ?></p>
                                                        <?php } else { ?>
                                                            <p> 
                                                                <?php
                                                                    $europe_beneficiaries = '';

                                                                    foreach ($modal_data['europe_beneficiaries'] as $beneficiary) {
                                                                        $europe_beneficiaries .= '<a class="beneficiary" href="' . $beneficiary['link'] . '">' . $beneficiary['name'] . '</a>, ';
                                                                    }

                                                                    echo substr($europe_beneficiaries, 0, -2);

                                                                ?>
                                                            </p>
                                                        <?php } ?>
                                                    </div>
                                                </div>
                                            </div>
                                        <?php } ?>

                                        <?php if (! empty($modal_data['world_data'])) { ?>
                                            <div class="tab-pane fade" id="beneficiary-content-world-<?php echo $modal_data['id']; ?>" role="tabpanel">
                                                <div class="row">
                                                    <div class="col-12">
                                                        <h5><?php echo get_custom_label('modal_world_tab_title'); ?></h5>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-12 col-lg-6">
                                                        <h6 class="mb-3"><i class="fa fa-bank"></i>&nbsp;&nbsp;<?php echo get_custom_label('modal_owner_heading'); ?></h6>
                                                        <p><a href="<?php echo $modal_data['authority']['authority_link']; ?>" class="authority-name"><?php echo $modal_data['authority']['authority_name']; ?></a></p>
                                                    </div>
                                                    <div class="col-12 col-lg-6">
                                                       <h6 class="mb-3"><i class="fa fa-users"></i>&nbsp;&nbsp;<?php echo get_custom_label('modal_involved_parties_heading'); ?></h6>
                                                       <ul>
                                                           <?php foreach($modal_data['involved_parties_categories'] as $involved_parties_category) { ?>
                                                               <li><?php echo $involved_parties_category; ?></li>
                                                           <?php } ?>
                                                       </ul>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-12 col-lg-6">
                                                        <h6 class="mb-3"><i class="fa fa-share-alt"></i>&nbsp;&nbsp;<?php echo get_custom_label('modal_data_heading'); ?></h6>
                                                        <?php foreach($modal_data['world_data'] as $world_data) { ?>
                                                            <p class="mb-2">
                                                                <span class="data-category-theme" style="color: <?php echo $world_data['data_category_theme_color']; ?>"><?php echo $world_data['data_category_theme']; ?></span><br/>
                                                                <?php $world_data_category_names = ''; ?>
                                                                <?php foreach($world_data['data_category_names'] as $world_data_category_name) {
                                                                    $world_data_category_names .= '<a class="data-category" href="' . $world_data_category_name['link'] . '">' . $world_data_category_name['name'] . '</a>, ';
                                                                }

                                                                echo substr($world_data_category_names, 0, -2);

                                                                ?>

                                                            </p>
                                                        <?php } ?>
                                                    </div>
                                                    <div class="col-12 col-lg-6">
                                                        <h6 class="mb-3"><i class="fa fa-database"></i>&nbsp;&nbsp;<?php echo get_custom_label('modal_beneficiaries_heading'); ?></h6>
                                                        <?php if (count($modal_data['world_beneficiaries']) == 0) { ?>
                                                            <p><?php echo get_custom_label('modal_no_beneficiaries'); ?></p>
                                                        <?php } else { ?>
                                                            <p> 
                                                                <?php
                                                                    $world_beneficiaries = '';

                                                                    foreach($modal_data['world_beneficiaries'] as $beneficiary) {
                                                                        $world_beneficiaries .= '<a class="beneficiary" href="' . $beneficiary['link'] . '">' . $beneficiary['name'] . '</a>, ';
                                                                    }

                                                                    echo substr($world_beneficiaries, 0, -2);

                                                                ?>
                                                            </p>
                                                        <?php } ?>
                                                    </div>
                                                </div>
                                            </div>
                                        <?php } ?>
                                    <?php } else { ?>
                                        <?php if (! empty($modal_data['belgium_data'])) { ?>
                                            <div class="tab-pane fade show active" id="beneficiary-content-belgium-<?php echo $modal_data['id']; ?>" role="tabpanel">
                                                <div class="row">
                                                    <div class="col-12">
                                                        <h5><?php echo get_custom_label('modal_belgium_tab_title'); ?></h5>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-12 col-lg-6">
                                                        <h6 class="mb-3"><i class="fa fa-bank"></i>&nbsp;&nbsp;<?php echo get_custom_label('modal_owner_heading'); ?></h6>
                                                        <p><a href="<?php echo $modal_data['authority']['authority_link']; ?>" class="authority-name"><?php echo $modal_data['authority']['authority_name']; ?></a></p>
                                                    </div>
                                                    <div class="col-12 col-lg-6">
                                                        <h6 class="mb-3"><i class="fa fa-users"></i>&nbsp;&nbsp;<?php echo get_custom_label('modal_involved_parties_heading'); ?></h6>
                                                        <ul>
                                                            <?php foreach($modal_data['involved_parties_categories'] as $involved_parties_category) { ?>
                                                                <li><?php echo $involved_parties_category; ?></li>
                                                            <?php } ?>
                                                        </ul>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-12 col-lg-6">
                                                        <h6 class="mb-3"><i class="fa fa-share-alt"></i>&nbsp;&nbsp;<?php echo get_custom_label('modal_data_heading'); ?></h6>
                                                        <?php foreach ($modal_data['belgium_data'] as $belgium_data) { ?>
                                                            <p class="mb-2">
                                                                <span class="data-category-theme" style="color: <?php echo $belgium_data['data_category_theme_color']; ?>"><?php echo $belgium_data['data_category_theme']; ?></span><br/>
                                                                <?php $belgium_data_category_names = ''; ?>
                                                                <?php foreach($belgium_data['data_category_names'] as $belgium_data_category_name) {
                                                                    $belgium_data_category_names .= '<a class="data-category" href="' . $belgium_data_category_name['link'] . '">' . $belgium_data_category_name['name'] . '</a>, ';
                                                                }

                                                                echo substr($belgium_data_category_names, 0, -2);

                                                                ?>

                                                            </p>
                                                        <?php } ?>
                                                    </div>
                                                    <div class="col-12 col-lg-6">
                                                        <h6 class="mb-3"><i class="fa fa-database"></i>&nbsp;&nbsp;<?php echo get_custom_label('modal_beneficiaries_heading'); ?></h6>
                                                        <?php if (count($modal_data['belgium_beneficiaries']) == 0) { ?>
                                                        <p><?php echo get_custom_label('modal_no_beneficiaries'); ?></p>
                                                        <?php } else { ?>
                                                        <p> 
                                                            <?php
                                                                $belgium_beneficiaries = '';

                                                                foreach ($modal_data['belgium_beneficiaries'] as $beneficiary) {
                                                                    $belgium_beneficiaries .= '<a class="beneficiary" href="' . $beneficiary['link'] . '">' . $beneficiary['name'] . '</a>, ';
                                                                
                                                                }

                                                                echo substr($belgium_beneficiaries, 0, -2);

                                                            ?>
                                                        </p>
                                                        <?php } ?>
                                                    </div>
                                                </div>
                                            </div>
                                        <?php } ?>

                                        <?php if (! empty($modal_data['europe_data'])) { ?>
                                            <div class="tab-pane fade" id="beneficiary-content-europe-<?php echo $modal_data['id']; ?>" role="tabpanel">
                                                <div class="row">
                                                    <div class="col-12">
                                                        <h5><?php echo get_custom_label('modal_europe_tab_title'); ?></h5>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-12 col-lg-6">
                                                        <h6 class="mb-3"><i class="fa fa-bank"></i>&nbsp;&nbsp;<?php echo get_custom_label('modal_owner_heading'); ?></h6>
                                                        <p><a href="<?php echo $modal_data['authority']['authority_link']; ?>" class="authority-name"><?php echo $modal_data['authority']['authority_name']; ?></a></p>
                                                    </div>
                                                    <div class="col-12 col-lg-6">
                                                        <h6 class="mb-3"><i class="fa fa-users"></i>&nbsp;&nbsp;<?php echo get_custom_label('modal_involved_parties_heading'); ?></h6>
                                                        <ul>
                                                            <?php foreach($modal_data['involved_parties_categories'] as $involved_parties_category) { ?>
                                                                <li><?php echo $involved_parties_category; ?></li>
                                                            <?php } ?>
                                                        </ul>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-12 col-lg-6">
                                                        <h6 class="mb-3"><i class="fa fa-share-alt"></i>&nbsp;&nbsp;<?php echo get_custom_label('modal_data_heading'); ?></h6>
                                                        <?php foreach($modal_data['europe_data'] as $europe_data) { ?>
                                                            <p class="mb-2">
                                                                <span class="data-category-theme" style="color: <?php echo $europe_data['data_category_theme_color']; ?>"><?php echo $europe_data['data_category_theme']; ?></span><br/>
                                                                <?php $europe_data_category_names = ''; ?>
                                                                <?php foreach($europe_data['data_category_names'] as $europe_data_category_name) {
                                                                    $europe_data_category_names .= '<a class="data-category" href="' . $europe_data_category_name['link'] . '">' . $europe_data_category_name['name'] . '</a>, ';
                                                                }

                                                                echo substr($europe_data_category_names, 0, -2);

                                                                ?>

                                                            </p>
                                                        <?php } ?>
                                                        
                                                    </div>
                                                    <div class="col-12 col-lg-6">
                                                        <h6 class="mb-3"><i class="fa fa-database"></i>&nbsp;&nbsp;<?php echo get_custom_label('modal_beneficiaries_heading'); ?></h6>
                                                        <?php if (count($modal_data['europe_beneficiaries']) == 0) { ?>
                                                            <p><?php echo get_custom_label('modal_no_beneficiaries'); ?></p>
                                                        <?php } else { ?>
                                                            <p> 
                                                                <?php
                                                                    $europe_beneficiaries = '';

                                                                    foreach ($modal_data['europe_beneficiaries'] as $beneficiary) {
                                                                        $europe_beneficiaries .= '<a class="beneficiary" href="' . $beneficiary['link'] . '">' . $beneficiary['name'] . '</a>, ';
                                                                    }

                                                                    echo substr($europe_beneficiaries, 0, -2);

                                                                ?>
                                                            </p>
                                                        <?php } ?>
                                                    </div>
                                                </div>
                                            </div>
                                        <?php } ?>

                                        <?php if (! empty($modal_data['world_data'])) { ?>
                                            <div class="tab-pane fade" id="beneficiary-content-world-<?php echo $modal_data['id']; ?>" role="tabpanel">
                                                <div class="row">
                                                    <div class="col-12">
                                                        <h5><?php echo get_custom_label('modal_world_tab_title'); ?></h5>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-12 col-lg-6">
                                                        <h6 class="mb-3"><i class="fa fa-bank"></i>&nbsp;&nbsp;<?php echo get_custom_label('modal_owner_heading'); ?></h6>
                                                        <p><a href="<?php echo $modal_data['authority']['authority_link']; ?>" class="authority-name"><?php echo $modal_data['authority']['authority_name']; ?></a></p>
                                                    </div>
                                                    <div class="col-12 col-lg-6">
                                                       <h6 class="mb-3"><i class="fa fa-users"></i>&nbsp;&nbsp;<?php echo get_custom_label('modal_involved_parties_heading'); ?></h6>
                                                       <ul>
                                                           <?php foreach($modal_data['involved_parties_categories'] as $involved_parties_category) { ?>
                                                               <li><?php echo $involved_parties_category; ?></li>
                                                           <?php } ?>
                                                       </ul>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-12 col-lg-6">
                                                        <h6 class="mb-3"><i class="fa fa-share-alt"></i>&nbsp;&nbsp;<?php echo get_custom_label('modal_data_heading'); ?></h6>
                                                        <?php foreach($modal_data['world_data'] as $world_data) { ?>
                                                            <p class="mb-2">
                                                                <span class="data-category-theme" style="color: <?php echo $world_data['data_category_theme_color']; ?>"><?php echo $world_data['data_category_theme']; ?></span><br/>
                                                                <?php $world_data_category_names = ''; ?>
                                                                <?php foreach($world_data['data_category_names'] as $world_data_category_name) {
                                                                    $world_data_category_names .= '<a class="data-category" href="' . $world_data_category_name['link'] . '">' . $world_data_category_name['name'] . '</a>, ';
                                                                }

                                                                echo substr($world_data_category_names, 0, -2);

                                                                ?>

                                                            </p>
                                                        <?php } ?>
                                                    </div>
                                                    <div class="col-12 col-lg-6">
                                                        <h6 class="mb-3"><i class="fa fa-database"></i>&nbsp;&nbsp;<?php echo get_custom_label('modal_beneficiaries_heading'); ?></h6>
                                                        <?php if (count($modal_data['world_beneficiaries']) == 0) { ?>
                                                            <p><?php echo get_custom_label('modal_no_beneficiaries'); ?></p>
                                                        <?php } else { ?>
                                                            <p> 
                                                                <?php
                                                                    $world_beneficiaries = '';

                                                                    foreach($modal_data['world_beneficiaries'] as $beneficiary) {
                                                                        $world_beneficiaries .= '<a class="beneficiary" href="' . $beneficiary['link'] . '">' . $beneficiary['name'] . '</a>, ';
                                                                    }

                                                                    echo substr($world_beneficiaries, 0, -2);

                                                                ?>
                                                            </p>
                                                        <?php } ?>
                                                        
                                                    </div>
                                                </div>
                                              
                                            </div>
                                        <?php } ?>
                                    <?php } ?>
                                </div>
                            </div>
                        </div>
                    </div>
                    <?php } ?>
                    <?php } ?>
                </div>

                <div class="row">
                    <div class="col-12 col-md-6 col-lg-5 col-xl-4 pl-0 pr-0" id="beneficiary-content-left-pane">
                        <div id="beneficiary-content-left-pane-title" class="d-flex align-items-center">
                            <span>
                                <?php echo sprintf(get_custom_label('beneficiary_content_nav_title'), "<b>" . strtolower(get_the_title($post->ID)) . "</b>"); ?>
                            </span>
                        </div>
                        <div id="beneficiary-accordion-xs" class="d-sm-none" role="tablist">
                            <?php $data_categories_themes = $response_beneficiaries[2]; ?>
                            <?php for ($i = 0; $i < count($data_categories_themes); $i++) { ?>
                                <div class="card" style="background: <?php echo $data_categories_themes[$i]['data_category_theme_color']; ?>">
                                    <div class="card-header" role="tab" style="background: <?php echo $data_categories_themes[$i]['data_category_theme_color']; ?>">
                                        <h5 class="mb-0">
                                            <a class="collapsed" data-toggle="collapse" href="#beneficiary-category-xs-<?php echo $i; ?>" aria-expanded="false" aria-controls="beneficiary-category-xs-<?php echo $i; ?>">
                                                <?php echo $data_categories_themes[$i]['data_category_theme']; ?>
                                            </a>
                                        </h5>
                                    </div>

                                    <div id="beneficiary-category-xs-<?php echo $i; ?>" class="collapse" role="tabpanel" data-parent="#beneficiary-accordion-xs">
                                        <div class="card-body py-0 pr-0">
                                            <div class="nav flex-column nav-pills" role="tablist" aria-orientation="vertical">
                                            <?php $j = 0; ?>
                                            <?php foreach ($data_categories_themes[$i]['data_category_names'] as $data_category_names) { ?>
                                                <a class="nav-link" data-toggle="pill" href="#tab-pane-beneficiary-<?php echo $i; ?>-<?php echo $j; ?>" role="tab" aria-controls="tab-pane-beneficiary-<?php echo $i; ?>-<?php echo $j; ?>" aria-selected="false"><?php echo $data_category_names; ?></a>
                                            <?php $j++; ?>
                                            <?php } ?>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            <?php } ?>
                        </div>
                        <div id="beneficiary-accordion" class="d-none d-sm-block" role="tablist">
                            <?php for ($i = 0; $i < count($data_categories_themes); $i++) { ?>
                                <div class="card" style="background: <?php echo $data_categories_themes[$i]['data_category_theme_color']; ?>">
                                    <div class="card-header" role="tab" style="background: <?php echo $data_categories_themes[$i]['data_category_theme_color']; ?>">
                                        <h5 class="mb-0">
                                            <a class="<?php echo ($i == 0 ? '' : 'collapsed'); ?>" data-toggle="collapse" href="#beneficiary-category-<?php echo $i; ?>" aria-expanded="<?php echo ($i == 0 ? 'true' : 'false'); ?>" aria-controls="beneficiary-category-<?php echo $i; ?>">
                                                <?php echo $data_categories_themes[$i]['data_category_theme']; ?>
                                            </a>
                                        </h5>
                                    </div>

                                    <div id="beneficiary-category-<?php echo $i; ?>" class="collapse <?php echo ($i == 0 ? 'show' : ''); ?>" role="tabpanel" data-parent="#beneficiary-accordion">
                                        <div class="card-body py-0 pr-0">
                                            <div class="nav flex-column nav-pills" role="tablist" aria-orientation="vertical">
                                            <?php $j = 0; ?>
                                            <?php foreach ($data_categories_themes[$i]['data_category_names'] as $data_category_names) { ?>
                                                <a class="nav-link<?php echo ($i == 0 && $j == 0 ? ' active' : ''); ?>" data-toggle="pill" href="#tab-pane-beneficiary-<?php echo $i; ?>-<?php echo $j; ?>" role="tab" aria-controls="tab-pane-beneficiary-<?php echo $i; ?>-<?php echo $j; ?>" aria-selected="<?php echo ($i == 0 && $j == 0 ? 'true' : 'false'); ?>"><?php echo $data_category_names; ?></a>
                                            <?php $j++; ?>
                                            <?php } ?>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            <?php } ?>
                        </div>
                    </div>
                    <div class="d-none d-sm-block col-12 col-md-6 col-lg-7 col-xl-8 pl-0 pr-0" id="beneficiary-content-right-pane">
                        <div class="tab-content pt-0">
                            <?php 
                            for ($i = 0; $i < count($data_categories_themes); $i++) {

                                $j = 0; 

                                foreach ($data_categories_themes[$i]['data_category_names'] as $data_category_name) { ?>

                                    <div class="tab-pane<?php echo ($i == 0 && $j == 0 ? ' active' : ''); ?>" id="tab-pane-beneficiary-<?php echo $i; ?>-<?php echo $j; ?>" role="tabpanel" aria-labelledby="tab-pane-beneficiary-<?php echo $i; ?>-<?php echo $j; ?>">

                                        <div class="d-sm-none beneficiary-content-right-pane-title-xs d-flex align-items-center justify-content-start">
                                            <div class="p-3">
                                                <a href="javascript:void(0);"><i class="fa fa-chevron-left p-1"></i></a>
                                            </div>
                                            <div>
                                                <a href="javascript:void(0);"><span class="title-xs"><?php echo $data_category_name; ?></span></a>
                                            </div>
                                        </div>

                                        <div class="d-none beneficiary-content-right-pane-title d-sm-flex align-items-center">
                                            <span>
                                                <?php echo $data_category_name; ?>
                                            </span>
                                        </div>

                                        <div class="card-columns-container">
                                            <div class="card-columns">

                                                <?php foreach ($response_beneficiaries[3] as $prd_per_dc_name) {

                                                    // if the data category is the current one
                                                    if ($data_category_name == $prd_per_dc_name['data_category_name']) {

                                                        // public items
                                                        if ($prd_per_dc_name['visibility'] == 'Publiek / publique') { 

                                                            foreach ($prd_per_dc_name['prd_data'] as $data) { ?>

                                                                <div class="card public" data-prd="authority-modal-<?php echo $data['id']; ?>">
                                                                    <div class="card-body">
                                                                        <h6 class="card-title">
                                                                            <i class="fa public" style="color: <?php echo $data_categories_themes[$i]['data_category_theme_color']; ?>"></i>&nbsp;&nbsp;<?php echo $data['name']; ?>
                                                                        </h6>
                                                                        <p class="card-text"><?php echo $data['description']; ?></p>
                                                                        <div class="card-footer">
                                                                            <small><span class="text-uppercase objective-title"><?php echo get_custom_label('card_objective') . '</span>&nbsp;' . $data['objective']; ?></small>
                                                                        </div>
                                                                    </div>
                                                                </div>

                                                            <?php } ?>

                                                        <?php
                                                        // semi-public items
                                                        } elseif ($prd_per_dc_name['visibility'] == 'Beperkt publiek / publique avec restrictions') {

                                                            foreach ($prd_per_dc_name['prd_data'] as $data) { ?>

                                                                <div class="card private" data-prd="authority-modal-<?php echo $data['id']; ?>">
                                                                    <div class="card-body">
                                                                        <h6 class="card-title"><i class="fa private text-danger"></i>&nbsp;&nbsp;<?php echo $data['name']; ?></h6>
                                                                        <p class="card-text"><?php echo $data['description']; ?></p>
                                                                    </div>
                                                                </div>

                                                            <?php } ?>

                                                        <?php } else { ?>

                                                            <div class="card private">
                                                                <div class="card-body">
                                                                    <h6 class="card-title">
                                                                        <i class="fa private text-danger"></i>&nbsp;&nbsp;<?php echo sprintf(get_custom_label('card_private_title'), $prd_per_dc_name['private_data_amount']); ?>
                                                                    </h6>
                                                                    <p class="card-text"><?php echo get_custom_label('card_private_description'); ?></p>
                                                                </div>
                                                            </div>

                                                        <?php } ?>

                                                    <?php } ?>
            
                                                <?php } ?>

                                            </div>
                                        </div>

                                    </div>

                                <?php

                                    $j++;
            
                                }

                            }

                         ?>
                        </div>
                    </div>
                </div>

            </section>

        <?php } ?>
        
    <?php } ?>

<?php get_footer(); ?>