/* Custom Javascript scripts */

// this function triggers when the user clicks on the menu items in the navigation bar (lg, xl resolutions)
$('#main-menu-navbar .nav-link').on('click', function() {

	if ($(this).data('menu') != undefined) {

		// removing active class of all navigation menu items
		$('#main-menu-navbar .nav-link').removeClass('active');

		// if main menu is hidden
		if ($('#main-menu').hasClass('d-none')) {

			// preventing body from scrolling
			$('body').addClass('noScroll');

			// displaying sub-menu and corresponding left pane
			$('#main-menu').removeClass('d-none');
			$('#' + $(this).data('menu')).removeClass('d-none');

			// adding active class to selected menu item
			$(this).addClass('active');

		}
		// if main menu is displayed
		else {
			
			// if the displayed submenu is not the one corresponding to the nav link
			if ($('#' + $(this).data('menu')).hasClass('d-none')) {

				// resetting background colors of all sub menu items
				$('#main-menu .nav-link').removeAttr('style');

				// hiding all chevron-right icons
				$('#main-menu .fa-chevron-right').addClass('d-none');

				// resetting background colors of all right panes
				$('.main-menu-right-pane').removeAttr('style');

				// hiding all tab panes of right panes
				$('.main-menu-right-pane .tab-pane').removeClass('show active');

				// hiding all sub-menus
				$('#main-menu .row').addClass('d-none');

				// showing required sub-menu
				$('#' + $(this).data('menu')).removeClass('d-none');

				// adding active class to selected menu item
				$(this).addClass('active');

			}
			else {

				hide_main_menu();

			}
			
		}

	}

});


// this function triggers when the user clicks on the sub menu items in the left navigation pane (lg, xl resolutions)
$('#main-menu .nav-link').on('click', function() {

	// resetting background colors of all sub menu items
	$('#main-menu .nav-link').removeAttr('style');

	// hiding all chevron-right icons
	$('#main-menu .fa-chevron-right').addClass('d-none');

	// showing chevron-right icon
	$(this).find('i').removeClass('d-none');

	// changing background color of selected item (if any)
	if ($(this).data('bgcolor')) {

		$(this).css({'background': $(this).data('bgcolor'), 'border-top': '1px solid ' + $(this).data('bgcolor')});
		$('#' + $(this).data('pane')).css('background', $(this).data('bgcolor'));
		$($(this).attr('href')).addClass('show active');

	}

	// changing font color
	if ($(this).data('color')) {

		$(this).css({'color': $(this).data('color')});

	}

});

// this function triggers when the user clicks on the cross icon in the left navigation pane
$('.hide-main-menu').on('click', function() {

	hide_main_menu();

});

// this function triggers when the user clicks on the hamburger icon in the navigation
$('.custom-toggler').on('click', function() {

	// preventing body from scrolling
	$('body').addClass('noScroll');

	$('#main-menu-mobile').removeClass('d-none');
	$('#main-nav-mobile').addClass('d-none');

	$('#main-menu-top-level-mobile-header').removeClass('d-none');
	$('#main-menu-top-level-mobile-content').removeClass('d-none');

});

// this function triggers when the user clicks on "X CLOSE" in the mobile navigation
$('#main-menu-top-level-mobile-header a').on('click', function() {

	// allowing body to scroll
	$('body').removeClass('noScroll');

	$('#main-menu-mobile').addClass('d-none');
	$('#main-nav-mobile').removeClass('d-none');

	$('#main-menu-top-level-mobile-header').addClass('d-none');
	$('#main-menu-top-level-mobile-content').addClass('d-none');

});

// this function triggers when the user clicks on the top menu items in the navigation
$('#main-menu-top-level-mobile-content .nav-link').on('click', function () {

	if ($(this).data('submenu') != undefined) {

		// hiding main menu
		$('#main-menu-top-level-mobile-header').addClass('d-none');
		$('#main-menu-top-level-mobile-content').addClass('d-none');

		// showing corresponding sub-menu
		$('#' + $(this).data('submenu') + '-mobile-header').removeClass('d-none');
		$('#' + $(this).data('submenu') + '-mobile-content').removeClass('d-none');

	}

});

// this function triggers when the user clicks on the header link of a sub-menu
$('.sub-menu-mobile-header').on('click', function() {

	$('#main-menu-top-level-mobile-header').removeClass('d-none');
	$('#main-menu-top-level-mobile-content').removeClass('d-none');

	$(this).closest('nav').addClass('d-none');
	$(this).closest('nav').next().addClass('d-none');

});

$('.sub-menu-mobile-header').on('click', function() {

	$(this).closest('nav').addClass('d-none');
	$(this).closest('nav').next().addClass('d-none');

	$('#' + $(this).data('submenu') + '-mobile-header').removeClass('d-none');
	$('#' + $(this).data('submenu') + '-mobile-content').removeClass('d-none');

});

$('.sub-menu-mobile-content a').on('click', function() {

	$(this).closest('div').addClass('d-none');
	$(this).closest('div').prev().addClass('d-none');

	$('#' + $(this).data('submenu') + '-mobile-header').removeClass('d-none');
	$('#' + $(this).data('submenu') + '-mobile-content').removeClass('d-none');

});

// this function triggers when the user clicks on the header link of a sub sub-menu
$('.sub-submenu-mobile-header a').on('click', function() {

	$(this).closest('nav').addClass('d-none');
	$(this).closest('nav').next().addClass('d-none');

	$('#' + $(this).data('submenu') + '-header').removeClass('d-none');
	$('#' + $(this).data('submenu') + '-content').removeClass('d-none');

});

// this function hides the main menu (lg, xl resolutions)
function hide_main_menu() {

	// removing border-top inline styles of all menu items
	$('#main-menu-navbar .nav-link').removeClass('active');

	// resetting background colors of all sub menu items
	$('#main-menu .nav-link').removeAttr('style');

	// hiding all chevron-right icons
	$('#main-menu .fa-chevron-right').addClass('d-none');

	// resetting background colors of all right panes
	$('.main-menu-right-pane').removeAttr('style');

	// hiding all tab panes of right panes
	$('.main-menu-right-pane .tab-pane').removeClass('show active');

	// hiding all main menus
	$('#main-menu .row').addClass('d-none');
	$('#main-menu').addClass('d-none');

	// allowing body to scroll
	$('body').removeClass('noScroll');

}

// this function hides the main menu (xs, sm, md resolutions)
function hide_main_menu_mobile() {

	$('#main-menu-mobile').addClass('d-none');
	$('#main-nav-mobile').addClass('d-none');

	// allowing body to scroll
	$('body').removeClass('noScroll');

}


$('.card-header a').on('click', function() {

	$(this).find('i').toggleClass('fa-angle-down fa-angle-up');

});

$('#authority-accordion .collapse .nav-link').on('click', function() {

	$('#authorities-content-left-pane .nav-link').removeClass('active').attr('aria-selected', 'false');
	$(this).addClass('active').attr('aria-selected', 'true');

	$('#authorities-content-right-pane .tab-pane').removeClass('active');
	$($(this).attr('href')).addClass('active');

});

$('#beneficiary-accordion .collapse .nav-link').on('click', function() {

	$('#beneficiary-content-left-pane .nav-link').removeClass('active').attr('aria-selected', 'false');
	$(this).addClass('active').attr('aria-selected', 'true');

	$('#beneficiary-content-right-pane .tab-pane').removeClass('active');
	$($(this).attr('href')).addClass('active');

});

$('#authorities-content-right-pane .card.public').on('click', function() {

	// hiding all modal-dalogs
	$('#authority-prd-modal .modal-dialog').addClass('d-none');

	// showing required modal dialog
	$('#' + $(this).data('prd')).removeClass('d-none');

	// showing modal
	$('#authority-prd-modal').modal('show');

});

$('#beneficiary-content-right-pane .card.public').on('click', function() {

	// hiding all modal-dalogs
	$('#beneficiary-prd-modal .modal-dialog').addClass('d-none');

	// showing required modal dialog
	$('#' + $(this).data('prd')).removeClass('d-none');

	// showing modal
	$('#beneficiary-prd-modal').modal('show');

});

$('#data-content-right-pane .card.public').on('click', function() {

	// hiding all modal-dalogs
	$('#data-prd-modal .modal-dialog').addClass('d-none');

	// showing required modal dialog
	$('#' + $(this).data('prd')).removeClass('d-none');

	// showing modal
	$('#data-prd-modal').modal('show');

});

function scrollToSection(target) {

	$('html, body').animate({
        scrollTop: ($(target).offset().top - 80)
    }, 500);

}

$('#authorities-contact-form button').on('click', function() {

	var email = $('#authorities-contact-form-email').val().trim();
	var message = $('#authorities-contact-form-message').val().trim();

	var email_regex = /^[a-zA-Z0-9._-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,4}$/i;

	$('#authorities-contact-form').find('input, textarea').removeClass('valid').removeClass('invalid');
	$('#authorities-contact-form').find('.invalid-field-feedback').addClass('d-none');

	if (email == '' || !email_regex.test(email)) {
		$('#authorities-contact-form-email').addClass('invalid');
		$('#authorities-contact-form-email').next().removeClass('d-none');
	}
	else {

		$('#authorities-contact-form-email').addClass('valid');

	}

	if (message == '') {
		$('#authorities-contact-form-message').addClass('invalid');
		$('#authorities-contact-form-message').next().removeClass('d-none');
	}
	else {
		$('#authorities-contact-form-message').addClass('valid');
	}

	if (email != '' && message != '') {

		$('#authorities-contact-form button').attr("disabled", true);
		$('#authorities-contact-form button').html('<i class="fa fa-spinner fa-pulse fa-fw" aria-hidden="true"></i>&nbsp;' + $('#authorities-contact-form button').data('loading'));

		setTimeout(function() {

			var url = $('#authorities-contact-form').find('input[name=url]').val();

			var xhttp = new XMLHttpRequest();

		    xhttp.open("POST", currentUrl + "/scripts/send-email.php", true);
		    xhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");

		    xhttp.onreadystatechange = function() {

		        if (xhttp.readyState == 4 && xhttp.status == 200) {

		        	$('#authorities-contact-form').find('input, textarea').removeClass('valid').removeClass('invalid');
		        	$('#authorities-contact-form').find('.invalid-field-feedback').addClass('d-none');

		        	$('#authorities-contact-form button').attr("disabled", false).toggleClass('btn-light btn-success').html('<i class="fa fa-check" aria-hidden="true"></i>&nbsp;' + $('#authorities-contact-form button').data('success'));
		        	
		        	$('#authorities-contact-form').find('input, textarea').val('');

		        	setTimeout(function() {
		        		$('#authorities-contact-form button').toggleClass('btn-success btn-light').html($('#authorities-contact-form button').data('text'));
		        	}, 5000);

		        }

			};

		    xhttp.send("email="+email+"&message="+message+"&url="+url+"&authority="+currentAuthorityEmail+"&language="+currentLanguage);

		}, 2000);

	}

});

function goToLink(element) {

	var link = $(element).data('link');

	var win = window.open(link, '_blank');
  	win.focus();

}

$('#authority-accordion-xs .nav-link').on('click', function() {
	$('#authorities-content-left-pane').addClass('d-none');
	$('#authorities-content-right-pane').removeClass('d-none');
});

$('#beneficiary-accordion-xs .nav-link').on('click', function() {
	$('#beneficiary-content-left-pane').addClass('d-none');
	$('#beneficiary-content-right-pane').removeClass('d-none');
});

$('.authorities-content-right-pane-title-xs a').on('click', function() {
	$('#authorities-content-left-pane').removeClass('d-none');
	$('#authorities-content-right-pane').addClass('d-none');
});

$('.beneficiary-content-right-pane-title-xs a').on('click', function() {
	$('#beneficiary-content-left-pane').removeClass('d-none');
	$('#beneficiary-content-right-pane').addClass('d-none');
});

$('#data-nav-xs .nav-link').on('click', function() {
	$('#data-content-left-pane').addClass('d-none');
	$('#data-content-right-pane').removeClass('d-none');
});

$('.data-content-right-pane-title-xs a').on('click', function() {
	$('#data-content-left-pane').removeClass('d-none');
	$('#data-content-right-pane').addClass('d-none');
});


$('#authority-prd-modal').on('show.bs.modal', function () {
	$('#authority-modal-countries li:first-child a').click();
});

$('#data-prd-modal').on('show.bs.modal', function () {
	$('#data-modal-countries li:first-child a').click();
});

$('#homepage-authorities-picklist').on('change', function() {
	$('a[href="'+$(':selected', $(this)).attr('data-target')+'"]').trigger('click');
});