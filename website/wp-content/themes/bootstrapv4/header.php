<!DOCTYPE html>
<html <?php language_attributes(); ?>>
    <head>
        <meta charset="<?php bloginfo( 'charset' ); ?>">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
        <!--[if lt Ijavascript:void(0)E 9]>
        <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
        <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
        <![endif]-->
        <script type="text/javascript">
            var currentUrl = '<?= get_bloginfo("template_url"); ?>';
            var currentAuthorityEmail = '<?= get_post_meta(get_post_field('authority_id'), 'authority_post_email')[0]; ?>';
            var currentLanguage = '<?= get_current_language(); ?>';
        </script>
        <?php wp_head(); ?>
        
        <link href="https://fonts.googleapis.com/css?family=Roboto+Slab:100,300,400,700|Roboto:100,300,400,500,700,900" rel="stylesheet">
        <script src="https://use.fontawesome.com/3f7226f6dd.js"></script>

        <link rel="stylesheet" href="<?= get_bloginfo("template_url"); ?>/halo/styles/examples.css">
        <link rel="stylesheet" href="<?= get_bloginfo("template_url"); ?>/lib/styles/vizuly.css">
        <link rel="stylesheet" href="<?= get_bloginfo("template_url"); ?>/lib/styles/vizuly_halo.css">

        <script src="<?= get_bloginfo("template_url"); ?>/halo/lib/d3.min.js"></script>
        <script src="<?= get_bloginfo("template_url"); ?>/halo/lib/vizuly_core.min.js"></script>
        <script src="<?= get_bloginfo("template_url"); ?>/halo/lib/vizuly_halo.min.js"></script>
        <script src="<?= get_bloginfo("template_url"); ?>/halo/halo_test.js"></script>

    </head>

    <body <?php body_class(); ?>>

        <!-- xs, sm, md nav -->
        <section class="d-lg-none" id="main-nav-mobile-container">
            <nav class="navbar fixed-top navbar-expand-lg bg-white justify-content-between" id="main-nav-mobile">
                <button class="navbar-toggler custom-toggler" type="button">
                    <span class="navbar-toggler-icon"></span>
                </button>
                <a class="navbar-brand" href="<?php echo esc_url(home_url( '/' )); ?>">
                    <img src="<?= get_bloginfo("template_url"); ?>/img/logo.png" width="120" alt="" />
                </a>
                <ul class="navbar-nav" id="main-menu-search-mobile" style="visibility: hidden">
                    <li class="nav-item">
                        <a href="javascript:searchThis();" class="nav-link"><i class="fa fa-search fa-lg"></i></a>
                    </li>
                </ul>
            </nav>
            
            <div class="fixed-top d-none" id="main-menu-mobile">
                <nav class="navbar fixed-top d-none" id="main-menu-top-level-mobile-header">
                    <a href="javascript:void(0);" class="text-white">
                        <i class="fa fa-remove fa-lg"></i>&nbsp;&nbsp;&nbsp;Close
                    </a>
                </nav>
                <div class="fixed-top d-none" id="main-menu-top-level-mobile-content">
                    <ul class="navbar-nav mr-auto mt-lg-0">
                        <?php foreach (get_main_menu_items('header_menu') as $menu_item) { ?>
                        <li class="nav-item">
                            <?php if (substr($menu_item->url, 0, 1) == '#') { ?>
                            <a class="nav-link" href="javascript:void(0);" data-submenu="<?php echo substr($menu_item->url, 1); ?>"><?php echo $menu_item->title; ?></a>
                            <?php } else { ?>
                            <a class="nav-link" href="<?php echo $menu_item->url; ?>"><?php echo $menu_item->title; ?></a>
                            <?php } ?>
                        </li>
                        <?php } ?>
                    </ul>
                    <p id="main-menu-language-switcher-mobile">
                        <?php foreach (get_main_menu_languages() as $language) { ?>
                        <a class="text-white text-uppercase <?php echo ($language['code'] == substr(get_current_language(), 1, 2) ? "active" : ""); ?>" href="<?php echo $language['url']; ?>"><?php echo substr($language['code'], 0, 2); ?></a>
                        <?php } ?>
                    </p>
                </div>

                <nav class="navbar fixed-top d-none sub-menu-mobile-header" id="main-menu-data-mobile-header">
                    <a href="javascript:void(0);" class="text-dark" data-submenu="main-menu-top-level">
                        <i class="fa fa-chevron-left fa-lg"></i>&nbsp;&nbsp;&nbsp;<?php echo get_custom_label('main_submenu_data_title'); ?>
                    </a>
                </nav>

                <div class="fixed-top d-none sub-menu-mobile-content" id="main-menu-data-mobile-content">
                    <ul class="navbar-nav">
                        <?php foreach (get_data_categories_records_per_theme() as $data_category_theme) { ?>
                        <li class="nav-item" style="background:<?php echo $data_category_theme['color']; ?>">
                            <a class="nav-link" data-submenu="main-menu-data-<?php echo $data_category_theme['name']; ?>" data-bgcolor="<?php echo $data_category_theme['color']; ?>" href="javascript:void(0);"><?php echo $data_category_theme['label']; ?></a>
                        </li>
                        <?php } ?>
                    </ul>
                </div>
                
                <?php foreach (get_data_categories_records_per_theme() as $data_categories_records_per_theme) { ?>
                    <nav class="navbar fixed-top d-none sub-submenu-data-mobile-header sub-submenu-mobile-header" style="background:<?php echo $data_categories_records_per_theme['color']; ?>" id="<?php echo 'main-menu-data-' . $data_categories_records_per_theme['name'] . '-mobile-header'; ?>">
                        <a href="javascript:void(0);" class="text-white" data-submenu="main-menu-data-mobile">
                            <i class="fa fa-chevron-left fa-lg"></i>&nbsp;&nbsp;&nbsp;<?php echo get_custom_label('main_submenu_' . $data_categories_records_per_theme['name'] . '_subtitle'); ?>
                        </a>
                    </nav>

                    <div class="fixed-top d-none sub-submenu-data-mobile-content" id="<?php echo 'main-menu-data-' . $data_categories_records_per_theme['name'] . '-mobile-content'; ?>" style="background:<?php echo $data_categories_records_per_theme['color']; ?>">
                        <ul class="navbar-nav">
                            <?php foreach($data_categories_records_per_theme['data'] as $data_categories_records_per_theme_data) { ?>
                                <li class="nav-item">
                                    <a class="nav-link text-white" style="background:<?php echo $data_categories_records_per_theme['color']; ?>" href="<?php echo $data_categories_records_per_theme_data['link']; ?>"><?php echo $data_categories_records_per_theme_data['name']; ?></a>
                                </li>
                            <?php } ?>
                        </ul>
                    </div>
                <?php } ?>

                <nav class="navbar fixed-top d-none sub-menu-mobile-header" id="main-menu-authorities-mobile-header">
                    <a href="javascript:void(0);">
                        <i class="fa fa-chevron-left fa-lg"></i>&nbsp;&nbsp;&nbsp;<?php echo get_custom_label('main_submenu_authorities_title'); ?>
                    </a>
                </nav>

                <div class="fixed-top d-none sub-menu-mobile-content" id="main-menu-authorities-mobile-content">
                    <ul class="navbar-nav">
                        <?php foreach (get_authorities_categories_records_per_category() as $authorities_categories_records_per_category) { ?>
                        <li class="nav-item">
                            <a class="nav-link" data-submenu="main-menu-authorities-<?php echo $authorities_categories_records_per_category['slug']; ?>" href="javascript:void(0);"><?php echo $authorities_categories_records_per_category['label']; ?></a>
                        </li>
                        <?php } ?>
                    </ul>
                </div>

                <?php foreach (get_authorities_categories_records_per_category() as $authorities_categories_records_per_category) { ?>
                    <nav class="navbar fixed-top d-none sub-submenu-mobile-header" id="<?php echo 'main-menu-authorities-' . $authorities_categories_records_per_category['slug'] . '-mobile-header'; ?>">
                        <a href="javascript:void(0);" class="text-uppercase" data-submenu="main-menu-authorities-mobile">
                            <i class="fa fa-chevron-left fa-lg"></i>&nbsp;&nbsp;&nbsp;<?php echo $authorities_categories_records_per_category['label']; ?>
                        </a>
                    </nav>

                    <div class="fixed-top d-none sub-submenu-authority-mobile-content" id="<?php echo 'main-menu-authorities-' . $authorities_categories_records_per_category['slug'] . '-mobile-content'; ?>">
                        <ul class="navbar-nav">
                            <?php foreach($authorities_categories_records_per_category['data'] as $authorities_categories_records_per_category_data) { ?>
                                <li class="nav-item">
                                    <a class="nav-link" href="<?php echo $authorities_categories_records_per_category_data['link']; ?>"><?php echo $authorities_categories_records_per_category_data['alias'][0]; ?></a>
                                </li>
                            <?php } ?>
                        </ul>
                    </div>
                <?php } ?>

                <nav class="navbar fixed-top d-none sub-menu-mobile-header" id="main-menu-beneficiaries-mobile-header">
                    <a href="javascript:void(0);">
                        <i class="fa fa-chevron-left fa-lg"></i>&nbsp;&nbsp;&nbsp;<?php echo get_custom_label('main_submenu_beneficiaries_title'); ?>
                    </a>
                </nav>

                <div class="fixed-top d-none sub-menu-mobile-content" id="main-menu-beneficiaries-mobile-content">
                    <ul class="navbar-nav">
                        <?php foreach (get_beneficiaries_categories() as $beneficiary_category) { ?>
                        <li class="nav-item">
                            <a class="nav-link" href="<?php echo $beneficiary_category['link']; ?>"><?php echo $beneficiary_category['name']; ?></a>
                        </li>
                        <?php } ?>
                    </ul>
                </div>
            
            </div>
        </section>
        
        <!-- lg, xl nav -->
        <section class="d-none d-lg-block" id="main-nav-container">
            <nav class="navbar fixed-top navbar-expand-lg bg-white justify-content-between py-0 pr-0" id="main-nav">
                <a class="navbar-brand" href="<?php echo esc_url( home_url( '/' ) ); ?>">
                    <img src="<?= get_bloginfo("template_url"); ?>/img/logo.png" width="120" alt="" />
                </a>
                <ul class="navbar-nav" id="main-menu-navbar">
                    <?php foreach (get_main_menu_items('header_menu') as $menu_item) { ?>
                        <?php if (substr($menu_item->url, 0, 1) == '#') { ?>
                            <li class="nav-item">
                                <a class="nav-link nav-<?php echo end(explode('-', $menu_item->url)); ?>" href="javascript:void(0);" data-menu="<?php echo substr($menu_item->url, 1); ?>"><?php echo $menu_item->title; ?></a>
                            </li>
                        <?php } ?>
                    <?php } ?>
                </ul>
                <ul class="navbar-nav" id="main-menu-search-language">
                    <!-- <li class="nav-item">
                        <a href="javascript:searchThis();" class="nav-link"><i class="fa fa-search fa-lg"></i></a>
                    </li> -->
                    <li class="nav-item">
                        <a class="nav-link<?php echo (get_post_field('post_name') == 'a-propos' || get_post_field('post_name') == 'over-deze-website' ? ' active' : '') ?>" href="<?php echo $menu_item->url; ?>"><?php echo $menu_item->title; ?></a>
                    </li>
                    <?php foreach (get_main_menu_languages() as $language) { ?>
                    <li class="nav-item">
                        <a class="nav-link text-uppercase <?php echo ($language['code'] == substr(get_current_language(), 1, 2) ? "active" : ""); ?>" href="<?php echo $language['url']; ?>"><?php echo substr($language['code'], 0, 2); ?></a>
                    </li>
                    <?php } ?>
                </ul>
            </nav>
            
            <div class="container-fluid fixed-top d-none" id="main-menu">
                <div class="row d-none" id="main-menu-data">
                    <div class="col-lg-4 col-xl-3 pr-0" id="main-menu-data-left-pane">
                        <p class="main-menu-title mb-0"><?php echo get_custom_label('main_submenu_data_title'); ?><a class="float-right hide-main-menu" href="javascript:void(0);"><i class="fa fa-remove"></i></a></p>
                        <ul class="nav flex-column nav-pills" role="tablist" aria-orientation="vertical">
                            <?php foreach (get_data_categories_records_per_theme() as $data_category_themes) { ?>
                                <li class="nav-item" style="background:<?php echo $data_category_themes['color']; ?>">
                                    <a class="nav-link" data-pane="main-menu-data-right-pane" data-toggle="pill" data-color="#FFF" data-bgcolor="<?php echo $data_category_themes['color']; ?>" href="#menu-data-<?php echo $data_category_themes['id']; ?>" role="tab" aria-controls="menu-data-<?php echo $data_category_themes['id']; ?>" aria-selected="false"><?php echo $data_category_themes['label']; ?><i class="fa fa-chevron-right d-none float-right"></i></a>
                                </li>
                            <?php } ?>
                        </ul>
                    </div>
                
                    <div class="col-lg-8 col-xl-9 pl-0 main-menu-right-pane" id="main-menu-data-right-pane">
                        <div class="tab-content">
                            <?php foreach (get_data_categories_records_per_theme() as $data_category_records_per_theme) { ?>
                                <div class="tab-pane" id="menu-data-<?php echo $data_category_records_per_theme['id']; ?>" role="tabpanel" style="background: url(<?php echo $data_category_records_per_theme['nav_image']; ?>) no-repeat center bottom; background-size: 50%;">
                                    <div class="categories-list-title text-white">
                                        <?php echo get_custom_label('main_submenu_' . $data_category_records_per_theme['name'] . '_subtitle'); ?>
                                    </div>
                                    <div class="categories-list">
                                    <?php foreach($data_category_records_per_theme['data'] as $data_category_records_per_theme_data) { ?>
                                        <p class="mb-0">
                                            <a href="<?php echo $data_category_records_per_theme_data['link']; ?>" class="text-white"><?php echo $data_category_records_per_theme_data['name']; ?></a>
                                        </p>
                                    <?php } ?>
                                    </div>
                                </div>
                            <?php } ?>
                        </div>
                    </div>
                </div>

                <div class="row d-none" id="main-menu-authorities">
                    <div class="col-lg-4 col-xl-3 pr-0" id="main-menu-authorities-left-pane">
                        <p class="main-menu-title mb-0"><?php echo get_custom_label('main_submenu_authorities_title'); ?><a class="float-right hide-main-menu" href="javascript:void(0);"><i class="fa fa-remove"></i></a></p>
                        <div id="main-menu-authorities-nav">
                            <ul class="nav flex-column nav-pills" role="tablist" aria-orientation="vertical">
                                <?php foreach (get_authorities_categories_records_per_category() as $authorities_categories_records_per_category) { ?>
                                    <li class="nav-item">
                                        <a class="nav-link" data-pane="main-menu-authorities-right-pane" data-bgcolor="#e8e8e8" data-toggle="pill" href="#menu-authority-<?php echo $authorities_categories_records_per_category['id']; ?>" role="tab" aria-controls="menu-authority-<?php echo $authorities_categories_records_per_category['id']; ?>" aria-selected="false"><?php echo $authorities_categories_records_per_category['label']; ?><i class="fa fa-chevron-right d-none float-right"></i></a>
                                    </li>
                                <?php } ?>
                            </ul>
                        </div>
                    </div>
                
                    <div class="col-lg-8 col-xl-9 pl-0 main-menu-right-pane" id="main-menu-authorities-right-pane">
                        <div class="tab-content">
                            <?php foreach (get_authorities_categories_records_per_category() as $authorities_categories_records_per_category) { ?>
                                <div class="tab-pane" id="menu-authority-<?php echo $authorities_categories_records_per_category['id']; ?>" role="tabpanel">
                                    <div class="categories-list-title">
                                        <?php echo $authorities_categories_records_per_category['label']; ?>
                                    </div>
                                    <div class="categories-list-dark">
                                    <?php foreach($authorities_categories_records_per_category['data'] as $authorities_categories_records_per_category_data) { ?>
                                        <p class="mb-0">
                                            <a class="" href="<?php echo $authorities_categories_records_per_category_data['link']; ?>"><?php echo $authorities_categories_records_per_category_data['alias'][0]; ?></a>
                                        </p>
                                    <?php } ?>
                                    </div>
                                </div>
                            <?php } ?>
                        </div>
                    </div>
                </div>

                <div class="row d-none" id="main-menu-beneficiaries">
                    <div class="col-lg-6" id="main-menu-beneficiaries-left-pane">
                        <p class="main-menu-title mb-0"><?php echo get_custom_label('main_submenu_beneficiaries_title'); ?><a class="float-right hide-main-menu" href="javascript:void(0);"><i class="fa fa-remove"></i></a></p>
                        <div id="main-menu-beneficiaries-nav">
                            <ul class="nav flex-column nav-pills" role="tablist" aria-orientation="vertical">
                                <?php foreach (get_beneficiaries_categories() as $beneficiary_category) { ?>
                                    <li class="nav-item">
                                        <a class="nav-link" href="<?php echo $beneficiary_category['link']; ?>"><?php echo $beneficiary_category['name']; ?></a>
                                    </li>
                                <?php } ?>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </section>
